﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppPrototype.CustomTypes;
using AppPrototype.Models;
using AppPrototype.Models.CustomTypes;
using AppPrototype.Models.CustomTypes.MealLogic;
using AppPrototype.Models.Enums;
using Xamarin.Forms;

namespace AppPrototype
{
	public partial class App : Application
	{
		public App()
		{
			InitializeComponent();

			/////////////////////////////////////////////////////////


			var Route = new Route("Test");
			var plan = new Plan()
			{
				User = new User() { Gender = AppPrototype.Models.Enums.Gender.M, UserName = "Chris" },
				FitnessLevel = FitnessLevel.Advanced,
				WeightGoal = WeightGoal.GainMuscle,
				Activity = Activity.BodyweightTraining,
				SessionLength = new TimeSpan(00, 30, 00),
				DaysPerWeek = 4,
				GymOrHome = GymOrHome.Home,
				Sessions = new List<Session>()
				{
					new Session()
					{
						ActivityType = Activity.BodyweightTraining,
						AvailableTime = new TimeSpan(00, 30, 00),
						PlanId = 1,
						Name = "S1",
						Version = 1,
						Sections =

							new List<Section>()
							{
								new Section()
								{
									MuscleGroups = new List<MuscleGroup>()
									{
									    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Chest")
									},
								   AvailableTime = new TimeSpan(00, 20, 00)
								},

                                new Section()
                                {
                                    MuscleGroups = new List<MuscleGroup>()
                                    {
								        Route.MuscleGroups.SingleOrDefault(i => i.Name == "Shoulders"),
								        Route.MuscleGroups.SingleOrDefault(i => i.Name == "Triceps")

								    },
                                    AvailableTime =
                                        new TimeSpan(00, 10, 00),
                                    IsSuperSetted = true

                            }

				      }
					},
                    new Session()
                    {
                        ActivityType = Activity.BodyweightTraining,
                        AvailableTime = new TimeSpan(00, 30, 00),
                        PlanId = 1,
                        Name = "S1",
                        Version = 1,
                        Sections =

                            new List<Section>()
                            {
                                new Section()
                                {
                                    MuscleGroups = new List<MuscleGroup>()
                                    {
                                        Route.MuscleGroups.SingleOrDefault(i => i.Name == "Back")
                                    },
                                   AvailableTime = new TimeSpan(00, 20, 00)
                                },

                                new Section()
                                {
                                    MuscleGroups = new List<MuscleGroup>()
                                    {
                                        Route.MuscleGroups.SingleOrDefault(i => i.Name == "Biceps"),
                                        Route.MuscleGroups.SingleOrDefault(i => i.Name == "Core")

                                    },
                                    AvailableTime =
                                        new TimeSpan(00, 10, 00),
                                    IsSuperSetted = true

                            }

                      }
                    },


               new Session()
                    {
                        ActivityType = Activity.BodyweightTraining,
                        AvailableTime = new TimeSpan(00, 30, 00),
                        PlanId = 1,
                        Name = "S1",
                        Version = 1,
                        Sections =

                            new List<Section>()
                            {
                                new Section()
                                {
                                    MuscleGroups = new List<MuscleGroup>()
                                    {
                                        Route.MuscleGroups.SingleOrDefault(i => i.Name == "Qauds")
                                    },
                                   AvailableTime = new TimeSpan(00, 20, 00)
                                },

                                new Section()
                                {
                                    MuscleGroups = new List<MuscleGroup>()
                                    {
                                        Route.MuscleGroups.SingleOrDefault(i => i.Name == "Calves"),
                                        Route.MuscleGroups.SingleOrDefault(i => i.Name == "Hamms")

                                    },
                                    AvailableTime =
                                        new TimeSpan(00, 10, 00),
                                    IsSuperSetted = true

                            }

                      }
                    },
               new Session()
                    {
                        ActivityType = Activity.BodyweightTraining,
                        AvailableTime = new TimeSpan(00, 30, 00),
                        PlanId = 1,
                        Name = "S1",
                        Version = 1,
                        Sections =

                            new List<Section>()
                            {
                                new Section()
                                {
                                    MuscleGroups = new List<MuscleGroup>()
                                    {
                                        Route.MuscleGroups.SingleOrDefault(i => i.Name == "Chest")
                                    },
                                   AvailableTime = new TimeSpan(00, 20, 00)
                                },

                                new Section()
                                {
                                    MuscleGroups = new List<MuscleGroup>()
                                    {
                                        Route.MuscleGroups.SingleOrDefault(i => i.Name == "Biceps"),
                                        Route.MuscleGroups.SingleOrDefault(i => i.Name == "Triceps")

                                    },
                                    AvailableTime =
                                        new TimeSpan(00, 10, 00),
                                    IsSuperSetted = true

                            }

                      }
                   
                }
                }
            };
		  //new RouteFinder().FindAndExecute(plan);
			new SetCalculator().CalculateSets(plan);
			new ExerciseAdder().PopulateExercises(plan);
		//	new TrainingTechniqueAdder().AddTrainingTechniques(plan);
		    new IWantBigger().WantBigger(plan, Route.MuscleGroups.SingleOrDefault(i => i.Name == "Shoulders"), ResultExpectancy.ASAP, null);
		//new IWantToLoose().IWantToLose(plan, Route.MuscleGroups.SingleOrDefault(i => i.Name == "Core"), 3,new List<Session>(), ResultExpectancy.Soon, new List<Exercise>(), Caller.IWantToLoose);
			//issue with STONES if user 17.3stone the system is going to remove the . and it will read 173 stone
			//issue why dont i just add the goalCalorieDifference in the method i can work it out by checking the plan.WeightGoal, and the ActivityLevel
			//new MacroGenerator().Generate(plan, "75kg", "5 ft 6", 21, "ModeratelyActive", WeightMethod.Kg, -200);
            
          //  new MealPlanGenerator().SetNutritionPlanType(plan.User, NutritionPlanType.Full);
          //  new MealPlanGenerator().SetWhichMealsArePlanned(plan.User,"Dinner Breakfast Lunch Snack");
          //  new MealPlanGenerator().GenerateMeals(plan);
          //  new SessionGoalGenerator().Generate(plan);
			///////////////////////////////////////////////////////////////// 
			MainPage = new AppPrototype.MainPage();
		}
	

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{ 
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	
	}
}
