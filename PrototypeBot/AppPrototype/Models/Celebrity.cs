﻿using AppPrototype.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AppPrototype.Models
{



    [Serializable]
    public class Celebrity
    {
        public string Name { get; set; }
        public int BodyTypeId { get; set; }
        public Gender Gender { get; set; }

        public Celebrity(string name, int bodyType, Gender gender)
        {
            Name = name;
            BodyTypeId = bodyType;
            Gender = gender;
        }

    }
}