﻿using AppPrototype.Models.CustomTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppPrototype.Models.Converters
{
    public class RestTimeCalculator
    {
        public TimeSpan CalculateCardioRestTime(Plan plan)
        {
        var workingTime = Convert.ToInt32(plan.User.CardioRatio.ToString().Substring(0, plan.User.CardioRatio.ToString().IndexOf(".")));
        var restingTime = Convert.ToInt32(plan.User.CardioRatio.ToString().Substring(plan.User.CardioRatio.ToString().IndexOf(".") + 1, 1));
        var parts = workingTime + restingTime;
        var workingSeconds = Convert.ToInt32((plan.User.SetTime.TotalSeconds / parts) * workingTime);
        var restingSeconds = Convert.ToInt32((plan.User.SetTime.TotalSeconds / parts) * restingTime);
        return new TimeSpan(00, 00, restingSeconds);
        }

    }
}
