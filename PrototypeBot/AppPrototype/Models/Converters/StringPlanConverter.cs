﻿using AppPrototype.Models.CustomTypes;
using AppPrototype.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppPrototype.Models.Converters
{
    [Serializable]
    public class StringPlanConverter
    {
        public Plan ConvertPlan(string weightGoal, string fitnessLevel, string gymOrHome, string activity, string sessionLength, string daysPerWeek)
        {
            var plan = new Plan();

            //set weightGoal
            var weightGoals = new List<WeightGoal>() { WeightGoal.GainMuscle, WeightGoal.LooseFat, WeightGoal.ToneUp };
            if (weightGoals.Any(i => i.ToString().ToLowerInvariant() == weightGoal.ToLowerInvariant()))
            {
                plan.WeightGoal = weightGoals.FirstOrDefault(i => i.ToString() == weightGoal);
            }
            ///////////////

            //Set fitnessLevel
            var fitnessLevels = new List<FitnessLevel>() { FitnessLevel.Beginner, FitnessLevel.Intermediate, FitnessLevel.Advanced };
            if (fitnessLevels.Any(i => i.ToString().ToLowerInvariant() == fitnessLevel.ToLowerInvariant()))
            {
                plan.FitnessLevel = fitnessLevels.FirstOrDefault(i => i.ToString() == fitnessLevel);
            }
            ///////////////

            //Set gymOrHome
            var gymOrHomes = new List<GymOrHome>() { GymOrHome.Gym, GymOrHome.Home };
            if (gymOrHomes.Any(i => i.ToString().ToLowerInvariant() == gymOrHome.ToLowerInvariant()))
            {
                plan.GymOrHome = gymOrHomes.FirstOrDefault(i => i.ToString() == gymOrHome);
            }
            ///////////////

            //Set activity
            var activities = new List<Activity>() { Activity.BodyweightTraining, Activity.Cardio, Activity.Weightlifting };
            if (activities.Any(i => i.ToString().ToLowerInvariant() == activity.ToLowerInvariant()))
            {
                plan.Activity = activities.FirstOrDefault(i => i.ToString() == activity);
            }
            ///////////////

            //Set sessionLength
            var sessionLengths = new List<TimeSpan>() { new TimeSpan(00,10,00), new TimeSpan(00, 15, 00), new TimeSpan(00, 20, 00), new TimeSpan(00, 30, 00),
                                                        new TimeSpan(00,45,00), new TimeSpan(00,60,00)};
            if (sessionLengths.Any(i => i.Minutes == Convert.ToInt32(sessionLength)))
            {
                plan.SessionLength = sessionLengths.FirstOrDefault(i => i.Minutes == Convert.ToInt32(sessionLength));
            }
            ///////////////

            //Set daysPerWeek
            if (Convert.ToInt32(daysPerWeek) <= 7 && Convert.ToInt32(daysPerWeek) >= 3)
            {
                plan.DaysPerWeek = Convert.ToInt32(daysPerWeek);
            }
            ///////////////

            //Set goal
            //if (!string.IsNullOrEmpty(goal))
            //{
            //  //  plan.Goal = goal;
            //}
            //////////////
            return plan;
        }
    }
}
