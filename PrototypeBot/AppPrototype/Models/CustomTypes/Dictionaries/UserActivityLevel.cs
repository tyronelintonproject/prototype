﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppPrototype.Models.CustomTypes.Dictionaries
{
    [Serializable]
    public class UserActivityLevel 
    {
        public Dictionary<string, double> dictionary = new Dictionary<string, double>()
        {
            { "LightlyActive", 1.375 },
            { "ModeratelyActive", 1.55 },
            { "VeryActive", 1.725 },
            { "ExtraActive", 1.9 },
        };
    }
}
