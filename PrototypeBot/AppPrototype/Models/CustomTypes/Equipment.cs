﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Text;

namespace AppPrototype.Models.CustomTypes
{
    [Serializable]
    public class Equipment
    {
        public string Name { get; set; }
        public List<string> Synonyms { get; set; }
    }
}
