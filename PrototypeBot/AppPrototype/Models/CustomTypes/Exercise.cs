﻿using System;
using System.Collections.Generic;
using AppPrototype.Models.Enums;

namespace AppPrototype.Models.CustomTypes
{
    [Serializable]
    public class Exercise
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int FamilyNo { get; set; }
        public MuscleGroup PrimaryMuscleGroup { get; set; }
        public MuscleGroup SecondaryMuscleGroup { get; set; }
        public int MovementType { get; set; }
        public CompOrIso CompOrIso { get; set; }
         public List<Activity> Activities { get; set; }
        public double Popularity { get; set; }
        public FitnessLevel FitnessLevel { get; set; }
        public List<WeightGoal> WeightGoals { get; set; }
        public EquipmentType? EquipmentType { get; set; }
        public int SiblingExerciseId { get; set; }
        public bool IsSuperSetted { get; set; }
        public bool IsDropSetted { get; set; }
        public bool IsTripleDropSetted { get; set; }
        public Exercise DropsetVariationExercise { get; set; }
        public bool CanHaveReps { get; set; }
        public string Gif { get; set; }

        public Exercise()
        {
            WeightGoals = new List<WeightGoal>();
            PrimaryMuscleGroup = new MuscleGroup();
            SecondaryMuscleGroup = new MuscleGroup();
            Activities= new List<Activity>();
        }
    }
}
