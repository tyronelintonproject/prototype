﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AppPrototype.CustomTypes;
using AppPrototype.Models.Enums;

namespace AppPrototype.Models.CustomTypes
{
    public class ExerciseAdder
    {

        private void AddSetToTheSetListWhichIsLessThanYetClosestTo3(Section section, List<Set> normalSets, List<Set> superSets)
        {
            if (section.IsSuperSetted)
            {
                superSets.Add(new Set() {SetType = SetType.SuperSet});
                return;
            }
            if (normalSets.Count >= 3)
            {
                superSets.Add(new Set() {SetType = SetType.SuperSet});
                return;
            }
            if (superSets.Count >= 3)
            {
                normalSets.Add(new Set() {SetType = SetType.Normal});
                return;
            }
            if (normalSets.Count <= 3 && superSets.Count <= 3)
            {

                superSets.Add(new Set() {SetType = SetType.SuperSet});
                return;
            }
        }

        public void PopulateOneSession(Plan plan, Session session, UpdateType updateType, Caller caller, ResultExpectancy resultExpectancy)
        {
            try
            {
                if (plan.Activity == Activity.Weightlifting || plan.Activity == Activity.BodyweightTraining)
                    {
                        if (plan.DaysPerWeek > 3)
                        {
                            foreach (var section in session.Sections)
                            {
                                var listOfRemainingNormalSets = CreateSetList(SetType.Normal, section.NoOfNormalSets);
                                var listOfRemainingSuperSets = CreateSetList(SetType.SuperSet, section.NoOfSuperSets);
                                var listOfRemainingTotalSets = listOfRemainingNormalSets.Concat(listOfRemainingSuperSets)
                                    .ToList();

                                while (section.TotalSets >= 2)
                                {

                                    if (HasEnoughSets(session, listOfRemainingTotalSets.Count))
                                    {
                                        if (listOfRemainingTotalSets.Count == 2)
                                        {
                                            //check the below method works properly
                                            AddSetToTheSetListWhichIsLessThanYetClosestTo3(section, listOfRemainingNormalSets, listOfRemainingSuperSets);
                                            session.IsOverPrescribed = true;
                                        }


                                        if (section.IsSuperSetted && HasEnoughSets(session, listOfRemainingTotalSets.Count))
                                        {
                                            AddSuperSetExercise(plan, session, section);
                                            //Only pass through the super sets, theyre shouldn't be any normal sets here anyway the setCalc should make sure of that
                                        }

                                        if ((section.Exercises == null && !section.IsSuperSetted) || (section.Exercises.Count == 0 && !section.IsSuperSetted) &&
                                        HasEnoughSets(session, listOfRemainingTotalSets.Count))
                                        {
                                            AddNormalSetExercise(plan, session, section);
                                        }

                                        if (section.Exercises != null && section.Exercises.Count != 0 && !section.IsSuperSetted && HasEnoughSets(session, listOfRemainingTotalSets.Count))
                                        {

                                            //Find the type of set not in the section and if theres 3 or more sets for that add it, else add the other exe.
                                            if (!section.Exercises.Any(i => i.IsSuperSetted))
                                            {
                                                AddSuperSetExercise(plan, session, section);
                                            }
                                            else
                                            {
                                                //Check which list has more sets left and add an exercise for that type
                                                if (listOfRemainingSuperSets.Count >= listOfRemainingNormalSets.Count)
                                                {
                                                    AddSuperSetExercise(plan, session, section);
                                                }
                                                else
                                                {
                                                    AddNormalSetExercise(plan, session, section);
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        }
                        if (plan.DaysPerWeek <= 3)
                        {
                            PopulateAFullBodySession(plan, session,updateType, caller);
                        }
                        
                }
                        if (plan.Activity == Activity.Cardio)
                        {
                        PopulateACircuit(plan, session, updateType, caller, resultExpectancy);
                        }
                }
            
            catch (Exception ex)
            {
                var es = ex;
            }

        }

        public void PopulateExercises(Plan plan)
        {
            try
            {

                foreach (var session in plan.Sessions)
                {

                    if (plan.Activity == Activity.Weightlifting || plan.Activity == Activity.BodyweightTraining)
                    {
                        if (plan.DaysPerWeek > 3)
                        {
                            foreach (var section in session.Sections)
                            {
                                var listOfRemainingNormalSets = CreateSetList(SetType.Normal, section.NoOfNormalSets);
                                var listOfRemainingSuperSets = CreateSetList(SetType.SuperSet, section.NoOfSuperSets);
                                var listOfRemainingTotalSets = listOfRemainingNormalSets.Concat(listOfRemainingSuperSets)
                                    .ToList();

                                while (section.TotalSets >= 2)
                                {

                                    if (HasEnoughSets(session, listOfRemainingTotalSets.Count))
                                    {
                                        if (listOfRemainingTotalSets.Count == 2)
                                        {
                                            //check the below method works properly
                                            AddSetToTheSetListWhichIsLessThanYetClosestTo3(section, listOfRemainingNormalSets, listOfRemainingSuperSets);
                                            session.IsOverPrescribed = true;
                                        }


                                        if (section.IsSuperSetted && HasEnoughSets(session, listOfRemainingTotalSets.Count))
                                        {
                                            AddSuperSetExercise(plan, session, section);
                                            //Only pass through the super sets, theyre shouldn't be any normal sets here anyway the setCalc should make sure of that
                                        }

                                        if ((section.Exercises == null && !section.IsSuperSetted) || (section.Exercises.Count == 0 && !section.IsSuperSetted) &&
                                        HasEnoughSets(session, listOfRemainingTotalSets.Count))
                                        {
                                            AddNormalSetExercise(plan, session, section);
                                        }

                                        if (section.Exercises != null && section.Exercises.Count != 0 && !section.IsSuperSetted && HasEnoughSets(session, listOfRemainingTotalSets.Count))
                                        {

                                            //Find the type of set not in the section and if theres 3 or more sets for that add it, else add the other exe.
                                            if (!section.Exercises.Any(i => i.IsSuperSetted))
                                            {
                                                AddSuperSetExercise(plan, session, section);
                                            }
                                            else
                                            {
                                                //Check which list has more sets left and add an exercise for that type
                                                if (listOfRemainingSuperSets.Count >= listOfRemainingNormalSets.Count)
                                                {
                                                    AddSuperSetExercise(plan, session, section);
                                                }
                                                else
                                                {
                                                    AddNormalSetExercise(plan, session, section);
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        }
                        if (plan.DaysPerWeek <= 3)
                        {
                            PopulateAFullBodySession(plan, session, UpdateType.None, Caller.None);
                        }
                        
                    }
                    if (plan.Activity == Activity.Cardio)
                        {
                            PopulateACircuit(plan, session, UpdateType.None, Caller.None, ResultExpectancy.None);
                        }
                    
                }
            }
            catch (Exception ex)
            {
                var es = ex;
            }
    }

        private List<Set> CreateSetList(SetType type, int noRequired)
        {
            var sets = new List<Set>();
                
            while (noRequired > 0)
            {
                sets.Add(new Set() {SetType = type});
                noRequired--;
            }
            return sets;
        }

        public void AddNormalSetExercise(Plan plan, Session session, Section section, MuscleGroup muscleGroup = null, bool mustBeCompound = false)
        {
            MuscleGroup selectedMuscleGroup = null;


            if (muscleGroup != null)
            {
                selectedMuscleGroup = muscleGroup;
            }
            else
            {
                //if we have exercises              //and we have more than 1 muscle group
                if (section.Exercises.Count > 0 && section.MuscleGroups.Count > 1)
                {
                    selectedMuscleGroup = section.Exercises.LastOrDefault().PrimaryMuscleGroup;
                }
                //this ensures we don't add another exercise to the same MG
                selectedMuscleGroup = (selectedMuscleGroup != null)
                    ? section.MuscleGroups.SingleOrDefault(i => i != selectedMuscleGroup)
                    : section.MuscleGroups.FirstOrDefault();
            }


            if (plan.Activity == Activity.Cardio)
            {
                section.Exercises.Add(new ExerciseFilterer().NormalSetFilter(plan, session, section, selectedMuscleGroup, mustBeCompound));
                return;
            }


            if (section.MuscleGroups.Any(i => i.Size == Size.Large))
            {
                    //If a big muscle group and we dont have enough comp exs must add comp                                         
                if (section.Exercises.Where(i => i.CompOrIso == CompOrIso.Compound && i.PrimaryMuscleGroup.Size == Size.Large).Count() < 2)
                {

                    section.Exercises.Add(new ExerciseFilterer().NormalSetFilter(plan, session, section, selectedMuscleGroup, true));
                    section.NoOfNormalSets = section.NoOfNormalSets - 3;
                    UpdateUsedTimeAfterAddingExerciseNotUsedForCardio(plan.User,section,SetType.Normal);
                    //Filter list and add exercise which is compound
                    //Add exercise to the exercise list
                    //Remove 3 normal sets
                }
                else
                {
                    section.Exercises.Add(
                        new ExerciseFilterer().NormalSetFilter(plan, session, section,
                            selectedMuscleGroup, mustBeCompound));
                    section.NoOfNormalSets = section.NoOfNormalSets - 3;
                    UpdateUsedTimeAfterAddingExerciseNotUsedForCardio(plan.User, section, SetType.Normal);
                    //Filter list and add exercise which is iso
                    //Add exercise to the exercise list
                    //Remove 3 normal sets
                }
            }

            

            else
            {// Cardio add exercises first session fine, second session only adding 3 exs then it exits the execution for some reason, third session has
                //no exercises.. INVESTIGATE and resolve.
                section.Exercises.Add(
                    new ExerciseFilterer().NormalSetFilter(plan, session, section, selectedMuscleGroup,
                        mustBeCompound));
                section.NoOfNormalSets = section.NoOfNormalSets - 3;
                UpdateUsedTimeAfterAddingExerciseNotUsedForCardio(plan.User, section, SetType.Normal);
                //Filter list and add exercise
                //Add exercise to the exercise list
                //Remove 3 normal sets
            }

        }

        public void AddSuperSetExercise(Plan plan, Session session, Section section, List<MuscleGroup> muscleGroup = null, bool mustBeCompound = false)
        {
            //MuscleGroup selectedMuscleGroup = null;
            
            //if we have exercises              //and we have more than 1 muscle group

            //if (section.Exercises.Count > 0 && section.MuscleGroups.Count > 1)
            //{
            //    selectedMuscleGroup = section.Exercises.LastOrDefault().PrimaryMuscleGroup;
            //}
            ////this ensures we don't add another exercise to the same MG
            //selectedMuscleGroup = (selectedMuscleGroup != null)
            //    ? section.MuscleGroups.SingleOrDefault(i => i != selectedMuscleGroup)
            //    : section.MuscleGroups.FirstOrDefault();
            //Test
            //List<MuscleGroup> muscleGroups = new List<MuscleGroup>(section.MuscleGroups);

            if (section.MuscleGroups.Any(i => i.Size == Size.Large))
            {
                if (section.Exercises.Where(i => i.CompOrIso == CompOrIso.Compound && i.PrimaryMuscleGroup.Size == Size.Large).Count() < 2)
                {
                    var exercises =  new ExerciseFilterer().SuperSetFilter(plan, session, section, muscleGroup ?? section.MuscleGroups, true);

                    CreateSuperSetLink(exercises);

                    section.Exercises.AddRange(exercises);
                    section.NoOfSuperSets = section.NoOfSuperSets - 3;
                    UpdateUsedTimeAfterAddingExerciseNotUsedForCardio(plan.User, section, SetType.SuperSet);
                    //this ensures we don't add another exercise to the same MG if there is more than 1
                    //selectedMuscleGroup = (selectedMuscleGroup != null)
                    //    ? section.MuscleGroups.SingleOrDefault(i => i != selectedMuscleGroup)
                    //    : section.MuscleGroups.FirstOrDefault();

                    ////add ex 2
                    //section.Exercises.AddRange(
                    //    new ExerciseFilterer().SuperSetFilter(plan, session, section,
                    //        muscleGroups, true));


                }
                else
                {
                    //add ex 1

                    var exercises = new ExerciseFilterer().SuperSetFilter(plan, session, section,
                            muscleGroup ?? section.MuscleGroups, mustBeCompound);

                    CreateSuperSetLink(exercises);
                    section.Exercises.AddRange(exercises);
                    section.NoOfSuperSets = section.NoOfSuperSets - 3;
                    UpdateUsedTimeAfterAddingExerciseNotUsedForCardio(plan.User, section, SetType.SuperSet);


                    ////this ensures we don't add another exercise to the same MG if there is more than 1
                    //selectedMuscleGroup = (selectedMuscleGroup != null)
                    //    ? section.MuscleGroups.SingleOrDefault(i => i != selectedMuscleGroup)
                    //    : section.MuscleGroups.FirstOrDefault();

                    ////add ex 2
                    //section.Exercises.AddRange(
                    //    new ExerciseFilterer().SuperSetFilter(plan, session, section,
                    //        muscleGroups, false));


                }

            }
            else
            {
                //add ex 1

                var exercises = new ExerciseFilterer().SuperSetFilter(plan, session, section,
                        muscleGroup ?? section.MuscleGroups, mustBeCompound);

                CreateSuperSetLink(exercises);
                section.Exercises.AddRange(exercises);
                section.NoOfSuperSets = section.NoOfSuperSets - 3;
                UpdateUsedTimeAfterAddingExerciseNotUsedForCardio(plan.User, section, SetType.SuperSet);
                //this ensures we don't add another exercise to the same MG if there is more than 1
                //selectedMuscleGroup = (selectedMuscleGroup != null)
                //    ? section.MuscleGroups.SingleOrDefault(i => i != selectedMuscleGroup)
                //    : section.MuscleGroups.FirstOrDefault();

                ////add ex 2
                //section.Exercises.AddRange(
                //    new ExerciseFilterer().SuperSetFilter(plan, session, section,
                //        muscleGroups, false));

            }
        }

        public void PopulateACircuit(Plan plan, Session session, UpdateType updateType, Caller caller, ResultExpectancy resultExpectancy)
        {
            var Route = new Route("Test");

            var selectedIndex = plan.Sessions.FindIndex(i => i == session);


            if (caller == Caller.IWantToLoose)
            {
                    //if this is an odd no session aka this is the second session(so index is 1) or the forth session(so index is 3) make this session
                    //a gain session so here we update the users weightgoal to gain so we get some of the power exercises good for weightloss

                    var weightGoal = plan.WeightGoal;

                    if (selectedIndex % 2 != 0)
                    {
                        UpdateUserGoal(WeightGoalUpdateReason.UserOnCardioPlanButWantToLoose, plan);
                    }

                    var largeMuscleGroups2 = new List<MuscleGroup>()
                {
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == session.PrioritizedMuscleGroup.Name),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Full"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == session.PrioritizedMuscleGroup.Name),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Full"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == session.PrioritizedMuscleGroup.Name),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Full"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == session.PrioritizedMuscleGroup.Name),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Full"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == session.PrioritizedMuscleGroup.Name),
                };

                    int myIndex = 0;

                    while (myIndex < session.ExPerRound)
                    {
                        AddNormalSetExercise(plan, session, session.Sections.FirstOrDefault(), largeMuscleGroups2[myIndex]);
                        myIndex++;
                    }

                    if (plan.WeightGoal == WeightGoal.GainMuscle)
                    {
                        UpdateUserGoal(WeightGoalUpdateReason.UserOnCardioPlanButWantToLoose, plan, weightGoal);
                    }

                    return;
                }

            if (updateType == UpdateType.Dedicate && caller == Caller.IWantBigger)
            {
                //save user weight goal in a var for re-adding later, update user weight goal part 1
                var userGoal = plan.WeightGoal;
                UpdateUserGoal(WeightGoalUpdateReason.UserOnCardioPlanButWantToGetBigger, plan);

                var indexDedicate = 0;
               while (indexDedicate < session.ExPerRound)
               {
                    AddNormalSetExercise(plan, session, session.Sections.FirstOrDefault(), session.PrioritizedMuscleGroup);
                    indexDedicate++;
               }

               var usedTimeDedicate = Convert.ToInt32(session.NoOfRounds * (Convert.ToInt32(session.Sections.FirstOrDefault().Exercises.Count * plan.User.SetTime.TotalSeconds)));
               //set the usedTime
               session.Sections.FirstOrDefault().UsedTime += new TimeSpan(00, 00, usedTimeDedicate);

                //readd user weight goal from var , update user weight goal part 2 set everything back as normal only wanted the change for this reason but not for everything.
                UpdateUserGoal(WeightGoalUpdateReason.UserOnCardioPlanButWantToGetBigger, plan, userGoal);
                return;
            }

                             List<MuscleGroup> largeMuscleGroups;

                if (plan.User.Gender == Gender.M)
                {
                    largeMuscleGroups = new List<MuscleGroup>()
                {
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Full"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Chest"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Full"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Qauds"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Full"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Shoulders"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Core"),
                };
                }
                else
                {
                    largeMuscleGroups = new List<MuscleGroup>()
                {
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Full"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Glutes"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Full"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Shoulders"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Full"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Qauds"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Core")
                };

                }

            if (updateType == UpdateType.Prioritize && caller == Caller.IWantBigger)
            {
                //this ensures we have at least half of the exercises for our priorizted msucleGroups
                int counter = 0;
                foreach (var mg in largeMuscleGroups)
                {   //if even number aka for every second mg swap it with our prioritized mg.
                    if (counter % 2 == 0)
                    {
                        UpdateMuscleGroup(mg, session.PrioritizedMuscleGroup);
                    }
                    counter++;
                }
            }
                     
            
            List<MuscleGroup> updatedLargeMuscleGroups = new List<MuscleGroup>(largeMuscleGroups);

                var thisIndex = plan.Sessions.FindIndex(i => i == session);

                var previousSession = (thisIndex > 0) ? plan.Sessions[thisIndex - 1] : null;

                if (previousSession != null && previousSession.Sections != null &&
                    previousSession.Sections.FirstOrDefault().Exercises.Count > 1)
                {
                    foreach (var mg in largeMuscleGroups)
                    {
                        if (!previousSession.Sections.FirstOrDefault().Exercises.Any(i => i.PrimaryMuscleGroup.Name == mg.Name))
                        {
                            //put that large muscle group at the start of the list so it is choosen first.
                            updatedLargeMuscleGroups.Remove(mg);
                            updatedLargeMuscleGroups.Insert(0, mg);
                        }
                    }
                }

                var index = 0;

                while (index < session.ExPerRound)
                { //Need to add a method for updating used time for cardio .. really i should call that methodin the addexercise method to keep cleaner
                AddNormalSetExercise(plan, session, session.Sections.FirstOrDefault(), (session.PrioritizedMuscleGroup ?? updatedLargeMuscleGroups[index]));
                index++;
                }

                var usedTime = Convert.ToInt32(session.NoOfRounds * (Convert.ToInt32(session.Sections.FirstOrDefault().Exercises.Count * plan.User.SetTime.TotalSeconds)));
                //set the usedTime
                session.Sections.FirstOrDefault().UsedTime += new TimeSpan(00, 00, usedTime);
         
        }



        public void PopulateAFullBodySession(Plan plan, Session session, UpdateType updateType, Caller caller)
        {
            if (updateType == UpdateType.Dedicate && caller == Caller.IWantBigger)
                    {
                        foreach (var sec in session.Sections)
                        {
                            while (HasEnoughSets(session, sec.NoOfNormalSets))
                            {
                                //add exercises
                                //still need to update below method to make it always be compound
                                AddNormalSetExercise(plan, session, sec, session.PrioritizedMuscleGroup, true);// sec.Exercises.Add(new ExerciseFilterer().NormalSetFilter(plan, session, sec, updatedLargeMuscleGroups[index], true));
                            }
                            while (HasEnoughSets(session, sec.NoOfSuperSets))
                            {
                                AddSuperSetExercise(plan, session, sec, sec.MuscleGroups , true);
                                //UpdateUsedTimeAfterAddingExerciseNotUsedForCardio(plan.User, sec, SetType.SuperSet);
                            }
                        }
                return;
                    }

            var Route = new Route("Test");

            List<MuscleGroup> largeMuscleGroups;
            List<MuscleGroup> compulsaryMuscleGroups;

            if (plan.User.Gender == Gender.M)
            {
                largeMuscleGroups = new List<MuscleGroup>()
                {
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Chest"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Back"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Shoulders"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Qauds"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Hamms"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Triceps"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Biceps"),
                };

                compulsaryMuscleGroups = new List<MuscleGroup>()
                {
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Chest"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Back"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Qauds"),
                };
            }
            else
            {
                largeMuscleGroups = new List<MuscleGroup>()
                {
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Glutes"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Hamms"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Qauds"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Shoulders"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Calves"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Core"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Full")
                };

                compulsaryMuscleGroups = new List<MuscleGroup>()
                {
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Qauds"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Glutes"),
                    Route.MuscleGroups.SingleOrDefault(i => i.Name == "Full"),
                };
            }

            //Just find a way of replacing the below , so every other muscle group is the prioritzed one Chest so its Chest, back, chest, hamms, chest ... 
            //this way its still prioritzed and im not altering the session structure here.
            if (updateType == UpdateType.Prioritize && caller == Caller.IWantBigger)
            {
                //this ensures that we will always have at least 1 exercise for our prioritized muscleGroup //needed as these could push our mg out of list.
                compulsaryMuscleGroups.Add(session.PrioritizedMuscleGroup);

                //this ensures we have at least half of the exercises for our priorizted msucleGroups
                int counter = 0;
                foreach (var mg in largeMuscleGroups)
                {   //if even number aka for every second mg swap it with our prioritized mg.
                    if (counter % 2 == 0)
                    {
                        UpdateMuscleGroup(mg, session.PrioritizedMuscleGroup);
                    }
                    counter++;
                }
            }
            List<MuscleGroup> updatedLargeMuscleGroups = new List<MuscleGroup>(largeMuscleGroups);

            var selectedIndex = plan.Sessions.FindIndex(i => i == session);

            var previousSession = (selectedIndex > 0) ? plan.Sessions[selectedIndex - 1] : null;

            if (previousSession != null && previousSession.Sections != null &&
                previousSession.Sections.FirstOrDefault().Exercises.Count > 1)
            {
                foreach (var mg in largeMuscleGroups)
                {
                    if (!previousSession.Sections.FirstOrDefault().Exercises.Any(i => i.PrimaryMuscleGroup.Name == mg.Name))
                    {
                        //put that large muscle group at the start of the list so it is choosen first.
                        updatedLargeMuscleGroups.Remove(mg);
                        updatedLargeMuscleGroups.Insert(0, mg);
                    }
                }
                //randomize the other of mini list so diff mg's get supersetted
                var rnd = new Random();
                var result = compulsaryMuscleGroups.OrderBy(item => rnd.Next());
                updatedLargeMuscleGroups.InsertRange(0, result);

            }

            var index = 0;

            foreach (var sec in session.Sections)
            {
                while (HasEnoughSets(session,sec.NoOfNormalSets))
                {

                    //add exercises
                    //still need to update below method to make it always be compound
                    AddNormalSetExercise(plan, session, sec, updatedLargeMuscleGroups[index], true);// sec.Exercises.Add(new ExerciseFilterer().NormalSetFilter(plan, session, sec, updatedLargeMuscleGroups[index], true));
                       //increment the MG
                        index++;

                        //UpdateUsedTimeAfterAddingExerciseNotUsedForCardio(plan.User, sec, SetType.Normal); < method already does this
                    
                }



                while (HasEnoughSets(session, sec.NoOfSuperSets))
                {
                    List<MuscleGroup> mgs = new List<MuscleGroup>() { updatedLargeMuscleGroups[index] };

                    
                    AddSuperSetExercise(plan, session, sec, new List<MuscleGroup>() { updatedLargeMuscleGroups[index] }, true);
                                      
                    index++;

                    //UpdateUsedTimeAfterAddingExerciseNotUsedForCardio(plan.User, sec, SetType.SuperSet);
                }
            }
        }

        private bool HasEnoughSets(Session session, int remainingSets)
        {
            if (remainingSets > 2)
            {
                return true;
            }

                    if (remainingSets == 2)
                    {
                        if (!session.IsOverPrescribed)
                        {
                            session.IsOverPrescribed = true;
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }

                            if (remainingSets < 2)
                            {
                                return false;
                            }
            return false;
        }

        public void SetDropSetExerciseForBodyweightTraining(Exercise exercise)
        {
            if (exercise.DropsetVariationExercise != null)
                throw new InvalidOperationException();
            exercise.DropsetVariationExercise = new ExerciseFilterer().ReturnEasierExercise(exercise);
        }

        public void UpdateUsedTimeAfterAddingExerciseNotUsedForCardio(User user, Section section, SetType setType)
        {
            if (setType == SetType.Normal)
            {
                var exerciseTime = new TimeSpan(00,00,Convert.ToInt32(user.SetTime.TotalSeconds * 3));
                section.UsedTime += exerciseTime;
            }
            else
            {
                var superSetTime = user.SetTime.Add(new TimeSpan(00, 00, Convert.ToInt32(user.SetTime.TotalSeconds * 0.25)));
                var exerciseTime = new TimeSpan(00, 00, Convert.ToInt32(superSetTime.TotalSeconds * 3));
                section.UsedTime += exerciseTime;
            }
        }

        public void CreateSuperSetLink(List<Exercise> exercises)
        { 
            foreach (var i in exercises)
            {
                i.IsSuperSetted = true;
                i.SiblingExerciseId = exercises.SingleOrDefault(ex => ex.Name != i.Name).Id;
            }
        }

        public void UpdateMuscleGroup(MuscleGroup oldMuscleGroup, MuscleGroup newMuscleGroup)
        {
            oldMuscleGroup.Id = newMuscleGroup.Id;
            oldMuscleGroup.Name = newMuscleGroup.Name;
            oldMuscleGroup.Size = newMuscleGroup.Size;
        }

        public void UpdateUserGoal(WeightGoalUpdateReason reason, Plan plan, WeightGoal weightGoal = WeightGoal.None)
        {
            if (reason == WeightGoalUpdateReason.UserOnCardioPlanButWantToGetBigger || reason == WeightGoalUpdateReason.UserOnCardioPlanButWantToLoose)
            {
                if (plan.WeightGoal != WeightGoal.GainMuscle && plan.Activity == Activity.Cardio)
                {
                    plan.WeightGoal = WeightGoal.GainMuscle;
                    plan.Activity = Activity.BodyweightTraining;
                }
                else
                {
                    if (plan.WeightGoal == WeightGoal.GainMuscle && plan.Activity == Activity.BodyweightTraining)
                    {
                        if (weightGoal != WeightGoal.None)
                        {
                            plan.WeightGoal = weightGoal;
                            plan.Activity = Activity.Cardio;
                        }
                    }
                }
            }
        }



    }
}
