﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppPrototype.CustomTypes;
using AppPrototype.Models.Enums;

namespace AppPrototype.Models.CustomTypes
{
    public class ExerciseFilterer
    {
        //Temp
        public Route Route = new Route("TEST");
        public List<Exercise> _exerciseList { get; set; }

        public ExerciseFilterer()
        {
            _exerciseList = new List<Exercise>()
            {//tri exs
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Dumbell, FitnessLevel = FitnessLevel.Beginner, Id = 654443, MovementType = 1, Name = "Kickbacks", CanHaveReps = true,
                    Popularity = 9.7f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Dumbell, FitnessLevel = FitnessLevel.Beginner, Id = 1552312, MovementType = 2, Name = "Extensions", CanHaveReps = true,
                    Popularity = 9.7f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps"),WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio ,Activity.BodyweightTraining,Activity.Weightlifting}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 65462, MovementType = 45, Name = "Close Grip Push Ups",  CanHaveReps = true,
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.ToneUp, WeightGoal.LooseFat}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio ,Activity.BodyweightTraining,Activity.Weightlifting}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 7514, MovementType = 5, Name = "Chair Dips",  CanHaveReps = true,
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle,WeightGoal.ToneUp, WeightGoal.LooseFat}
                },
                //bi exs
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Dumbell, FitnessLevel = FitnessLevel.Beginner, Id = 23455, MovementType = 1, Name = "Curls",  CanHaveReps = true,
                    Popularity = 9.7f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Biceps"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Biceps"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle,WeightGoal.ToneUp}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Dumbell, FitnessLevel = FitnessLevel.Beginner, Id = 453, MovementType = 41, Name = "Hammers",  CanHaveReps = true,
                    Popularity = 3.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Biceps"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Biceps"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle,WeightGoal.ToneUp}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Dumbell, FitnessLevel = FitnessLevel.Beginner, Id = 2322455, MovementType = 21, Name = "Concentration Curls",  CanHaveReps = true,
                    Popularity = 9.9f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Biceps"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Biceps"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle,WeightGoal.ToneUp}
                },
           
                //Shoulders
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Dumbell, FitnessLevel = FitnessLevel.Beginner, Id = 23, MovementType = 1, Name = "Shoulder Press",  CanHaveReps = true,
                    Popularity = 9.7f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Shoulders"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.ToneUp}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Dumbell, FitnessLevel = FitnessLevel.Beginner, Id = 113, MovementType = 2, Name = "Front Lat Press",  CanHaveReps = true,
                    Popularity = 9.4f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Shoulders"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.ToneUp}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting},  CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Dumbell, FitnessLevel = FitnessLevel.Beginner, Id = 7653, MovementType = 3, Name = "Side Lat Press",  CanHaveReps = true,
                    Popularity = 9.98f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Shoulders"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.ToneUp}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting, Activity.BodyweightTraining,Activity.Cardio}, FamilyNo  = 1000,  CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Dumbell, FitnessLevel = FitnessLevel.Beginner, Id = 4533, MovementType = 3, Name = "Pike Push Ups",  CanHaveReps = true,
                    Popularity = 9.98f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Shoulders"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.ToneUp, WeightGoal.LooseFat}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting, Activity.BodyweightTraining,  Activity.Cardio}, FamilyNo  = 1000, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Dumbell, FitnessLevel = FitnessLevel.Advanced, Id = 4533, MovementType = 98, Name = "Elevated Pike Push Ups",  CanHaveReps = true,
                    Popularity = 9.98f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Shoulders"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.ToneUp, WeightGoal.LooseFat}
                },

                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting},  CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Dumbell, FitnessLevel = FitnessLevel.Beginner, Id = 4353, MovementType = 4, Name = "Rear  Delt Press",  CanHaveReps = true,
                    Popularity = 9.1f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Shoulders"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.ToneUp}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Dumbell, FitnessLevel = FitnessLevel.Beginner, Id = 112212, MovementType = 6, Name = "Arnold Presses",  CanHaveReps = true,
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Shoulders"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle}
                },
                //Chest
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting},  CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Dumbell, FitnessLevel = FitnessLevel.Beginner, Id = 885, MovementType = 12, Name = " Incline Dumbell Press",  CanHaveReps = true,
                    Popularity = 9.1f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.LooseFat, WeightGoal.ToneUp}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting},  CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Dumbell, FitnessLevel = FitnessLevel.Intermediate, Id = 342566, MovementType = 3, Name = "Pull Overs",  CanHaveReps = true,
                    Popularity = 9.1f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Back"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.ToneUp}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting},  CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Dumbell, FitnessLevel = FitnessLevel.Beginner, Id = 3345, MovementType = 4, Name = "Vertical Chest Press",  CanHaveReps = true,
                    Popularity = 4.56f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.ToneUp}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting},  CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Dumbell, FitnessLevel = FitnessLevel.Beginner, Id = 5546, MovementType = 6, Name = "Flat Smith Machine Press",  CanHaveReps = true,
                    Popularity = 9.4f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting},  CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Barbell, FitnessLevel = FitnessLevel.Beginner, Id = 66674, MovementType = 16, Name = "Chest Decline Barbell Press",  CanHaveReps = true,
                    Popularity = 10.8f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting},  CompOrIso = CompOrIso.Isolation, EquipmentType = EquipmentType.Machine, FitnessLevel = FitnessLevel.Beginner, Id = 33456, MovementType = 7, Name = "Cable Flies",  CanHaveReps = true,
                    Popularity = 7.6f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.ToneUp}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting, Activity.BodyweightTraining,  Activity.Cardio},  CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 33456, MovementType = 97, Name = "Kneeling Push Up",  CanHaveReps = true,
                    Popularity = 7.6f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.ToneUp, WeightGoal.LooseFat}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting, Activity.BodyweightTraining, Activity.Cardio}, FamilyNo = 1002,  CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 33456, MovementType = 6546, Name = "Wide Grip Push Up",  CanHaveReps = true,
                    Popularity = 7.6f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.ToneUp, WeightGoal.LooseFat}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting,  Activity.BodyweightTraining,  Activity.Cardio}, FamilyNo = 1002,  CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Intermediate, Id = 33456, MovementType = 2347, Name = "Elevated Wide Grip Push Up",  CanHaveReps = true,
                    Popularity = 7.6f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.ToneUp, WeightGoal.LooseFat}
                },

          //Back
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting},  CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Dumbell, FitnessLevel = FitnessLevel.Beginner, Id = 324452, MovementType = 3, Name = "DB Bent Rows",  CanHaveReps = true,
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Back"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Biceps"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting},  CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.KettelBell, FitnessLevel = FitnessLevel.Beginner, Id = 1411, MovementType = 6, Name = "Single Arm Bell Rows",  CanHaveReps = true,
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Back"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Biceps"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio ,Activity.BodyweightTraining,  Activity.Cardio}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 31321, MovementType = 2, Name = "Reverse Snow Angels",  CanHaveReps = true,
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Back"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.ToneUp, WeightGoal.LooseFat }
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Weightlifting, Activity.Cardio ,Activity.BodyweightTraining}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Dumbell, FitnessLevel = FitnessLevel.Beginner, Id = 33212, MovementType = 453, Name = "Pull Downs",  CanHaveReps = true,
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Back"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Biceps"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.ToneUp, WeightGoal.LooseFat }
                },

                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio, Activity.BodyweightTraining, Activity.Weightlifting,  Activity.Cardio}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 312, MovementType = 76, Name = "Stiff Leg Back Extensions",  CanHaveReps = true,
                    Popularity = 6.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Back"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.ToneUp, WeightGoal.LooseFat, WeightGoal.GainMuscle }
                },
                //Qauds
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio ,Activity.BodyweightTraining,Activity.Weightlifting},  CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 773245241, MovementType = 1, Name = "Piston Sqauts",  CanHaveReps = true,
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Qauds"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.LooseFat, WeightGoal.ToneUp}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio ,Activity.BodyweightTraining,Activity.Weightlifting},  CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 43123241, MovementType = 2, Name = "Sqauts",  CanHaveReps = true,
                    Popularity = 9.9f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Qauds"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.LooseFat, WeightGoal.ToneUp}
                },
                 new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio ,Activity.BodyweightTraining,Activity.Weightlifting},  CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 42243241, MovementType = 345, Name = "Lunges",  CanHaveReps = true,
                    Popularity = 9.9f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Qauds"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.LooseFat, WeightGoal.ToneUp}
                },
                 new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio ,Activity.BodyweightTraining,Activity.BodyweightTraining},  CompOrIso = CompOrIso.Isolation, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 42243241, MovementType = 4543, Name = "Single Leg Lunges",  CanHaveReps = true,
                    Popularity = 9.9f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Qauds"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.LooseFat, WeightGoal.ToneUp}
                },
                 new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio ,Activity.BodyweightTraining,Activity.BodyweightTraining},  CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 42243241, MovementType = 33425, Name = "Elevated Lunges",  CanHaveReps = true,
                    Popularity = 9.9f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Qauds"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.LooseFat, WeightGoal.ToneUp}
                },
                 new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio ,Activity.BodyweightTraining,Activity.BodyweightTraining},  CompOrIso = CompOrIso.Isolation, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 42243241, MovementType = 334562, Name = "Sumo Sqauts",  CanHaveReps = true,
                    Popularity = 9.9f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Qauds"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.LooseFat, WeightGoal.ToneUp}
                },
                 new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio ,Activity.BodyweightTraining,Activity.BodyweightTraining},  CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 42243241, MovementType = 1233, Name = "Seated Leg Extensions",  CanHaveReps = true,
                    Popularity = 9.9f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Qauds"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.LooseFat, WeightGoal.ToneUp}
                },
                //Hamms
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio ,Activity.BodyweightTraining,Activity.Weightlifting},  CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 434341, MovementType = 4, Name = "Straight Leg Deadlifts",  CanHaveReps = true,
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Hamms"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.LooseFat, WeightGoal.ToneUp}
                },
                   new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio ,Activity.BodyweightTraining,Activity.Weightlifting},  CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 436443241, MovementType = 5, Name = "Crab Walks", 
                    Popularity = 9.9f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Hamms"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.LooseFat, WeightGoal.ToneUp}
                },
                //Calves
                  new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio ,Activity.BodyweightTraining,Activity.Weightlifting}, FamilyNo = 1003, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 436443241, MovementType = 5, Name = "Standing Calve Raises",  CanHaveReps = true,
                    Popularity = 9.9f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Calves"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.LooseFat, WeightGoal.ToneUp}
                },
                 new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio ,Activity.BodyweightTraining,Activity.Weightlifting}, FamilyNo = 1003, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Intermediate, Id = 43655241, MovementType = 25, Name = "Single Leg Standing Calve Raises",  CanHaveReps = true,
                    Popularity = 9.9f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Calves"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.LooseFat, WeightGoal.ToneUp}
                },
               new Exercise()                                                                                            {
                    Activities = new List<Activity>(){Activity.Cardio ,Activity.BodyweightTraining,Activity.Weightlifting}, FamilyNo = 1004, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 43655241, MovementType = 325, Name = "Seated Calve Raises",  CanHaveReps = true,
                    Popularity = 9.9f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Calves"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.LooseFat, WeightGoal.ToneUp}
                },
               //Core
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio, Activity.BodyweightTraining,Activity.Weightlifting}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 154354338, MovementType = 5, Name = "Plank", 
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Core"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.LooseFat, WeightGoal.ToneUp}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio, Activity.BodyweightTraining,Activity.Weightlifting}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 3121, MovementType = 4, Name = "Mountain Climbers", 
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Core"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.ToneUp,WeightGoal.LooseFat}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio, Activity.BodyweightTraining}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 312121231, MovementType = 432, Name = "Scissor Kicks", 
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Core"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.ToneUp,WeightGoal.LooseFat}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio, Activity.BodyweightTraining}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 23452, MovementType = 412, Name = "Diagonal Crunches",  CanHaveReps = true,
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Core"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.ToneUp,WeightGoal.LooseFat, WeightGoal.GainMuscle}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio, Activity.BodyweightTraining}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 33521231, MovementType = 4312, Name = "Standing High Knees", 
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Core"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.ToneUp,WeightGoal.LooseFat}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio, Activity.BodyweightTraining}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 3451, MovementType = 98, Name = "Decline Crunches",  CanHaveReps = true,
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Core"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.ToneUp,WeightGoal.LooseFat, WeightGoal.GainMuscle}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio, Activity.BodyweightTraining}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 31131, MovementType = 48, Name = "Sit Ups",  CanHaveReps = true,
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Core"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.ToneUp,WeightGoal.LooseFat, WeightGoal.GainMuscle}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio, Activity.BodyweightTraining}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 2131, MovementType =748, Name = "Assisted Sit Ups",  CanHaveReps = true,
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Core"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.ToneUp,WeightGoal.LooseFat}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio, Activity.BodyweightTraining}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Intermediate, Id = 29565, MovementType =8, Name = "Side Plank", 
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Core"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.ToneUp,WeightGoal.LooseFat}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio, Activity.BodyweightTraining}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Intermediate, Id = 29005, MovementType =83, Name = "Diagonal Plank",
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Core"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.ToneUp,WeightGoal.LooseFat}
                },
                                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio, Activity.BodyweightTraining}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Intermediate, Id = 22775, MovementType =4483, Name = "Elevated Plank",
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Core"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.ToneUp,WeightGoal.LooseFat, WeightGoal.GainMuscle}
                },
                                                     new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio, Activity.BodyweightTraining}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Intermediate, Id = 22775, MovementType =4483, Name = "Elevated Diagonal Plank",
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Core"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.ToneUp,WeightGoal.LooseFat, WeightGoal.GainMuscle}
                },


                //Glute exs
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio , Activity.BodyweightTraining,Activity.Weightlifting}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 176118, MovementType = 15454, Name = "Hip Thrusts",  CanHaveReps = true,
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Glutes"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Hamms"), WeightGoals = new List<WeightGoal>(){WeightGoal.LooseFat, WeightGoal.ToneUp}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio, Activity.BodyweightTraining,Activity.Weightlifting}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 1763118, MovementType = 1232, Name = "Side Skaters",
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Glutes"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Hamms"), WeightGoals = new List<WeightGoal>(){WeightGoal.LooseFat, WeightGoal.ToneUp}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio, Activity.BodyweightTraining,Activity.Weightlifting}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 1765118, MovementType = 34321, Name = "Diagonal Squats",  CanHaveReps = true,
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Glutes"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Hamms"), WeightGoals = new List<WeightGoal>(){WeightGoal.LooseFat, WeightGoal.ToneUp}
                },
                //Full Exercises
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio,  Activity.BodyweightTraining, Activity.Weightlifting}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 1132423418, MovementType = 234232, Name = "Star Jumps",
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.ToneUp, WeightGoal.LooseFat}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio ,Activity.BodyweightTraining,  Activity.Weightlifting}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Bodyweight, FitnessLevel = FitnessLevel.Beginner, Id = 23423, MovementType = 52, Name = "Burpes",  CanHaveReps = true,
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.ToneUp, WeightGoal.LooseFat}
                },
                new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio ,Activity.BodyweightTraining,  Activity.Weightlifting}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Dumbell, FitnessLevel = FitnessLevel.Beginner, Id = 332, MovementType = 3, Name = "Sqaut Jumps",  CanHaveReps = true,
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Qauds"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.ToneUp, WeightGoal.LooseFat}
                },
               new Exercise()
                {
                    Activities = new List<Activity>(){Activity.Cardio ,Activity.BodyweightTraining,  Activity.Weightlifting}, CompOrIso = CompOrIso.Compound, EquipmentType = EquipmentType.Dumbell, FitnessLevel = FitnessLevel.Beginner, Id = 32332, MovementType = 903, Name = "High Knees",
                    Popularity = 9.2f, PrimaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Full"), SecondaryMuscleGroup = Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Core"), WeightGoals = new List<WeightGoal>(){WeightGoal.GainMuscle, WeightGoal.ToneUp, WeightGoal.LooseFat}
                }
            };
        }

        public Exercise FindSingle(string v)
        {
            throw new NotImplementedException();
        }

        public Exercise FindSingle(string exerciseName, MuscleGroup mg, GymOrHome gymOrHome, WeightGoal weightGoal)
        {
            //FIND THIS METHOD ON THE OTHER VERSION OF APP PROTOTYPE
            throw new NotImplementedException();
        }

        public Exercise NormalSetFilter(Plan plan, Session session, Section section, MuscleGroup muscleGroup, bool mustBeCompound = false)
        {
            List<Exercise> filteredList = null;

            if (mustBeCompound)
            {
                //add filters with comp only
                filteredList = (plan.InjuryList.Count > 0) ? _exerciseList.Where(i => i.CompOrIso == CompOrIso.Compound &&
                                                        i.Activities.Contains(plan.Activity) &&
                                                        i.PrimaryMuscleGroup.Name == muscleGroup.Name &&
                                                        GetAvailibleFitnessLevels(i).Contains(i.FitnessLevel) &&
                                                      !section.Exercises.Any(ex=>ex.Name == i.Name) &&
                                                        !section.Exercises.Any(exercise => exercise.MovementType == i.MovementType && exercise.PrimaryMuscleGroup.Name == i.Name) &&
                                                        i.WeightGoals.Contains(plan.WeightGoal) && !plan.InjuryList.Any(mg=>mg.Name == i.PrimaryMuscleGroup.Name) &&
                                                        !plan.InjuryList.Contains(i.SecondaryMuscleGroup)).ToList()
                                                        
                                  : _exerciseList.Where(i => i.CompOrIso == CompOrIso.Compound && i.Activities.Contains(plan.Activity) &&
                                                             i.PrimaryMuscleGroup.Name == muscleGroup.Name &&
                                                             GetAvailibleFitnessLevels(i).Contains(i.FitnessLevel) &&
                                                             !section.Exercises.Any(ex => ex.Name == i.Name) &&
                                                             !section.Exercises.Any(exercise => exercise.MovementType == i.MovementType &&
                                                             exercise.PrimaryMuscleGroup.Name == i.Name) && i.WeightGoals.Contains(plan.WeightGoal) &&
                                                             !plan.InjuryList.Contains(i.SecondaryMuscleGroup)).ToList();

            }                                               
            else
            {
                
                filteredList = (plan.InjuryList.Count > 0) ? 
                                                             _exerciseList.Where(i => i.Activities.Contains(plan.Activity) &&
                                                             i.PrimaryMuscleGroup.Name == muscleGroup.Name &&
                                                             GetAvailibleFitnessLevels(i).Contains(i.FitnessLevel) &&
                                                                                      !section.Exercises.Any(ex => ex.Name == i.Name) &&
                                                             !section.Exercises.Any(exercise => exercise.MovementType == i.MovementType && exercise.PrimaryMuscleGroup.Name == i.Name) &&
                                                             i.WeightGoals.Contains(plan.WeightGoal) && !plan.InjuryList.Any(mg => mg.Name == i.PrimaryMuscleGroup.Name) &&
                                                             !plan.InjuryList.Contains(i.SecondaryMuscleGroup)).ToList()

                    : _exerciseList.Where(i => i.Activities.Contains(plan.Activity) &&
                                               i.PrimaryMuscleGroup.Name == muscleGroup.Name &&
                                               GetAvailibleFitnessLevels(i).Contains(i.FitnessLevel) &&
                                               !section.Exercises.Any(ex => ex.Name == i.Name) &&
                                               !section.Exercises.Any( exercise => exercise.MovementType == i.MovementType &&
                                               exercise.PrimaryMuscleGroup.Name ==i.Name) && i.WeightGoals.Contains(plan.WeightGoal)).ToList();

            }

            // add more filters for home suitibility
            if (plan.GymOrHome == GymOrHome.Home)
            {
                //test this part
                filteredList = filteredList.Where(i => plan.EquipmentList.Any(eq => eq.Synonyms.Contains(i.EquipmentType.ToString())) || i.EquipmentType == EquipmentType.Bodyweight).ToList();
            }

            if (session.ActivityType == Activity.Cardio)
            {

                if (plan.DaysPerWeek <= 3)
                {
                    //filteredList =
                    //    filteredList.Where(
                    //        i => !plan.Sessions.Any(
                    //            ses => ses.Sections.Any(sec => sec.Exercises.Any(ex => ex.Name == i.Name)))).ToList();
                }
                else
                {
                    var selectedIndex = plan.Sessions.FindIndex(i => i == session);
                    var previousSession = (selectedIndex > 0) ? plan.Sessions[selectedIndex - 1] : null;

                    if (previousSession != null && previousSession.Sections.FirstOrDefault().Exercises.Count > 0)
                    {
                        filteredList = filteredList.Where(i => !previousSession.Sections.FirstOrDefault().Exercises.Any(ex => ex.Name == i.Name)).ToList();
                    }

                }
                
            }

            new IntegrityChecker().CheckAndAct(SetType.Normal, _exerciseList,filteredList,plan,session,section, mustBeCompound);
            return filteredList.OrderByDescending(i=>i.Popularity).FirstOrDefault();
        }


        public List<Exercise> SuperSetFilter(Plan plan, Session session, Section section, List<MuscleGroup> muscleGroups, bool mustBeCompound = false)
        {
            List<Exercise> filteredList = null;
            List<Exercise> multipleExercises = new List<Exercise>();

            var selectedEquipmentTypeList = new List<EquipmentType>() { EquipmentType.Bodyweight, EquipmentType.Dumbell, EquipmentType.KettelBell, EquipmentType.Barbell };
          
            if (muscleGroups.Count > 1)
            {
                foreach (var muscleGroup in muscleGroups)
                {
                    filteredList = (plan.InjuryList.Count > 0)
                        ? _exerciseList.Where(i => i.Activities.Contains(plan.Activity) &&
                                                   i.PrimaryMuscleGroup.Name == muscleGroup.Name &&
                                                   GetAvailibleFitnessLevels(i).Contains(i.FitnessLevel) &&
                                                   !section.Exercises.Any(ex => ex.Name == i.Name) &&
                                                   !section.Exercises.Any(
                                                       exercise => exercise.MovementType == i.MovementType &&
                                                                   exercise.PrimaryMuscleGroup.Name ==
                                                                   i.Name) &&
                                                   i.WeightGoals.Contains(plan.WeightGoal) &&
                                                   !plan.InjuryList.Any(
                                                       mg => mg.Name == i.PrimaryMuscleGroup.Name) &&
                                                   !plan.InjuryList.Contains(i.SecondaryMuscleGroup)).ToList()

                        : _exerciseList.Where(i => i.Activities.Contains(plan.Activity) &&
                                                   i.PrimaryMuscleGroup.Name == muscleGroup.Name &&
                                                   GetAvailibleFitnessLevels(i).Contains(i.FitnessLevel) &&
                                                   !section.Exercises.Any(ex => ex.Name == i.Name) &&
                                                   !section.Exercises.Any(
                                                       exercise => exercise.MovementType == i.MovementType &&
                                                                   exercise.PrimaryMuscleGroup.Name ==
                                                                   i.Name) &&
                                                   i.WeightGoals.Contains(plan.WeightGoal)).ToList();




                    if (mustBeCompound)
                    {
                        //add filters with comp only
                        filteredList = filteredList.Where(i => i.CompOrIso == CompOrIso.Compound).ToList();
                    }

                    if (plan.GymOrHome == GymOrHome.Gym)
                    {
                        filteredList = filteredList.Where(i => selectedEquipmentTypeList.Any(eq => eq.ToString() == i.EquipmentType.ToString())).ToList();
                    }
                    else
                    {
                        // add more filters for home suitibility
                        //test this part
                        filteredList = filteredList
                            .Where(i => plan.EquipmentList.Any(eq => eq.Synonyms.Contains(i.EquipmentType.ToString())) || i.EquipmentType == EquipmentType.Bodyweight).ToList();
                    }

                    new IntegrityChecker().CheckAndAct(SetType.SuperSet, _exerciseList, filteredList, plan, session, section, mustBeCompound,null, multipleExercises);
                    multipleExercises.Add(filteredList.OrderByDescending(i => i.Popularity).FirstOrDefault());
                }

                return multipleExercises;
            }
            else
            {
               
                    filteredList = _exerciseList.Where(i => i.Activities.Contains(plan.Activity) &&
                                                            i.PrimaryMuscleGroup.Name == muscleGroups.FirstOrDefault().Name &&
                                                            GetAvailibleFitnessLevels(i).Contains(i.FitnessLevel) &&
                                                            !section.Exercises.Any(ex => ex.Name == i.Name) &&
                                                            !section.Exercises.Any(
                                                                exercise => exercise.MovementType == i.MovementType &&
                                                                            exercise.PrimaryMuscleGroup.Name ==
                                                                            i.Name) &&
                                                            i.WeightGoals.Contains(plan.WeightGoal) &&
                                                            !plan.InjuryList.Any(
                                                                mg => mg.Name == i.PrimaryMuscleGroup.Name) &&
                                                            !plan.InjuryList.Contains(i.SecondaryMuscleGroup)).ToList();

                    if (mustBeCompound)
                    {
                        //add filters with comp only
                        filteredList = filteredList.Where(i => i.CompOrIso == CompOrIso.Compound).ToList();
                    }
                    

                    if (plan.GymOrHome == GymOrHome.Gym)
                    {
                        filteredList = filteredList.Where(i => selectedEquipmentTypeList.Any(eq => eq.ToString() == i.EquipmentType.ToString())).ToList();
                    }
                    else
                    {
                        // add more filters for home suitibility
                        //test this part
                        filteredList = filteredList.Where(i => plan.EquipmentList.Any(eq => eq.Synonyms.Contains(i.EquipmentType.ToString())) || i.EquipmentType == EquipmentType.Bodyweight).ToList();
                    }

                    new IntegrityChecker().CheckAndAct(SetType.SuperSet, _exerciseList, filteredList, plan, session, section, mustBeCompound);
                    multipleExercises.Add(filteredList.OrderByDescending(i => i.Popularity).FirstOrDefault());
                    multipleExercises.Add(filteredList
                        .Where(i => i.MovementType != multipleExercises.FirstOrDefault().MovementType)
                        .OrderByDescending(i => i.Popularity).FirstOrDefault());
                    return multipleExercises;
                }
            
        }



        //Test this method im unsure of it
        private List<FitnessLevel> GetAvailibleFitnessLevels(Exercise exercise)
        {
            if (exercise.FitnessLevel == FitnessLevel.Advanced)
                return new List<FitnessLevel>(){FitnessLevel.Advanced,FitnessLevel.Intermediate,FitnessLevel.Beginner};
            if (exercise.FitnessLevel == FitnessLevel.Intermediate)
                return new List<FitnessLevel>() { FitnessLevel.Intermediate, FitnessLevel.Beginner };
            if (exercise.FitnessLevel == FitnessLevel.Beginner)
                return new List<FitnessLevel>() { FitnessLevel.Beginner };

            return null;
        }

        public Exercise ReturnEasierExercise(Exercise exercise)
        {
            var first = _exerciseList.FirstOrDefault(i => i.FamilyNo == exercise.FamilyNo &&
                                               Convert.ToInt32(i.FitnessLevel) <
                                               Convert.ToInt32(exercise.FitnessLevel));
            return first;

        }


        public List<Exercise> GetExercises(MuscleGroup muscleGroup, WeightGoal weightGoal, FitnessLevel fitnessLevel, GymOrHome gymOrHome)
        {
            var list = _exerciseList.Where(i => i.PrimaryMuscleGroup == muscleGroup && i.WeightGoals.Contains(weightGoal) && GetAvailibleFitnessLevels(i).Any(s => s == fitnessLevel));
            var filteredList = list;

            if (gymOrHome == GymOrHome.Home)
            {
                filteredList = list.Where(i => i.EquipmentType == EquipmentType.Bodyweight);

                if (filteredList.Count() < 3 || filteredList == null)
                    list = _exerciseList.Where(i => i.PrimaryMuscleGroup == muscleGroup);
               // filteredList = list.Where(i => i.EquipmentType == EquipmentType.Bodyweight);
                return filteredList.ToList();
            }

            if (filteredList.Count() < 3 || filteredList == null)
            {
                list = _exerciseList.Where(i => i.PrimaryMuscleGroup == muscleGroup);
                return list.ToList();
            }

            return _exerciseList;
        }
    }
}
