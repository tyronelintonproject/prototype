﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppPrototype.Models.CustomTypes.Food
{
    [Serializable]
    public class Breakfast : MealObject
    {
        public List<FoodItem> BreakfastProteins { get; set; }
        public List<FoodItem> BreakfastCarbs { get; set; }
        public List<FoodItem> BreakfastFats { get; set; }
        public List<FoodItem> BreakfastVeg { get; set; }
        public List<FoodItem> BreakfastFruit { get; set; }
        public List<FoodItem> Items { get { return BreakfastProteins.Concat(BreakfastCarbs).Concat(BreakfastFats).Concat(BreakfastVeg).Concat(BreakfastFruit).ToList(); } }

        public override string ToString()
        {
            return base.ToString();
        }

        public Breakfast()
        {
            BreakfastProteins = new List<FoodItem>();
            BreakfastCarbs = new List<FoodItem>();
            BreakfastFats = new List<FoodItem>();
            BreakfastVeg = new List<FoodItem>();
            BreakfastFruit = new List<FoodItem>();
        }

    }
}

