﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppPrototype.Models.CustomTypes.Food
{
    [Serializable]
    public class Dinner : MealObject
    {
        public List<FoodItem> DinnerProteins { get; set; }
        public List<FoodItem> DinnerCarbs { get; set; }
        public List<FoodItem> DinnerFats { get; set; }
        public List<FoodItem> DinnerVeg { get; set; }
        public List<FoodItem> DinnerFruit { get; set; }
        public new List<FoodItem> Items { get { return DinnerProteins.Concat(DinnerCarbs).Concat(DinnerFats).Concat(DinnerVeg).Concat(DinnerFruit).ToList(); } }


        public Dinner()
        {
            DinnerProteins = new List<FoodItem>();
            DinnerCarbs = new List<FoodItem>();
            DinnerFats = new List<FoodItem>();
            DinnerVeg = new List<FoodItem>();
            DinnerFruit = new List<FoodItem>();
        }


        public override string ToString()
        {
            return base.ToString();
        }

    }
}
