﻿using AppPrototype.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppPrototype.Models.CustomTypes.Food
{
    [Serializable]
    public class FoodItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double GramsOfProtein { get; set; }
        public double GramsOfCarbs { get; set; }
        public double GramsOfFat { get; set; }
        public double TotalMixGrams
        {
            get
            {
                return GramsOfCarbs + GramsOfProtein + GramsOfFat;
            }
        }
        public string Retailor { get; set; }
        public FoodType Type { get; set; }
        public List<MealType> AppropriateMeals { get; set; }
        public string Parent { get; set; }

        public FoodItem(int id, string name, double gramsOfProtein, double gramsOfCarbs, double gramsOfFat, FoodType foodType, List<MealType> apropriateMeals, string parent, string retailor = null)
        {
            Id = id;
            Name = name;
            GramsOfProtein = gramsOfProtein;
            GramsOfCarbs = gramsOfCarbs;
            GramsOfFat = gramsOfFat;
            Type = foodType;
            AppropriateMeals = apropriateMeals;
            Parent = parent;
            Retailor = retailor;
        }

    }
}

