﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppPrototype.Models.CustomTypes.Food
{
    [Serializable]
    public class Lunch : MealObject
    {
        public List<FoodItem> LunchMixs { get; set; }
        public List<FoodItem> LunchRetailorMixs { get; set; }
        public List<FoodItem> LunchProteins { get; set; }
        public List<FoodItem> LunchCarbs { get; set; }
        public List<FoodItem> LunchFats { get; set; }
        public List<FoodItem> LunchVeg { get; set; }
        public List<FoodItem> LunchFruit { get; set; }
        public List<FoodItem> Items { get { return LunchMixs.Concat(LunchRetailorMixs).Concat(LunchProteins).Concat(LunchCarbs).Concat(LunchFats)
        .Concat(LunchVeg).Concat(LunchFruit).ToList(); } }



        public override string ToString()
        {
            return base.ToString();
        }

        public Lunch()
            
        {
            LunchMixs = new List<FoodItem>();
            LunchProteins = new List<FoodItem>();
            LunchCarbs = new List<FoodItem>();
            LunchFats = new List<FoodItem>();
            LunchVeg = new List<FoodItem>();
            LunchFruit = new List<FoodItem>();
            LunchRetailorMixs = new List<FoodItem>();
        }

    }
}
