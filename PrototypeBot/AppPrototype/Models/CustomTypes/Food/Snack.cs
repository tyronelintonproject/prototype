﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppPrototype.Models.CustomTypes.Food
{
    [Serializable]
    public class Snack : MealObject
    {
        public List<FoodItem> SnackProteins { get; set; }
        public List<FoodItem> SnackCarbs { get; set; }
        public List<FoodItem> SnackFats { get; set; }
        public List<FoodItem> SnackVeg { get; set; }
        public List<FoodItem> SnackFruit { get; set; }
        public List<FoodItem> SnackMixes { get; set; }
        public List<FoodItem> Items { get { return SnackProteins.Concat(SnackCarbs).Concat(SnackFats).Concat(SnackVeg).Concat(SnackFruit).Concat(SnackMixes).ToList(); } }


        public Snack()
        {
            SnackProteins = new List<FoodItem>();
            SnackCarbs = new List<FoodItem>();
            SnackFats = new List<FoodItem>();
            SnackVeg = new List<FoodItem>();
            SnackFruit = new List<FoodItem>();
            SnackMixes = new List<FoodItem>();
        }

        public override string ToString()
        {
            return base.ToString();
        }

    }
}
