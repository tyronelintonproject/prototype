﻿using System;
using System.Collections.Generic;

namespace AppPrototype.Models.CustomTypes
{
    [Serializable]
    public class Goal
    {
        public DateTime StartDate { get; set; }
        public DateTime ExpectedEndDate { get; set; }
        public string Name { get; set; }
        public MuscleGroup MuscleGroup { get; set; }
        public List<SessionScore> SessionScores { get; set; }
    }
}
