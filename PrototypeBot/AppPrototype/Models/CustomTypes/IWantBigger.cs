﻿using AppPrototype.CustomTypes;
using AppPrototype.Models.Enums;
using Bot_Application1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppPrototype.Models.CustomTypes
{
    [Serializable]
    public class IWantBigger
    {
        private Route Route = new Route("fgf");

        public void Test(Plan plan)
        {
            //plan.Sessions.FirstOrDefault().Level = Level.Hard;
           // plan.InjuryPreferedReplacement = new MuscleGroup() { Name = "Shoulders" };
            //session.ActivityType = Activity.Cardio;
            //plan.Sessions.FirstOrDefault().Sections.FirstOrDefault().AvailableTime = new TimeSpan(00, 10, 00);
        }

        public Plan WantBigger(Plan plan, MuscleGroup muscleGroup, ResultExpectancy resultExpectancy, List<Exercise> dontIncludeTheseExercises = null)
        {  



            //this should work for any activity type
            //if (plan.Activity == Activity.BodyweightTraining || plan.Activity == Activity.Cardio)
            //{
                //if the user has already prioritzed this muscle group 
                if (plan.Sessions.Where(i => i.PrioritizedMuscleGroup != null && i.PrioritizedMuscleGroup.Name == muscleGroup.Name).Count() >= 2)
                    throw new Exception();

                var sessionsContainingSelectedMG = new List<Session>();
                int noOfTimesPW = 0;

                //this code works for any no of days per week , it should do
                //if (plan.DaysPerWeek <= 3)
                //{
                //prioritize 2 sessions
                foreach (var session in plan.Sessions)
                {   //Check if we already prioritized this muscle group
                    if (plan.Sessions.Where(i => i.PrioritizedMuscleGroup != null && i.PrioritizedMuscleGroup.Name == muscleGroup.Name).Count() < 2)
                    {
                        foreach (var section in session.Sections)
                        {
                            //check if we train the mg in that session
                            if (section.Exercises.Any(ex =>  ex.PrimaryMuscleGroup.Name == muscleGroup.Name))
                            {
                                noOfTimesPW++;
                                sessionsContainingSelectedMG.Add(session);
                                //only prioritize 2 sessions not all of them
                                if (noOfTimesPW <= 2)
                                {
                                    session.PrioritizedMuscleGroup = Route.MuscleGroups.SingleOrDefault(smg => smg.Name == muscleGroup.Name);
                                }
                            }
                        }
                    }
                }
                //check if we now have 2 prioritzed sessions if not then enter method and make 2 priortized sessions
                if (plan.Sessions.Where(i => i.PrioritizedMuscleGroup != null && i.PrioritizedMuscleGroup.Name == muscleGroup.Name).Count() < 2)
                {//check if we have 1 prioritized session if not aka we have 0 prioritize the 2 least important sessions
                    if (plan.Sessions.Where(i => i.PrioritizedMuscleGroup != null && i.PrioritizedMuscleGroup.Name == muscleGroup.Name).Count() == 1)
                    {
                        var sessionToPrioritize = GetLeastImportantSession(plan, plan.Sessions.Where(ses => ses.PrioritizedMuscleGroup == null).ToList());
                        sessionToPrioritize.PrioritizedMuscleGroup = muscleGroup;
                    }
                    else
                    {
                        var sessionToPrioritize1 = GetLeastImportantSession(plan, plan.Sessions.Where(ses => ses.PrioritizedMuscleGroup == null).ToList());
                        sessionToPrioritize1.PrioritizedMuscleGroup = muscleGroup;
                        var sessionToPrioritize2 = GetLeastImportantSession(plan, plan.Sessions.Where(ses => ses.PrioritizedMuscleGroup == null && ses != sessionToPrioritize1).ToList());
                        sessionToPrioritize2.PrioritizedMuscleGroup = muscleGroup;
                    }
                }
                //}
                //else
                //{
                //    foreach (var session in plan.Sessions)
                //    {   //Check if we already prioritized this muscle group
                //        if (plan.Sessions.Where(i => i.PrioritizedMuscleGroup == muscleGroup).Count() < 2)
                //        {
                //            foreach (var section in session.Sections)
                //            {

                //                foreach (var mg in section.MuscleGroups)
                //                {
                //                    //check if were training that muscle group in that session and if we are then prioritze it
                //                    if (mg.Name == muscleGroup.Name)
                //                    {
                //                        session.PrioritizedMuscleGroup = Route.MuscleGroups.SingleOrDefault(smg => smg.Name == muscleGroup.Name);
                //                        sessionsContainingSelectedMG.Add(session);
                //                        noOfTimesPW++;
                //                    }
                //                }

                //            }
                //        }
                //    }
                //}


                if (resultExpectancy == ResultExpectancy.ASAP)
                {
                    switch (plan.SessionLength.TotalMinutes)
                    {
                        case 15:
                            IWantBigger30MinsOrLess(plan, muscleGroup, noOfTimesPW, sessionsContainingSelectedMG, resultExpectancy, dontIncludeTheseExercises);
                            break;
                        case 30:
                            IWantBigger30MinsOrLess(plan, muscleGroup, noOfTimesPW, sessionsContainingSelectedMG, resultExpectancy, dontIncludeTheseExercises);
                            break;
                        case 45:
                            IWantBigger45MinsOrGreater(plan, muscleGroup, noOfTimesPW, sessionsContainingSelectedMG, resultExpectancy, dontIncludeTheseExercises);
                            break;
                        case 60:
                            IWantBigger45MinsOrGreater(plan, muscleGroup, noOfTimesPW, sessionsContainingSelectedMG, resultExpectancy, dontIncludeTheseExercises);
                            break;
                    }
                }
                else
                {
                    switch (plan.SessionLength.TotalMinutes)
                    {
                        case 15:
                            IWantBigger30MinsOrLess(plan, muscleGroup, noOfTimesPW, sessionsContainingSelectedMG, resultExpectancy, dontIncludeTheseExercises);
                            break;
                        case 30: //if they dont want it asap dont decicate 2 sessions.
                            IWantBigger45MinsOrGreater(plan, muscleGroup, noOfTimesPW, sessionsContainingSelectedMG, resultExpectancy, dontIncludeTheseExercises);
                            break;
                        case 45:
                            IWantBigger45MinsOrGreater(plan, muscleGroup, noOfTimesPW, sessionsContainingSelectedMG, resultExpectancy, dontIncludeTheseExercises);
                            break;
                        case 60:
                            IWantBigger45MinsOrGreater(plan, muscleGroup, noOfTimesPW, sessionsContainingSelectedMG, resultExpectancy, dontIncludeTheseExercises);
                            break;
                    }
                }

            return plan;
        }


        public void IWantBigger30MinsOrLess(Plan plan, MuscleGroup muscleGroup, int noOfTimesPW, List<Session> sessionsContainingSelectedMG, ResultExpectancy resultExpectancy, List<Exercise> dontIncludeTheseExercises)
        {
            //if theyre already training them twice or more a week
            if (noOfTimesPW >= 2)

            {// if weve only got one or less session priotrized
                if (sessionsContainingSelectedMG.Where(session => session.PrioritizedMuscleGroup == muscleGroup).Count() <= 1)
                {
                    //we want to re-create 2 sessions and ensure both are prioritized for the selected MG
                    //priortize session 1
                    new SessionCreator().Dedicate(plan, sessionsContainingSelectedMG.FirstOrDefault(), sessionsContainingSelectedMG, muscleGroup, resultExpectancy,
                        dontIncludeTheseExercises, Caller.IWantBigger);
                    //remove the above from the list
                    sessionsContainingSelectedMG.RemoveAt(0);
                    //priortize session 2
                    new SessionCreator().Dedicate(plan, sessionsContainingSelectedMG.FirstOrDefault(), sessionsContainingSelectedMG, muscleGroup, resultExpectancy,
                        dontIncludeTheseExercises, Caller.IWantBigger);
                }
            }
            if (noOfTimesPW == 1)
            {// if weve only got one or less session priotrized
                if (sessionsContainingSelectedMG.Where(session => session.PrioritizedMuscleGroup == muscleGroup).Count() <= 1)
                {
                    //we want to re-create the sole sessionWithHasTheSelectedMG 
                    //priortize session 1
                    new SessionCreator().Dedicate(plan, sessionsContainingSelectedMG.FirstOrDefault(), sessionsContainingSelectedMG, muscleGroup, resultExpectancy,
                        dontIncludeTheseExercises);

                    //then we want to find another session with the least impact on the user to replace
                    new SessionCreator().Dedicate(plan, GetLeastImportantSession(plan, plan.Sessions.Where(i => i != sessionsContainingSelectedMG.FirstOrDefault()).ToList()),
                        sessionsContainingSelectedMG, muscleGroup, resultExpectancy, dontIncludeTheseExercises);
                }
            }
            if (noOfTimesPW == 0)
            {// we have no sessions priortized and we are not training this muscle group

                //the first session we will replace
                var firstSessionToReplace = GetLeastImportantSession(plan, plan.Sessions);
                var secondSessionToReplace = GetLeastImportantSession(plan, plan.Sessions.Where(i => i != firstSessionToReplace).ToList());


                //we want to re-create the sole sessionWithHasTheSelectedMG 
                //priortize session 1
                new SessionCreator().Prioritize(plan, firstSessionToReplace, sessionsContainingSelectedMG, muscleGroup, resultExpectancy,
                        dontIncludeTheseExercises);

                //then we want to find another session with the least impact on the user to replace
                new SessionCreator().Prioritize(plan, secondSessionToReplace,
                    sessionsContainingSelectedMG, muscleGroup, resultExpectancy, dontIncludeTheseExercises);

            }
        }


        public void IWantBigger45MinsOrGreater(Plan plan, MuscleGroup muscleGroup, int noOfTimesPW, List<Session> sessionsContainingSelectedMG, ResultExpectancy resultExpectancy, List<Exercise> dontIncludeTheseExercises)
        {
            //if theyre already training them twice or more a week
            if (noOfTimesPW >= 2)

            {// if weve only got one or less session priotrized
                if (sessionsContainingSelectedMG.Where(session => session.PrioritizedMuscleGroup == muscleGroup).Count() <= 1)
                {
                    //we want to re-create 2 sessions and ensure both are prioritized for the selected MG
                    //priortize session 1
                    new SessionCreator().Prioritize(plan, sessionsContainingSelectedMG.FirstOrDefault(), sessionsContainingSelectedMG, muscleGroup, resultExpectancy,
                        dontIncludeTheseExercises);
                    //remove the above from the list
                    sessionsContainingSelectedMG.RemoveAt(0);
                    //priortize session 2
                    new SessionCreator().Prioritize(plan, sessionsContainingSelectedMG.FirstOrDefault(), sessionsContainingSelectedMG, muscleGroup, resultExpectancy,
                        dontIncludeTheseExercises);
                }
            }
            if (noOfTimesPW == 1)
            {// if weve only got one or less session priotrized
                if (sessionsContainingSelectedMG.Where(session => session.PrioritizedMuscleGroup == muscleGroup).Count() <= 1)
                {
                    //we want to re-create the sole sessionWithHasTheSelectedMG 
                    //priortize session 1
                    new SessionCreator().Prioritize(plan, sessionsContainingSelectedMG.FirstOrDefault(), sessionsContainingSelectedMG, muscleGroup, resultExpectancy,
                        dontIncludeTheseExercises);

                    //then we want to find another session with the least impact on the user to replace
                    new SessionCreator().Prioritize(plan, GetLeastImportantSession(plan, plan.Sessions.Where(i => i != sessionsContainingSelectedMG.FirstOrDefault()).ToList()),
                        sessionsContainingSelectedMG, muscleGroup, resultExpectancy, dontIncludeTheseExercises);
                }
            }
            if (noOfTimesPW == 0)
            {// we have no sessions priortized and we are not training this muscle group

                //the first session we will replace
                var firstSessionToReplace = GetLeastImportantSession(plan, plan.Sessions);
                var secondSessionToReplace = GetLeastImportantSession(plan, plan.Sessions.Where(i => i != firstSessionToReplace).ToList());


                //we want to re-create the sole sessionWithHasTheSelectedMG 
                //priortize session 1
                new SessionCreator().Prioritize(plan, firstSessionToReplace, sessionsContainingSelectedMG, muscleGroup, resultExpectancy,
                        dontIncludeTheseExercises);

                //then we want to find another session with the least impact on the user to replace
                new SessionCreator().Prioritize(plan, secondSessionToReplace,
                    sessionsContainingSelectedMG, muscleGroup, resultExpectancy, dontIncludeTheseExercises);

            }

        }

        private Session GetLeastImportantSession(Plan plan, List<Session> sessionsToRate)
        {
            return plan.Sessions.First();

            if (sessionsToRate.Count < 2)
                throw new InvalidOperationException();

            var session1Mgs = new List<MuscleGroup>();

            foreach (var section in sessionsToRate.FirstOrDefault().Sections)
            {
                session1Mgs.AddRange(section.MuscleGroups);
            }

            var session2Mgs = new List<MuscleGroup>();

            foreach (var section in sessionsToRate.FirstOrDefault().Sections)
            {
                session2Mgs.AddRange(section.MuscleGroups);
            }

            if (session1Mgs.Where(mg => mg.Size == Size.Large).Count() == session2Mgs.Where(mg => mg.Size == Size.Large).Count())
            {

                var session1NoOfCompound = 0;
                foreach (var section in sessionsToRate[0].Sections)
                {
                    foreach (var ex in section.Exercises)
                        if (ex.CompOrIso == CompOrIso.Compound)
                            session1NoOfCompound++;
                }
                var session2NoOfCompound = 0;
                foreach (var section in sessionsToRate[1].Sections)
                {
                    foreach (var ex in section.Exercises)
                        if (ex.CompOrIso == CompOrIso.Compound)
                            session2NoOfCompound++;
                }

                if (session1NoOfCompound >= session2NoOfCompound)
                    return sessionsToRate[1];
                return sessionsToRate[0];

            }

            if (session1Mgs.Where(mg => mg.Size == Size.Large).Count() > session2Mgs.Where(mg => mg.Size == Size.Large).Count())
            {
                return sessionsToRate[1];
            }
            else
            {
                return sessionsToRate[0];
            }
        }


    }
}

