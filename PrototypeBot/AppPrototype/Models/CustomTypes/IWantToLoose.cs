﻿using AppPrototype.CustomTypes;
using AppPrototype.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppPrototype.Models.CustomTypes
{
    
    public class IWantToLoose
    {
        private Route Route = new Route("fgf");

        public void WantToLoose(Plan plan, MuscleGroup muscleGroup, ResultExpectancy resultExpectancy, List<Exercise> dontIncludeTheseExercises, Caller  caller = Caller.None)
        {
            if (plan.Sessions.Where(i => i.PrioritizedMuscleGroup != null && i.PrioritizedMuscleGroup.Name == muscleGroup.Name).Count() >= 2)
                throw new Exception();

            int noOfTimesPW = 0;
            var sessionsContainingSelectedMG = new List<Session>();


            if (plan.Activity == Activity.Cardio)
            {
                if (resultExpectancy == ResultExpectancy.ASAP)
                {
                    //this should work for any activity type
                    //if (plan.Activity == Activity.BodyweightTraining || plan.Activity == Activity.Cardio)
                    //{
                    //if the user has already prioritzed this muscle group 


                    sessionsContainingSelectedMG = new List<Session>();
                    noOfTimesPW = 0;

                    //this code works for any no of days per week , it should do
                    //if (plan.DaysPerWeek <= 3)
                    //{
                    //prioritize all sessions
                    foreach (var session in plan.Sessions)
                    {   //Check if we already prioritized this muscle group
                        if (plan.Sessions.Where(i => i.PrioritizedMuscleGroup != null && i.PrioritizedMuscleGroup.Name == muscleGroup.Name).Count() < 2)
                        {
                            foreach (var section in session.Sections)
                            {
                                session.PrioritizedMuscleGroup = Route.MuscleGroups.SingleOrDefault(smg => smg.Name == muscleGroup.Name);
                            }
                        }
                    }
                }
                if (resultExpectancy == ResultExpectancy.Soon)
                {
                    //this should work for any activity type
                    //if (plan.Activity == Activity.BodyweightTraining || plan.Activity == Activity.Cardio)
                    //{
                    //if the user has already prioritzed this muscle group 


                    sessionsContainingSelectedMG = new List<Session>();
                    noOfTimesPW = 0;

                    //this code works for any no of days per week , it should do
                    //if (plan.DaysPerWeek <= 3)
                    //{
                    //prioritize all sessions
                    foreach (var session in plan.Sessions)
                    {   //Check if we already prioritized this muscle group
                        if (plan.Sessions.Where(i => i.PrioritizedMuscleGroup != null && i.PrioritizedMuscleGroup.Name == muscleGroup.Name).Count() < 2)
                        {
                            int counter = 0;
                            while (counter < 2)
                            {
                                plan.Sessions[counter].PrioritizedMuscleGroup = Route.MuscleGroups.SingleOrDefault(smg => smg.Name == muscleGroup.Name);
                                counter++;
                            }
                        }
                    }
                }
                //switch (plan.SessionLength.TotalMinutes)
                //    {
                //        case 15:
                //            IWantToLose30MinsOrLess(plan, muscleGroup, noOfTimesPW, sessionsContainingSelectedMG, resultExpectancy, dontIncludeTheseExercises);
                //            break;
                //        case 30:
                //            IWantToLose30MinsOrLess(plan, muscleGroup, noOfTimesPW, sessionsContainingSelectedMG, resultExpectancy, dontIncludeTheseExercises);
                //            break;
                //        case 45:
                //            IWantToLose(plan, muscleGroup, noOfTimesPW, sessionsContainingSelectedMG, resultExpectancy, dontIncludeTheseExercises);
                //            break;
                //    }
                IWantToLose(plan, muscleGroup, noOfTimesPW, sessionsContainingSelectedMG, resultExpectancy, dontIncludeTheseExercises);
            }
        }

        public void IWantToLose(Plan plan, MuscleGroup muscleGroup, int noOfTimesPW, List<Session> sessionsContainingSelectedMG, ResultExpectancy resultExpectancy, List<Exercise> dontIncludeTheseExercises, Caller caller = Caller.None)
        {
            if (resultExpectancy == ResultExpectancy.ASAP)
            {
                if (plan.WeightGoal == WeightGoal.LooseFat || plan.WeightGoal == WeightGoal.ToneUp)
                {
                    foreach (var ses in plan.Sessions)
                    {
                        new SessionCreator().Prioritize(plan, ses, sessionsContainingSelectedMG, muscleGroup, resultExpectancy, dontIncludeTheseExercises, caller);
                    }
                }
            }
            if (resultExpectancy == ResultExpectancy.Soon)
            {
                if (plan.WeightGoal == WeightGoal.LooseFat || plan.WeightGoal == WeightGoal.ToneUp)
                {
                    if (plan.Sessions.Count >= 3)
                    {
                        new SessionCreator().Prioritize(plan, plan.Sessions[0], sessionsContainingSelectedMG, muscleGroup, resultExpectancy, dontIncludeTheseExercises, caller);
                        new SessionCreator().Prioritize(plan, plan.Sessions[2], sessionsContainingSelectedMG, muscleGroup, resultExpectancy, dontIncludeTheseExercises, caller);
                    }
                }
            }
        }

        //public void IWantToLose45MinsOrMore(Plan plan, MuscleGroup muscleGroup, int noOfTimesPW, List<Session> sessionsContainingSelectedMG, ResultExpectancy resultExpectancy, List<Exercise> dontIncludeTheseExercises)
        //{

        //}


    }
}
