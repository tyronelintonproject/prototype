﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppPrototype.Models.CustomTypes
{   [Serializable]
    public class IncreaseContainer
    {
        //Strength Props
        public int UpperBodyStrengthIncrease { get; set; }
        public int LowerBodyStrengthIncrease { get; set; }
        public int BicepStrengthIncrease { get; set; }
        public int TricepStrengthIncrease { get; set; }
        public int ShoulderStrengthIncrease { get; set; }
        public int ChestStrengthIncrease { get; set; }
        public int BackStrengthIncrease { get; set; }
        public int QaudsStrengthIncrease { get; set; }
        public int HamstringStrengthIncrease { get; set; }
        public int CalveStrengthIncrease { get; set; }
        public int GlutesStrengthIncrease { get; set; }
        public int CoreStrengthIncrease { get; set; }

        //Endurance Props
        public int UpperBodyEnduranceIncrease { get; set; }
        public int LowerBodyEnduranceIncrease { get; set; }
        public int BicepEnduranceIncrease { get; set; }
        public int TricepEnduranceIncrease { get; set; }
        public int ShoulderEnduranceIncrease { get; set; }
        public int ChestEnduranceIncrease { get; set; }
        public int BackEnduranceIncrease { get; set; }
        public int QaudsEnduranceIncrease { get; set; }
        public int HamstringEnduranceIncrease { get; set; }
        public int CalveEnduranceIncrease { get; set; }
        public int GlutesEnduranceIncrease { get; set; }
        public int CoreEnduranceIncrease { get; set; }
    }
}
