﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppPrototype.Models.CustomTypes
{
    public class InjuryHandler
    {
        public void Check(Plan plan)
        {
            if (plan.InjuryList.Count == 0)
                return;
            foreach (var session in plan.Sessions)
            {
                foreach (var section in session.Sections)
                {
                    foreach (var injuredMuscleGroup in plan.InjuryList)
                    {
                        if (section.MuscleGroups.Contains(injuredMuscleGroup))
                        {
                            Replace(section, injuredMuscleGroup, plan.InjuryPreferedReplacement);
                        }
                    }
                }
            }
        }

        private void Replace(Section section, MuscleGroup injuredMuscleGroup, MuscleGroup replacementMuscleGroup)
        {
            section.MuscleGroups.Remove(injuredMuscleGroup);
            section.MuscleGroups.Add(replacementMuscleGroup);
        }


    }
}
