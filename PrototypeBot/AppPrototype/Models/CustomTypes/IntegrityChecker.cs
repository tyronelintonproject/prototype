﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppPrototype.CustomTypes;
using AppPrototype.Models.Enums;

namespace AppPrototype.Models.CustomTypes
{
    public class IntegrityChecker
    {
        private Route route = new Route("Test");

        private bool Check(SetType setType, List<Exercise> exercises)
        {
            if (setType == SetType.Normal)
            {
                if (exercises.Count >= 1)
                    return true;
                return false;
            }
            else
            {
                if (exercises.Count >= 2)
                    return true;
                return false;
            }
        }

        public void CheckAndAct(SetType setType, List<Exercise> exercises, List<Exercise> filteredExercises, Plan plan, Session session, Section section, bool mustBeCompound, MuscleGroup muscleGroup = null, List<Exercise> ineligbleExercises = null)
        {

            if (Check(setType, filteredExercises))
            {
                return;
            }
            
            var equipmentList = new List<EquipmentType>() { EquipmentType.Bodyweight, EquipmentType.Dumbell, EquipmentType.KettelBell, EquipmentType.Barbell };
            var startingFilteredList = new List<Exercise>(filteredExercises);

            muscleGroup = muscleGroup ?? section.MuscleGroups.FirstOrDefault();

            var list1st = new List<Exercise>();
            var list2nd = new List<Exercise>();
            var list3rd = new List<Exercise>();
            var list4th = new List<Exercise>();
            var list5th = new List<Exercise>();

            if (ineligbleExercises != null && ineligbleExercises.Count > 0)
            {
                foreach (var i in ineligbleExercises)
                {
                    if (exercises.Any(ex => ex.Id == i.Id))
                    {
                        exercises.Remove(i);
                    }

                }
                
            }
            
            if (setType == SetType.Normal)
                {
                 
                            if (plan.GymOrHome == GymOrHome.Home)
                            {
                                if (plan.InjuryList.Count > 0)
                                {
                                    list1st = (mustBeCompound)
                                        ? exercises.Where(
                                            i => i.CompOrIso == CompOrIso.Compound &&
                                                 equipmentList.Any(tpe => tpe == i.EquipmentType) &&
                                                 plan.InjuryList.Any(mg => mg.Name != i.PrimaryMuscleGroup.Name) &&
                                                 !AllSessionExercises(session).Any(ex => ex.Name == i.Name)).ToList() 
                                        : exercises.Where(
                                            i => equipmentList.Any(tpe => tpe == i.EquipmentType) &&
                                                 plan.InjuryList.Any(mg => mg.Name != i.PrimaryMuscleGroup.Name) &&
                                                 !AllSessionExercises(session).Any(ex => ex.Name == i.Name)).ToList();
                    }
                                else
                                {
                                    list1st = (mustBeCompound)
                                        ? exercises.Where(
                                            i => i.CompOrIso == CompOrIso.Compound &&
                                                 equipmentList.Any(tpe => tpe == i.EquipmentType) &&
                                                 !AllSessionExercises(session).Any(ex => ex.Name == i.Name)).ToList()
                                        : exercises.Where(
                                            i => equipmentList.Any(tpe => tpe == i.EquipmentType) &&
                                                 !AllSessionExercises(session).Any(ex => ex.Name == i.Name)).ToList();
                    }
                            }
                            else
                            {
                                if (plan.InjuryList.Count > 0)
                                {
                                    list1st = (mustBeCompound)
                                        ? exercises.Where(
                                            i => i.CompOrIso == CompOrIso.Compound &&
                                                 plan.InjuryList.Any(mg => mg.Name != i.PrimaryMuscleGroup.Name) &&
                                                 !AllSessionExercises(session).Any(ex => ex.Name == i.Name)).ToList()
                                        : exercises.Where(
                                            i => equipmentList.Any(tpe => tpe == i.EquipmentType) &&
                                                 plan.InjuryList.Any(mg => mg.Name != i.PrimaryMuscleGroup.Name) &&
                                                 !AllSessionExercises(session).Any(ex => ex.Name == i.Name)).ToList();
                    }
                                else
                                {
                                    list1st = (mustBeCompound)
                                        ? exercises.Where(
                                            i => i.CompOrIso == CompOrIso.Compound &&
                                                 !AllSessionExercises(session).Any(ex => ex.Name == i.Name)).ToList()
                                        : exercises.Where(
                                            i => equipmentList.Any(tpe => tpe == i.EquipmentType) &&
                                                 !AllSessionExercises(session).Any(ex => ex.Name == i.Name)).ToList();
                    }
                                
                            }

                if (list1st.Count > 1)
                            {
                                list2nd = list1st.Where(i => i.Activities.Contains(session.ActivityType)).ToList();

                                if (list2nd.Count > 1)
                                {
                                    list3rd = list2nd.Where(i => i.PrimaryMuscleGroup.Name == muscleGroup.Name).ToList();

                                    if (list3rd.Count > 1)
                                    {
                                        list4th = list3rd.Where(i => GetAvailibleFitnessLevels(i).Contains(i.FitnessLevel)).ToList();

                                        if (list4th.Count > 1)
                                        {
                                            list5th = list4th.Where(i => i.WeightGoals.Contains(plan.WeightGoal)).ToList();

                                            if (list5th.Count > 1)
                                            {
                                                filteredExercises.AddRange(list5th);
                                            }
                                            else
                                            {
                                                if (list5th.Count == 1)
                                                {
                                                    filteredExercises.AddRange(list5th);
                                                }
                                                else
                                                {
                                                    filteredExercises.AddRange(list4th);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (list4th.Count == 1)
                                            {
                                                filteredExercises.AddRange(list4th);
                                            }
                                            else
                                            {
                                                filteredExercises.AddRange(list3rd);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (list3rd.Count == 1)
                                        {
                                            filteredExercises.AddRange(list3rd);
                                        }
                                        else
                                        {
                                            filteredExercises.AddRange(list2nd);
                                        }
                                    }

                                }
                                else
                                {
                                    if (list2nd.Count == 1)
                                    {
                                        filteredExercises.AddRange(list2nd);
                                    }
                                    else
                                    {
                                        filteredExercises.AddRange(list1st);
                                    }
                                }
                            }
                            else
                            {
                                if (list1st.Count == 1)
                                {
                                    filteredExercises.AddRange(list1st);
                                }
                                else
                                {
                                    if (plan.InjuryList.Count > 0)
                                    {
                                        filteredExercises.Add(exercises.FirstOrDefault(i => equipmentList.Any(tpe => tpe == i.EquipmentType) && plan.InjuryList.Any(mg => mg.Name != i.PrimaryMuscleGroup.Name)));
                        }
                                    else
                                    {
                                        filteredExercises.Add(exercises.FirstOrDefault(i => equipmentList.Any(tpe => tpe == i.EquipmentType)));
                        }

                                }
                            }
                  
                }
                else
                {
                if (plan.GymOrHome == GymOrHome.Home)
                    {
                        if (plan.InjuryList.Count > 0)
                        {
                            list1st = (mustBeCompound)
                                ? exercises.Where(i => i.CompOrIso == CompOrIso.Compound &&
                                                       equipmentList.Any(tpe => tpe == i.EquipmentType) &&
                                                       plan.InjuryList.Any(
                                                           mg => mg.Name != i.PrimaryMuscleGroup.Name) &&
                                                       !AllSessionExercises(session).Any(ex => ex.Name == i.Name)).ToList()
                                : exercises.Where(i => equipmentList.Any(tpe => tpe == i.EquipmentType) &&
                                                       plan.InjuryList.Any(
                                                           mg => mg.Name != i.PrimaryMuscleGroup.Name) &&
                                                       !AllSessionExercises(session).Any(ex => ex.Name == i.Name)).ToList();
                    }
                        else
                        {
                            list1st = (mustBeCompound)
                                ? exercises.Where(i => i.CompOrIso == CompOrIso.Compound &&
                                                       !AllSessionExercises(session).Any(ex => ex.Name == i.Name)).ToList()
                                : exercises.Where(i => equipmentList.Any(tpe => tpe == i.EquipmentType) &&
                                                       !AllSessionExercises(session).Any(ex => ex.Name == i.Name)).ToList();
                    }
                        
                    }
                    else
                    {
                        if (plan.InjuryList.Count > 0)
                        {
                            list1st = (mustBeCompound)
                                ? exercises.Where(i => i.CompOrIso == CompOrIso.Compound &&
                                                       plan.InjuryList.Any(
                                                           mg => mg.Name != i.PrimaryMuscleGroup.Name) &&
                                                       !AllSessionExercises(session).Any(ex => ex.Name == i.Name)).ToList()
                                : exercises.Where(i => equipmentList.Any(tpe => tpe == i.EquipmentType) &&
                                                       plan.InjuryList.Any(
                                                           mg => mg.Name != i.PrimaryMuscleGroup.Name) &&
                                                       !AllSessionExercises(session).Any(ex => ex.Name == i.Name)).ToList();
                    }
                        else
                        {
                            list1st = (mustBeCompound)
                                ? exercises.Where(i => i.CompOrIso == CompOrIso.Compound &&
                                                       !AllSessionExercises(session).Any(ex => ex.Name == i.Name)).ToList()
                                : exercises.Where(i => equipmentList.Any(tpe => tpe == i.EquipmentType) &&
                                                       !AllSessionExercises(session).Any(ex=>ex.Name == i.Name)).ToList();
                        }
                        
                    }



                if (list1st.Count > 2)
                    {
                        list2nd = list1st.Where(i => i.Activities.Contains(session.ActivityType)).ToList();

                        if (list2nd.Count > 2)
                        {
                            list3rd = list2nd.Where(i => i.PrimaryMuscleGroup.Name == muscleGroup.Name).ToList();

                            if (list3rd.Count > 2)
                            {
                                list4th = list3rd.Where(i => GetAvailibleFitnessLevels(i).Contains(i.FitnessLevel)).ToList();

                                if (list4th.Count > 2)
                                {
                                    list5th = list4th.Where(i => i.WeightGoals.Contains(plan.WeightGoal)).ToList();

                                    if (list5th.Count > 2)
                                    {
                                        filteredExercises.AddRange(list5th);
                                    }
                                    else
                                    {
                                        if (list5th.Count == 2)
                                        {
                                            filteredExercises.AddRange(list5th);
                                            if (startingFilteredList.Count > 0 && !list5th.Any(i=>i.Name != startingFilteredList.FirstOrDefault().Name))
                                            {
                                                list5th.Add(startingFilteredList.FirstOrDefault());
                                            }
                                    }
                                        else
                                        {
                                            filteredExercises.AddRange(list4th);
                                        }
                                    }
                                }
                                else
                                {
                                    if (list4th.Count == 2)
                                    {
                                        filteredExercises.AddRange(list4th);
                                        if (startingFilteredList.Count > 0 && !list4th.Any(i => i.Name != startingFilteredList.FirstOrDefault().Name))
                                        {
                                            list4th.Add(startingFilteredList.FirstOrDefault());
                                        }
                                }
                                    else
                                    {
                                        filteredExercises.AddRange(list3rd);
                                    }
                                }
                            }
                            else
                            {
                                if (list3rd.Count == 2)
                                {
                                    filteredExercises.AddRange(list3rd);
                                    if (startingFilteredList.Count > 0 && !list3rd.Any(i => i.Name != startingFilteredList.FirstOrDefault().Name))
                                    {
                                        list3rd.Add(startingFilteredList.FirstOrDefault());
                                    }
                            }
                                else
                                {
                                    filteredExercises.AddRange(list2nd);
                                }
                            }

                        }
                        else
                        {
                            if (list2nd.Count == 2)
                            {
                                filteredExercises.AddRange(list2nd);
                                if (startingFilteredList.Count > 0 && !list2nd.Any(i => i.Name != startingFilteredList.FirstOrDefault().Name))
                                {
                                    list2nd.Add(startingFilteredList.FirstOrDefault());
                                }
                        }
                            else
                            {
                                filteredExercises.AddRange(list1st);
                            }
                        }
                    }
                    else
                    {
                        if (list1st.Count == 2)
                        {
                            filteredExercises.AddRange(list1st);
                            if (startingFilteredList.Count > 0 && !list1st.Any(i => i.Name != startingFilteredList.FirstOrDefault().Name))
                            {
                                list1st.Add(startingFilteredList.FirstOrDefault());
                            }
                    }
                        else
                        {
                            filteredExercises.Add(exercises.FirstOrDefault(i => equipmentList.Any(tpe => tpe == i.EquipmentType) && plan.InjuryList.Any(mg => mg.Name != i.PrimaryMuscleGroup.Name)));
                        }
                    }
        }
    }




        private List<FitnessLevel> GetAvailibleFitnessLevels(Exercise exercise)
        {
            if (exercise.FitnessLevel == FitnessLevel.Advanced)
                return new List<FitnessLevel>() { FitnessLevel.Advanced, FitnessLevel.Intermediate, FitnessLevel.Beginner };
            if (exercise.FitnessLevel == FitnessLevel.Intermediate)
                return new List<FitnessLevel>() { FitnessLevel.Intermediate, FitnessLevel.Beginner };
            if (exercise.FitnessLevel == FitnessLevel.Beginner)
                return new List<FitnessLevel>() { FitnessLevel.Beginner };

            return null;
        }

        public List<Exercise> AllSessionExercises(Session session)
        {
            var exercises = new List<Exercise>();

            foreach (var section in session.Sections)
            {
                exercises.AddRange(section.Exercises);
            }

            return exercises;
        }

    }
}