﻿using AppPrototype.Models.CustomTypes.Food;
using AppPrototype.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppPrototype.Models.CustomTypes.MealLogic
{

    public class FoodFinder
    {

        public List<FoodItem> FoodBank = new List<FoodItem>()
        {   //protein items
            new FoodItem(1,"Egg", 6,3,2, FoodType.Protein,new List<MealType>(){MealType.Breakfast,MealType.Lunch},"Egg"),
            new FoodItem(1,"Omlette", 18,9,6, FoodType.Protein,new List<MealType>(){MealType.Breakfast,MealType.Lunch},"Egg"),
            new FoodItem(1,"Scrambled Eggs x 3", 18,9,6, FoodType.Protein,new List<MealType>(){MealType.Breakfast},"Egg"),
            new FoodItem(1," 4 Grilled Bacon Rashers", 16,11,15, FoodType.Protein,new List<MealType>(){MealType.Breakfast},"Pork"),
            new FoodItem(1,"Porride w/ Protein scoop", 18,20,8, FoodType.Protein,new List<MealType>(){MealType.Breakfast},"Oats"),
            //protein items
            new FoodItem(1,"Large Chicken Breast", 23,10,7, FoodType.Protein,new List<MealType>(){MealType.Lunch, MealType.Dinner} ,"Chicken"),
            new FoodItem(1,"Small handful of diced chicken", 16,5,3, FoodType.Protein,new List<MealType>(){MealType.Lunch, MealType.Dinner} ,"Chicken"),
            new FoodItem(1,"2 Large Chicken Breasts", 38,20,14, FoodType.Protein,new List<MealType>(){MealType.Lunch, MealType.Dinner} ,"Chicken"),
            new FoodItem(1,"Spicey Diced Chicken", 25,10,7, FoodType.Protein,new List<MealType>(){MealType.Dinner, MealType.Lunch},"Chicken"),
            new FoodItem(1,"8 Oz Steak", 29,7,12, FoodType.Protein,new List<MealType>(){MealType.Dinner, MealType.Lunch},"Beef"),
            new FoodItem(1,"4 medium Slices of Beef", 23,10,10, FoodType.Protein,new List<MealType>(){MealType.Dinner, MealType.Lunch},"Beef"),
            new FoodItem(1,"Large Cod", 29,10,15, FoodType.Protein,new List<MealType>(){MealType.Dinner, MealType.Lunch},"Fish"),
            new FoodItem(1,"Meduim Red Salmon", 30,12,15, FoodType.Protein,new List<MealType>(){MealType.Dinner, MealType.Lunch},"Fish"),
            //snacks
            new FoodItem(1,"Protein shake w/ milk", 25,10,5, FoodType.Mix,new List<MealType>(){MealType.Snack, MealType.Lunch},"ProteinShake"),
            new FoodItem(1,"Mini chicken salad", 20,3,3, FoodType.Mix,new List<MealType>(){MealType.Snack, MealType.Lunch},"Salad"),
            //carb items
            new FoodItem(1,"half cup/brown rice", 9,60,6, FoodType.Carb,new List<MealType>(){MealType.Lunch, MealType.Dinner} ,"Rice"),
            new FoodItem(1,"full cup/brown rice", 18,100,12, FoodType.Carb,new List<MealType>(){MealType.Lunch, MealType.Dinner} ,"Rice"),
            new FoodItem(1,"Cup of Pasta", 11,60,6, FoodType.Carb,new List<MealType>(){MealType.Dinner, MealType.Lunch, MealType.Snack},"Pasta"),
            new FoodItem(1,"Sweet Potatoe", 10,50,4, FoodType.Carb,new List<MealType>(){MealType.Dinner, MealType.Lunch},"Potatoe"),
            new FoodItem(1,"Baked Potatoe", 10,50,4, FoodType.Carb,new List<MealType>(){MealType.Dinner, MealType.Lunch},"Potatoe"),
            new FoodItem(1,"Spagetti", 10,60,6, FoodType.Carb,new List<MealType>(){MealType.Dinner, MealType.Lunch},"Pasta"),
            new FoodItem(1,"Oats", 8,25,6, FoodType.Carb,new List<MealType>(){MealType.Breakfast, MealType.Snack},"Oats"),
            new FoodItem(1,"Bagel", 4,30,5, FoodType.Carb,new List<MealType>(){MealType.Breakfast, MealType.Snack},"Bread"),
            new FoodItem(1,"2 slices wholegrain bread", 11,39,2, FoodType.Carb,new List<MealType>(){MealType.Breakfast, MealType.Snack},"Bread"),
            new FoodItem(1,"French bread", 6,45,8, FoodType.Carb,new List<MealType>(){MealType.Breakfast, MealType.Snack},"Bread"),
            new FoodItem(1,"Half cup Mixed peppers", 5,35,3, FoodType.Carb,new List<MealType>(){MealType.Breakfast, MealType.Snack},"Spice"),
            //veg items
            new FoodItem(1,"Cup/green veg", 6,3,1, FoodType.Veg,new List<MealType>(){MealType.Lunch, MealType.Dinner},"Vegetable"),
            //sandwich items
            new FoodItem(1,"Tuna Sandwich, B/Bread", 20,40,12, FoodType.Mix,new List<MealType>(){MealType.Lunch, MealType.Snack},"Sandwich"),
            new FoodItem(1,"Turkey Sandwich, B/Bread", 23,40,12, FoodType.Mix,new List<MealType>(){MealType.Lunch, MealType.Snack},"Sandwich"),
            new FoodItem(1,"Chicken Sandwich, B/Bread", 25,40,12, FoodType.Mix,new List<MealType>(){MealType.Lunch, MealType.Snack},"Sandwich"),
            //pastapotItems
            new FoodItem(1,"Pasta pot w/Chicken", 17,43,5, FoodType.Mix,new List<MealType>(){MealType.Lunch, MealType.Snack},"Pasta"),
            new FoodItem(1,"Pasta pot w/Prawns", 20,40,8, FoodType.Mix,new List<MealType>(){MealType.Lunch, MealType.Snack},"Pasta"),
            new FoodItem(1,"Pasta pot w/Tuna", 25,42,8, FoodType.Mix,new List<MealType>(){MealType.Lunch, MealType.Snack},"Pasta"),
            //baked potatoe
            new FoodItem(1,"Baked potatoe w/Tuna", 21,36,8, FoodType.Mix,new List<MealType>(){MealType.Lunch, MealType.Snack},"BakedPotatoe"),
            //fruits
            new FoodItem(1,"Banana", 2,30,1, FoodType.Fruit,new List<MealType>(){MealType.Lunch, MealType.Snack, MealType.Breakfast},"Fruit"),
            //nutts
            new FoodItem(1,"Hanful of Cashews", 23,5,20, FoodType.Fat,new List<MealType>(){MealType.Lunch, MealType.Snack, MealType.Breakfast},"Fat"),

            //Retailor Items
            new FoodItem(1,"Chicken deli wrap", 22, 35,25, FoodType.Mix,new List<MealType>(){MealType.Lunch, MealType.Snack, MealType.Lunch},"Wrap","Mc Donalds"),
            new FoodItem(1,"Steak Bake", 19,5,40, FoodType.Mix,new List<MealType>(){MealType.Lunch, MealType.Snack, MealType.Lunch},"Pastry","Greggs"),


        };


        private double FindAverageProteinGrams(List<FoodItem> foodTypeItemsToCompare)
        {
            if (foodTypeItemsToCompare == null || foodTypeItemsToCompare.Count == 0)
                return 0;
            return foodTypeItemsToCompare.Average(i => i.GramsOfProtein);
        }
        private double FindAverageCarbGrams(List<FoodItem> foodTypeItemsToCompare)
        {
            if (foodTypeItemsToCompare == null || foodTypeItemsToCompare.Count == 0)
                return 0;
            return foodTypeItemsToCompare.Average(i => i.GramsOfCarbs);
        }
        private double FindAverageFatGrams(List<FoodItem> foodTypeItemsToCompare)
        {
            if (foodTypeItemsToCompare == null || foodTypeItemsToCompare.Count == 0)
                return 0;
            return foodTypeItemsToCompare.Average(i => i.GramsOfFat);
        }
        private double FindAverageMixGrams(List<FoodItem> foodTypeItemsToCompare)
        {
            if (foodTypeItemsToCompare == null || foodTypeItemsToCompare.Count == 0)
                return 0;
            return foodTypeItemsToCompare.Average(i => i.TotalMixGrams);
        }



        private bool IsWithinRange(double grams1, double grams2, FoodType foodType, double gauge = 0)
        {
            if (gauge == 0)
            {
                switch (foodType)
                {
                    case FoodType.Protein:
                        {
                            gauge = 4;
                        }
                        break;
                    case FoodType.Carb:
                        {
                            gauge = 6;
                        }
                        break;
                    default:
                        {
                            gauge = 4;
                        }
                        break;
                }
            }
            else
            {
                switch (foodType)
                {
                       
                    case FoodType.Carb:
                        {
                            gauge = gauge * 1.25;
                        }
                        break;
                }
            }

            var result = Math.Abs(grams1 - grams2);
            return (result <= gauge);
        }


        private FoodItem IsWithinRangeMixItem(List<FoodItem> foodItems, double grams2, FoodType foodType, double gauge = 0)
        {
            if (gauge == 0)
            {
                switch (foodType)
                {
                    case FoodType.Protein:
                        {
                            gauge = 4;
                        }
                        break;
                    case FoodType.Carb:
                        {
                            gauge = 6;
                        }
                        break;
                    case FoodType.Mix:
                        {
                            gauge = 10;
                        }
                        break;
                    default:
                        {
                            gauge = 4;
                        }
                        break;
                }
            }
            else
            {
                switch (foodType)
                {

                    case FoodType.Carb:
                        {
                            gauge = gauge * 1.25;
                        }
                        break;
                }
            }

            foreach (var i in foodItems)
            {
                var result = Math.Abs(i.TotalMixGrams - grams2);
                if (result <= gauge)
                    return i;
            }

            return null;
        }




        public List<FoodItem> FindFood(Plan plan, MealType mealType, FoodType foodType, double grams)
        {

            grams = RunMacroBalancer(mealType, foodType, grams);

            //a list to store the food results          
            var confirmedFoodList = new List<FoodItem>();
            //a list to store the suitable items
            var suitableFoodItems = new List<FoodItem>();

            //we need to do a switch because we dont know what range type to check either pro,carb,fat

            switch (foodType)
            {
                case FoodType.Protein:
                    {
                        //filter the list once for suitable items
                        suitableFoodItems = FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == foodType && IsWithinRange(i.GramsOfProtein, grams, foodType) == true).ToList();
                        suitableFoodItems = (suitableFoodItems.Count() > 0) ? suitableFoodItems : FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == foodType && IsWithinRange(i.GramsOfProtein, grams, foodType, 10)
                        == true && !confirmedFoodList.Contains(i)).ToList();
                    }
                    break;
                case FoodType.Carb:
                    {
                        //filter the list once for suitable items
                        suitableFoodItems = FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == foodType && IsWithinRange(i.GramsOfCarbs, grams, foodType) == true).ToList();
                        suitableFoodItems = (suitableFoodItems.Count() > 0) ? suitableFoodItems : FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == foodType && IsWithinRange(i.GramsOfCarbs, grams, foodType, 20)
                        == true && !confirmedFoodList.Contains(i)).ToList();
                    }
                    break;
                case FoodType.Fat:
                    {
                        //filter the list once for suitable items
                        suitableFoodItems = FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == foodType && IsWithinRange(i.GramsOfFat, grams, foodType) == true).ToList();
                        suitableFoodItems = (suitableFoodItems.Count() > 0) ? suitableFoodItems : FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == foodType && IsWithinRange(i.GramsOfFat, grams, foodType, 15)
                        == true && !confirmedFoodList.Contains(i)).ToList();
                    }
                    break;
                case FoodType.Veg:
                    {
                        //filter the list once for suitable items   -- as this is veg we are running setting it to true so it auto goes in as we dont track veg.
                        suitableFoodItems = FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == foodType && IsWithinRange(grams, grams, foodType) == true).ToList();
                        suitableFoodItems = (suitableFoodItems.Count() > 0) ? suitableFoodItems : FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == foodType && IsWithinRange(i.TotalMixGrams, grams, foodType, 20)
                        == true && !confirmedFoodList.Contains(i)).ToList();
                    }
                    break;
                case FoodType.Fruit:
                    {
                        //filter the list once for suitable items   -- as this is veg we are running setting it to true so it auto goes in as we dont track veg.
                        suitableFoodItems = FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == foodType && IsWithinRange(grams, grams, foodType) == true).ToList();
                        suitableFoodItems = (suitableFoodItems.Count() > 0) ? suitableFoodItems : FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == foodType && IsWithinRange(i.TotalMixGrams, grams, foodType, 20)
                        == true && !confirmedFoodList.Contains(i)).ToList();
                    }
                    break;
                }

            //we only really want one veg atm so we just add it if its a veg then return 
            if (foodType == FoodType.Veg)
            {
                if (suitableFoodItems != null && suitableFoodItems.Count > 0)
                {
                    AddItem(plan.User.NutritionPlan, confirmedFoodList, suitableFoodItems, 1, foodType, mealType, grams);
                    return confirmedFoodList;
                }
                    return confirmedFoodList;
            }

            AddItem(plan.User.NutritionPlan, confirmedFoodList, suitableFoodItems, 4, foodType, mealType, grams);

            return confirmedFoodList;

        }


        public void AddItem(NutritionPlan plan, List<FoodItem> confirmedFoodList, List<FoodItem> suitableFoodItems, int iteratorValue, FoodType foodType, MealType mealType, double grams)
        {
            var mealObject = new MealObject();

            switch (mealType)
            {

                case MealType.Breakfast:
                    {
                        mealObject = plan.Breakfasts;
                        //store item in a variable
                        suitableFoodItems = suitableFoodItems.Where(i => i.Type == foodType && !(mealObject as Breakfast).Items.Any(mi => mi.Name == i.Name)).ToList();
                    }
                    break;
                case MealType.Lunch:
                    {
                        mealObject = plan.Lunches;
                        //store item in a variable
                        suitableFoodItems = suitableFoodItems.Where(i => i.Type == foodType && !(mealObject as Lunch).Items.Any(mi => mi.Name == i.Name)).ToList();

                    }
                    break;
                case MealType.Dinner:
                    {
                        mealObject = plan.Dinners;
                        //store item in a variable
                        suitableFoodItems = suitableFoodItems.Where(i => i.Type == foodType && !(mealObject as Dinner).Items.Any(mi => mi.Name == i.Name)).ToList();

                    }
                    break;
                case MealType.Snack:
                    {
                        mealObject = plan.Snacks;
                        //store item in a variable
                        suitableFoodItems = suitableFoodItems.Where(i => i.Type == foodType && !(mealObject as Snack).Items.Any(mi => mi.Name == i.Name)).ToList();

                    }
                    break;

            }

            suitableFoodItems = (suitableFoodItems.Count() > 0) ? suitableFoodItems : FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == foodType && !(mealObject as Snack).Items.Any(mi => mi.Name == i.Name) 
               && IsWithinRange(i.TotalMixGrams, grams, foodType, 30)
                == true && !confirmedFoodList.Contains(i)).ToList();

            //reset the iterator if we dont have enough items
            iteratorValue = (suitableFoodItems.Count() < iteratorValue) ? suitableFoodItems.Count : iteratorValue;

            int safteyCheck = 0;

            while (confirmedFoodList.Count < iteratorValue)
            {
                if (safteyCheck > 10)
                    return;

               var item = ReturnHighestProtein(suitableFoodItems);

                if (item != null)
                {//check that this fooditems Parent isnt going to be in the list more than twice e.g. scrambled egg, omlette, boiled eggs, nutts < we only want 2 of the same parent type 
                    if (confirmedFoodList.Where(i => i.Parent == item.Parent).Count() < 2)
                    {   //add the item if we dont have the same parent twice already
                        confirmedFoodList.Add(item);
                        //remove the item so we can add the next item in the list
                        suitableFoodItems.Remove(item);
                    }
                    else
                    {   //if we have the same parent twice then go into the suitableItemsList and get item with different Parent
                        var updatedItem = suitableFoodItems.SingleOrDefault(i => i.Parent != item.Parent);
                        {
                            if (updatedItem != null)
                            {   //if we find another item add it
                                confirmedFoodList.Add(updatedItem);
                                //remove the item so we can add the next item in the list
                                suitableFoodItems.Remove(updatedItem);
                            }
                            else
                            {   //if we dont we will have too add that item
                                confirmedFoodList.Add(item);
                                //remove the item so we can add the next item in the list
                                suitableFoodItems.Remove(item);
                            }
                        }
                    }
                }
                else
                {
                    switch (foodType)
                    {
                        case FoodType.Protein:
                            {
                                //filter the list once for suitable items
                                suitableFoodItems = FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == foodType && IsWithinRange(i.GramsOfProtein, grams, foodType, 10) == true && !confirmedFoodList.Contains(i)).ToList();
                            }
                            break;
                        case FoodType.Carb:
                            {
                                //filter the list once for suitable items
                                suitableFoodItems = FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == foodType && IsWithinRange(i.GramsOfCarbs, grams, foodType, 20) == true && !confirmedFoodList.Contains(i)).ToList();
                            }
                            break;
                        case FoodType.Fat:
                            {
                                //filter the list once for suitable items
                                suitableFoodItems = FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == foodType && IsWithinRange(i.GramsOfFat, grams, foodType, 10) == true && !confirmedFoodList.Contains(i)).ToList();
                            }
                            break;
                        case FoodType.Fruit:
                            {
                                //filter the list once for suitable items
                                suitableFoodItems = FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == foodType && IsWithinRange(i.GramsOfFat, grams, foodType, 20) == true && !confirmedFoodList.Contains(i)).ToList();
                            }
                            break;
                    }
                    
                    if (suitableFoodItems.Count == 0)
                    {
                        switch (foodType)
                        {
                            case FoodType.Protein:
                                {
                                    //filter the list once for suitable items
                                    suitableFoodItems = FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == foodType && IsWithinRange(i.GramsOfProtein, grams, foodType, 20) == true && !confirmedFoodList.Contains(i)).ToList();
                                    suitableFoodItems = (suitableFoodItems.Count() > 0) ? suitableFoodItems : FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == foodType && IsWithinRange(i.GramsOfProtein, grams, foodType, 40)
                                    == true && !confirmedFoodList.Contains(i)).ToList();
                                }
                                break;
                            case FoodType.Carb:
                                {
                                    //filter the list once for suitable items
                                    suitableFoodItems = FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == foodType && IsWithinRange(i.GramsOfCarbs, grams, foodType, 40) == true && !confirmedFoodList.Contains(i)).ToList();
                                    suitableFoodItems = (suitableFoodItems.Count() > 0) ? suitableFoodItems : FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == foodType && IsWithinRange(i.GramsOfCarbs, grams, foodType, 60) 
                                    == true && !confirmedFoodList.Contains(i)).ToList();
                                }
                                break;
                            case FoodType.Fat:
                                {
                                    //filter the list once for suitable items
                                    suitableFoodItems = FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == foodType && IsWithinRange(i.GramsOfFat, grams, foodType, 30) == true && !confirmedFoodList.Contains(i)).ToList();
                                    suitableFoodItems = (suitableFoodItems.Count() > 0) ? suitableFoodItems : FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == foodType && IsWithinRange(i.GramsOfFat, grams, foodType, 50)
                                    == true && !confirmedFoodList.Contains(i)).ToList();
                                }
                                break;
                            case FoodType.Fruit:
                                {
                                    //filter the list once for suitable items
                                    suitableFoodItems = FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == foodType && IsWithinRange(i.TotalMixGrams, grams, foodType, 30) == true && !confirmedFoodList.Contains(i)).ToList();
                                    suitableFoodItems = (suitableFoodItems.Count() > 0) ? suitableFoodItems : FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == foodType && IsWithinRange(i.TotalMixGrams, grams, foodType, 50)
                                    == true && !confirmedFoodList.Contains(i)).ToList();
                                }
                                break;


                        }
                    }

                }

                safteyCheck++;

            }
        }

        public List<FoodItem> FindLunchMixes(Plan plan, MealType mealType, FoodType foodType, double grams, List<FoodItem> currentSnacks)
        {
            //look at method for usage
            //grams = RunMacroBalancer(mealType, foodType, grams); 
            //one foodtype at a time. we need something

            var snacksList = FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && (i.Type == FoodType.Fat || i.Type == FoodType.Fruit));

            if (currentSnacks != null && currentSnacks.Count > 0)
            {
                grams = grams - snacksList.Average(i => i.TotalMixGrams);
            }


            //a list to store the food results          
            var confirmedFoodList = new List<FoodItem>();
            //a list to store the suitable items
            var suitableFoodItems = new List<FoodItem>();

                //make sure we add sandwich first
                if (confirmedFoodList.Count == 0)
                {   //filter to sandwiches
                    AddSpecficItem(suitableFoodItems, confirmedFoodList, grams, foodType, "Sandwich");
                    
                }
                //make sure we add pasta pot / baked potatoe second
                if (confirmedFoodList.Count == 1)
                {   //filter to sandwiches
                    AddSpecficItem(suitableFoodItems, confirmedFoodList, grams, foodType, "BakedPotatoe");
                }
                else throw new Exception();

                return confirmedFoodList; 
        }

        public List<FoodItem> FindLunchSnacks(Plan plan, MealType mealType, FoodType foodType, double grams, SnackAdder SnackAdder, List<string> retailors = null)
        {
            //look at method for usage
            //grams = RunMacroBalancer(mealType, foodType, grams); 
            //one foodtype at a time. we need something

             //a list to store the food results          
            var confirmedFoodList = new List<FoodItem>();
            //a list to store the suitable items
            var suitableFoodItems = new List<FoodItem>(FoodBank.Where(i=>i.AppropriateMeals.Contains(mealType) && (i.Type == FoodType.Fruit || i.Type == FoodType.Fat)));
            var items = new List<FoodItem>();
            //this checks if its a sandwich then filters the list accordingly
            if (SnackAdder == SnackAdder.Sandwich)
            {
                items.AddRange(FoodBank.Where(i => i.Parent == "Sandwich"));
            }
            //this checks if method is called for a retailor check so filters lists
            if (SnackAdder == SnackAdder.LocalShops)
            {
                if (retailors == null || retailors.Count == 0)
                    throw new Exception();

                foreach (var i in retailors)
                {
                    items.AddRange(FoodBank.Where(item => item.Retailor == i));
                }
            }
            //get the average grams for a sandwich or retailor item checks that we will have enough to add a snack and then a snack/retailor item
            var avCalForItem = items.Average(i => i.TotalMixGrams);
            var avCalForSnack = suitableFoodItems.Average(i => i.TotalMixGrams);
            if ((grams - avCalForItem) >= avCalForSnack)
            {
                //int x = 0;
                //while (x < 1)
                //{                     //if we have enough grams to 
                    AddItem(plan.User.NutritionPlan, confirmedFoodList, suitableFoodItems, 1, foodType, mealType, avCalForSnack);
                //    x++;
                //    foodType = (foodType == FoodType.Fruit) ? FoodType.Fat : FoodType.Fruit;
                //}
            }
            return confirmedFoodList;
        }


        public List<FoodItem> FindSnacks(Plan plan, MealType mealType, FoodType foodType, double grams, SnackAdder SnackAdder)
        { 
            //a list to store the food results          
            var confirmedFoodList = new List<FoodItem>();
            //a list to store the suitable items
            var suitableFoodItems = new List<FoodItem>(FoodBank.Where(i => i.AppropriateMeals.Contains(mealType)));
            var items = new List<FoodItem>();

            //check how many snacks we could add for the snacks
            var avSnack = suitableFoodItems.Average(fi => fi.TotalMixGrams);
            //if true add 2 snacks an AM and a PM snack
            if ((avSnack * 1.75) <= grams)
            {
                AddItem(plan.User.NutritionPlan, confirmedFoodList, suitableFoodItems, 2, foodType, mealType, grams / 2);
            }
            else
            {
                AddItem(plan.User.NutritionPlan, confirmedFoodList, suitableFoodItems, 1, foodType, mealType, grams / 1.25);
            }
            return confirmedFoodList;
        }



        public List<FoodItem> LunchFinder(Plan plan, MealType mealType, FoodType foodType, double grams)
        { 
            //a list to store the food results          
            var confirmedFoodList = new List<FoodItem>();
            //a list to store the suitable items
            var suitableFoodItems = new List<FoodItem>();

            //look at method for usage
            RunMacroBalancer(mealType, foodType, grams);

            //this method takes in a iterator and adds an item of the mealtype and foodtype that many times
            AddItem(plan.User.NutritionPlan, confirmedFoodList, suitableFoodItems, 2, foodType, mealType, grams);
            
            return confirmedFoodList;
        }

        public void AddSpecficItem(List<FoodItem> suitableFoodItems, List<FoodItem> confirmedFoodList, double grams, FoodType foodType, string parent)
        {
            //filter to parent

            suitableFoodItems = FoodBank.Where(i => i.Parent == parent).ToList();
            if (suitableFoodItems.Count == 0)
                throw new Exception();


            var selectedItem = IsWithinRangeMixItem(suitableFoodItems, grams, foodType);

            if (selectedItem != null)
            {
                confirmedFoodList.Add(selectedItem);
                return;
            }
            else
            {
                selectedItem = IsWithinRangeMixItem(suitableFoodItems, grams, foodType, 10);
                if (selectedItem != null)
                {
                    confirmedFoodList.Add(selectedItem);
                    return;
                }
                selectedItem = IsWithinRangeMixItem(suitableFoodItems, grams, foodType, 20);
                if (selectedItem != null)
                {
                    confirmedFoodList.Add(selectedItem);
                    return;
                }
                selectedItem = IsWithinRangeMixItem(suitableFoodItems, grams, foodType, 40);
                if (selectedItem != null)
                {
                    confirmedFoodList.Add(selectedItem);
                    return;
                }
                selectedItem = IsWithinRangeMixItem(suitableFoodItems, grams, foodType, 60);
                if (selectedItem != null)
                {
                    confirmedFoodList.Add(selectedItem);
                    return;
                }
                selectedItem = IsWithinRangeMixItem(suitableFoodItems, grams, foodType, 80);
                if (selectedItem != null)
                {
                    confirmedFoodList.Add(selectedItem);
                    return;
                }

            }
        }

        public double RunMacroBalancer(MealType mealType, FoodType foodType, double grams)
        {
            var suitableCarbs = FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == FoodType.Carb);
            var suitableProteins = FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == FoodType.Protein);
            var suitableFats = FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == FoodType.Fat);
            var suitableVeg = FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == FoodType.Veg);
            var suitableSnacks = FoodBank.Where(i => i.AppropriateMeals.Contains(mealType) && i.Type == FoodType.Fruit || i.Type == FoodType.Fat);

            var averageXInMeatFishFood = new double();
            var averageXInStarchyFood = new double();
            var averageXInFatBasedFood = new double();
            var averageXInVegFood = new double();
            var averageXInSnackFood = new double();

            //update the grams for a true reflection e.g. so we take into account the protein in the starchy foods and veg too as well as meat dishes
            switch (foodType)
            {
                case FoodType.Protein:
                    {
                        averageXInStarchyFood = FindAverageProteinGrams(suitableCarbs.ToList());
                        averageXInFatBasedFood = FindAverageProteinGrams(suitableFats.ToList());
                        averageXInVegFood = FindAverageProteinGrams(suitableVeg.ToList());
                        grams = grams - (averageXInStarchyFood + averageXInFatBasedFood + averageXInVegFood);
                    }
                    break;
                case FoodType.Carb:
                    {
                        averageXInMeatFishFood = FindAverageCarbGrams(suitableProteins.ToList());
                        averageXInFatBasedFood = FindAverageCarbGrams(suitableFats.ToList());
                        averageXInVegFood = FindAverageCarbGrams(suitableVeg.ToList());
                        grams = grams - (averageXInMeatFishFood + averageXInFatBasedFood + averageXInVegFood);
                    }
                    break;
                case FoodType.Fat:
                    {
                        averageXInStarchyFood = FindAverageFatGrams(suitableCarbs.ToList());
                        averageXInMeatFishFood = FindAverageFatGrams(suitableProteins.ToList());
                        averageXInVegFood = FindAverageFatGrams(suitableVeg.ToList());
                        grams = grams - (averageXInStarchyFood + averageXInMeatFishFood + averageXInVegFood);
                    }
                    break;
                case FoodType.Mix:
                    {
                        averageXInSnackFood = FindAverageMixGrams(suitableSnacks.ToList());
                      return grams = grams - averageXInSnackFood;
                    }
            }
            return grams;
        }

        public List<FoodItem> FindLocalShopsItems(Plan plan, List<string> localShopsList, MealType mealType, double proteinPerMeal, double carbsPerMeal, 
            double fatPerMeal, List<FoodItem> currentSnacks)
        {
            var suitableItemsList = new List<FoodItem>();
            //add all the retailor items to a list
            foreach (var i in localShopsList)
            {
                suitableItemsList.AddRange(FoodBank.Where(fi => fi.Retailor == i));
            }


            var confirmedFoodList = new List<FoodItem>();

            var grams = proteinPerMeal + fatPerMeal + carbsPerMeal;
            //check if we have added a snack for lunch already if we have then update the grams for the meal
            if (currentSnacks != null && currentSnacks.Count > 0)
            {
                grams = grams - currentSnacks.Average(i => i.TotalMixGrams);
            }

            AddItem(plan.User.NutritionPlan, confirmedFoodList, suitableItemsList, 2, FoodType.Mix, mealType, grams);
            
            return confirmedFoodList;
        }


        public FoodItem ReturnHighestProtein(List<FoodItem> itemsToCompare)
        {
            return itemsToCompare.FirstOrDefault(i=> i.GramsOfProtein == itemsToCompare.Max(mx => mx.GramsOfProtein));
        }


    }
}