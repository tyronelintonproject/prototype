﻿using AppPrototype.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using AppPrototype.Models.CustomTypes.Dictionaries;

namespace AppPrototype.Models.CustomTypes.MealLogic
{
    public class MacroGenerator
    {
        private WeightConverter WeightConverter = new WeightConverter();
        private Dictionaries.UserActivityLevel Dictionary = new Dictionaries.UserActivityLevel();

        //Takes in a string (height in foot) and Converts height to Cm returns as a double
        private double ConvertHeight(string height)
        {
            //it will come as 5 foot 7, 5 ft 7, 5'7, 5.7, 
            //Can i just take the numbers from that? Yes. then i can put those 2 numbers into a list of ints. and 
            string[] numbers = Regex.Split(height, @"\D+");
            double cm;
            double inc = ((Convert.ToDouble(numbers[0])) * 12) + Convert.ToDouble(numbers[1]);
            cm = inc * 2.54;
            return cm;
        }

        //Takes in a string (weight in foot) and Converts weight to Cm returns as a double
        private double ConvertWeight(string weight, WeightMethod optedWeightMethod)
        {
            //how they gona give us their weight? it could be either stone, kg, or pounds, theirfore i should ask them to write "kg" for kg "stone" for stone
            //etc, and then il need a way to decipher which they have opted for, a property in the user object would be easiest
            double pounds = 0;

            switch (optedWeightMethod)
            {
                case WeightMethod.Stone:
                    {
                        //this ones stone
                        string[] numbers = Regex.Split(weight, @"\D+");
                        double stone = Convert.ToDouble(numbers[0]);
                        pounds = stone * 14;
                    }
                    break;

                case WeightMethod.Kg:
                    {
                        //this ones kg
                        string[] numbers = Regex.Split(weight, @"\D+");
                        double kg = Convert.ToDouble(numbers[0]);
                        pounds = kg * 2.20462;
                    }
                    break;

                case WeightMethod.Pound:
                    {
                        //this ones pounds, but were not going to convert pounds as this is the apps weight method 
                        string[] numbers = Regex.Split(weight, @"\D+");
                        pounds = Convert.ToDouble(numbers[0]);
                    }
                    break;
            }

            return pounds;
        }


        private void UpdateUserDetails(User user, double weight, double height, int age, string userActivityLevel)
        {
            user.Age = age;
            user.Weight = weight;
            user.Height = height;

            //get the value for the userActivityLevel from the dictionary
            Dictionary.dictionary.TryGetValue(userActivityLevel, out double level);

            user.UserActivityLevel = level;
        }

        private void SetCalories(User user, int goalCalorieDifference)
        {
            var calories = (10 * WeightConverter.ConvertPoundsToKg(user.Weight)) + (6.25 * user.Height) - (5 * user.Age) + 5;
            var caloriesWithActivityConsidered = calories * user.UserActivityLevel;
            var caloriesBasedOnUserGoal = caloriesWithActivityConsidered + goalCalorieDifference;

            user.Calories = caloriesBasedOnUserGoal;
        }

        private void SetMacros(Plan plan)
        {   //Lightly Active
            if (plan.User.UserActivityLevel == 1.375 && plan.WeightGoal == WeightGoal.LooseFat)
            {   //Done
                UpdateUsersMacros(plan, 1.2, 0.50);
            }
            if (plan.User.UserActivityLevel == 1.375 && plan.WeightGoal == WeightGoal.ToneUp)
            {
                UpdateUsersMacros(plan, 1.2, 0.40);
            }
            if (plan.User.UserActivityLevel == 1.375 && plan.WeightGoal == WeightGoal.GainMuscle)
            {
                UpdateUsersMacros(plan, 1.2, 0.25);
            }
            //Moderately Active
            if (plan.User.UserActivityLevel == 1.55 && plan.WeightGoal == WeightGoal.LooseFat)
            {
                UpdateUsersMacros(plan, 1.2, 0.50);
            }
            if (plan.User.UserActivityLevel == 1.55 && plan.WeightGoal == WeightGoal.ToneUp)
            {
                UpdateUsersMacros(plan, 1.2, 0.40);
            }
            if (plan.User.UserActivityLevel == 1.55 && plan.WeightGoal == WeightGoal.GainMuscle)
            {
                UpdateUsersMacros(plan, 1.2, 0.30);
            }
            //Very Active
            if (plan.User.UserActivityLevel == 1.725 && plan.WeightGoal == WeightGoal.LooseFat)
            {
                UpdateUsersMacros(plan, 1.2, 0.40);
            }
            if (plan.User.UserActivityLevel == 1.725 && plan.WeightGoal == WeightGoal.ToneUp)
            {
                UpdateUsersMacros(plan, 1.2, 0.30);
            }
            if (plan.User.UserActivityLevel == 1.725 && plan.WeightGoal == WeightGoal.GainMuscle)
            {
                UpdateUsersMacros(plan, 1.2, 0.20);
            }
            //Extra Active
            if (plan.User.UserActivityLevel == 1.9 && plan.WeightGoal == WeightGoal.LooseFat)
            {
                UpdateUsersMacros(plan, 1.5, 0.40);
            }
            if (plan.User.UserActivityLevel == 1.9 && plan.WeightGoal == WeightGoal.ToneUp)
            {
                UpdateUsersMacros(plan, 1.5, 0.30);
            }
            if (plan.User.UserActivityLevel == 1.9 && plan.WeightGoal == WeightGoal.GainMuscle)
            {
                UpdateUsersMacros(plan, 1.5, 0.20);
            }
        }
        //            Moderate && Loose then { do this--protein == 1.2g PP of bodyweight, fats 40 % of remaining, the remainder}
        //            --why because this person doesn't need to much carbs as they want to loose n they are only moderately active. 

        //Moderate && Maintain then { do this}
        //            Moderate && Gain then { protein == 1.2g PP of bodyweight, fats 30 % of remaining, the remainder}
        //            Very Active && Loose then { }
        //            Very Active && Maintain then { }
        //            Very Active && Gain then { protein == 1.3g PP of bodyweight, fats 20 % of remaining, the remainder}
        //}


        private void UpdateUsersMacros(Plan plan, double proteinValue, double fatValue)
        {
            //create a new MacroList for the user
            var userMacros = new MacroList();

            //store the calories in a variable
            var calories = plan.User.Calories;

            //work out the protein
            var protein = proteinValue * plan.User.Weight;
            //minus the protein from the calories
            calories = calories - (protein * 4);//x4 to get the caloric value 

            //work out the fats
            var fats = calories * fatValue;
            //minus the carbs from the calories
            calories = calories - fats;

            //work out the carbs

            var carbs = calories;
            //minus the carbs from the calories
            calories = calories - carbs;

            if (calories == 0)
            {
                userMacros.Protein = protein;
                userMacros.Carbs = carbs / 4;
                userMacros.Fat = fats / 9;

                //update the plan.User.Macros
                plan.User.NutritionPlan.MacroList = userMacros;
            }
        }

        public void Generate(Plan plan, string weight, string height, int age, string userActivityLevel, WeightMethod optedWeightMethod, int goalCalorieDifference)
        {
            UpdateUserDetails(plan.User, ConvertWeight(weight, optedWeightMethod), ConvertHeight(height), age, userActivityLevel);
            SetCalories(plan.User, goalCalorieDifference);
            SetMacros(plan);
        }


}
}
