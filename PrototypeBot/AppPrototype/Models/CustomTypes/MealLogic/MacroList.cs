﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppPrototype.Models.CustomTypes.MealLogic
{
    [Serializable]
    public class MacroList
    {
        public double Protein { get; set; }
        public double Carbs { get; set; }
        public double Fat { get; set; }
    }
}
