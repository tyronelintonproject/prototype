﻿using AppPrototype.Models.CustomTypes.Food;
using AppPrototype.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppPrototype.Models.CustomTypes.MealLogic
{
    public class MealPlanGenerator
    {
        private FoodFinder FoodFinder { get; set; }

        public MealPlanGenerator()
        {
            FoodFinder = new FoodFinder();
        }


        public void SetNutritionPlanType(User user, NutritionPlanType nutritionPlanType)
        {
            user.NutritionPlan.NutrtionPlanType = nutritionPlanType;
        }

        public void SetWhichMealsArePlanned(User user, string plannedMeals)
        {
            string Breakfast = "Breakfast";
            string Lunch = "Lunch";
            string Dinner = "Dinner";
            string Snack = "Snack";

            if (plannedMeals.Contains(Breakfast) || plannedMeals.Contains(Breakfast.ToLowerInvariant()))
            user.NutritionPlan.HasBreakfast = true;
            if(plannedMeals.Contains(Lunch) || plannedMeals.Contains(Lunch.ToLowerInvariant()))
                user.NutritionPlan.HasLunch = true;
            if(plannedMeals.Contains(Dinner) || plannedMeals.Contains(Dinner.ToLowerInvariant()))
                user.NutritionPlan.HasDinner = true;
            if (plannedMeals.Contains(Snack) || plannedMeals.Contains(Snack.ToLowerInvariant()))
                user.NutritionPlan.HasSnack = true;
        }

        private double CalculateMacrosPerMeal(NutritionPlan nutritionPlan, FoodType foodType)
        {
            var counter = 0;

            if (nutritionPlan.HasBreakfast)
                counter++;
            if (nutritionPlan.HasLunch)
                counter++;
            if (nutritionPlan.HasDinner)
                counter++;
            if (nutritionPlan.HasSnack)
                counter++;


            if (foodType == FoodType.Protein)
            {
                return (counter == 0) ? 0 : nutritionPlan.MacroList.Protein / Convert.ToDouble(counter);
            }
            if (foodType == FoodType.Carb)
            {
                return (counter == 0) ? 0 : nutritionPlan.MacroList.Carbs / Convert.ToDouble(counter);
            }
            if (foodType == FoodType.Fat)
            {
                return (counter == 0) ? 0 : nutritionPlan.MacroList.Fat / Convert.ToDouble(counter);
            }

            return 0;
        }
        
        
        //use the foodFinder to populate the breakfast protein options, carb options, etc
                //food finder go and get me my breakfastProteins this is how many grams i have (Calls the above method) .. CalculateMacrosPerMeal(Protein)
                //pop them in the list once youve found them, here are some rules


                //so lets say i want to find my protein options for X grams of protein lets say X is 20 grams in this example
                //so i look i have my foodSearcher and i tell it i want suitable Breakfast proteins i need 20 grams worth.
                //the foodSearcher goes to the food bank and gets a list of breakfast protein items it knows it needs 4 options so it firstly takes the items which are
                //very close to the 20 grams like an "omlette"(this contains 16grams lets say, as it includes 3 eggs) if it has 4 options it simply returns and goes to get the carb BreakfastItems
                //if its only got 2 it will generate and return a new protein Breakfast option e.g. it will return an egg X 3 which is 16grams.
                //NOTE: it can only contain 2 options from the same foodType e.g. Egg, or Nutts, etc so you can only have 2 variations of egg like an omlette or scrambled.
                //It will repeat the proces for carbs. (maybe for breakfast it will inlude a fruit option too)
                //it will then populate the breakfastProteinsList in the Breakfast object.
                //We then assign this breakfast variable to the users.Breakfast object.
                //We do the same for the other mealTypes.

        public void GenerateMeals(Plan plan, string weight, string height, int age, UserActivityLevel userActivityLevel, WeightMethod weightMethod)
        {
            //Add 100 cals daily if user wants to gain weight or minus if not.

            if (weight.Contains("unsure") || string.IsNullOrEmpty(weight))
                throw new InvalidOperationException();

            var calDiff = 0;
            if (plan.WeightGoal == WeightGoal.GainMuscle)
            {
                calDiff = 100;
            }
            else
            {
                calDiff = -100;
            }

            new MacroGenerator().Generate(plan, weight, height, age, userActivityLevel.ToString(), weightMethod, calDiff);

            var proteinPerMeal = CalculateMacrosPerMeal(plan.User.NutritionPlan, FoodType.Protein);
            var carbsPerMeal = CalculateMacrosPerMeal(plan.User.NutritionPlan, FoodType.Carb);
            var fatPerMeal = CalculateMacrosPerMeal(plan.User.NutritionPlan, FoodType.Fat);
          
            if (plan.User.NutritionPlan.HasBreakfast)
            {
                GenerateBreakfast(plan, proteinPerMeal, carbsPerMeal, fatPerMeal);
            }
            if (plan.User.NutritionPlan.HasLunch)
            {
               GenerateLunch(plan, proteinPerMeal, carbsPerMeal, fatPerMeal, new List<string> { "Mc Donalds","Greggs"});
            }
            if (plan.User.NutritionPlan.HasDinner)
            {
                GenerateDinner(plan, proteinPerMeal, carbsPerMeal, fatPerMeal);
            }
            if (plan.User.NutritionPlan.HasSnack)
            {
                GenerateSnacks(plan, proteinPerMeal, carbsPerMeal, fatPerMeal);
            }
        }

        public void GenerateLunch(Plan plan, double proteinPerMeal, double carbsPerMeal, double fatPerMeal, List<string> localShopsList = null)
        {
            if (localShopsList != null && localShopsList.Count > 0)
            {
                //do i need to add lunch snacks like below?
                plan.User.NutritionPlan.Lunches.LunchFruit = FoodFinder.FindLunchSnacks(plan, MealType.Lunch, FoodType.Fruit, proteinPerMeal + carbsPerMeal + fatPerMeal, SnackAdder.LocalShops, localShopsList);
                
                //the position of this method is imp it waits for the above to add the snack and then gets it into a list then we we call below 
                //it knows weve been successful and added a snack.
                var snackList = plan.User.NutritionPlan.Lunches.LunchFats.Concat(plan.User.NutritionPlan.Lunches.LunchFruit).ToList();

                plan.User.NutritionPlan.Lunches.LunchMixs = FoodFinder.FindLunchMixes(plan, MealType.Lunch, FoodType.Mix, proteinPerMeal + carbsPerMeal + fatPerMeal,snackList);

                plan.User.NutritionPlan.Lunches.LunchRetailorMixs = FoodFinder.FindLocalShopsItems(plan, localShopsList, MealType.Lunch, proteinPerMeal, carbsPerMeal,
                    fatPerMeal, snackList);
            }
            else
            {
                plan.User.NutritionPlan.Lunches.LunchFruit = FoodFinder.FindLunchSnacks(plan, MealType.Lunch, FoodType.Fruit, proteinPerMeal + carbsPerMeal + fatPerMeal, SnackAdder.Sandwich);
                plan.User.NutritionPlan.Lunches.LunchFats = FoodFinder.FindLunchSnacks(plan, MealType.Lunch, FoodType.Fat, proteinPerMeal + carbsPerMeal + fatPerMeal, SnackAdder.Sandwich);

                var snackList = plan.User.NutritionPlan.Lunches.LunchFats.Concat(plan.User.NutritionPlan.Lunches.LunchFruit).ToList();

                plan.User.NutritionPlan.Lunches.LunchMixs = FoodFinder.FindLunchMixes(plan, MealType.Lunch, FoodType.Mix, proteinPerMeal + carbsPerMeal + fatPerMeal, snackList);
                plan.User.NutritionPlan.Lunches.LunchMixs = FoodFinder.FindLunchMixes(plan, MealType.Lunch, FoodType.Mix, proteinPerMeal + carbsPerMeal + fatPerMeal, snackList);
                plan.User.NutritionPlan.Lunches.LunchProteins = FoodFinder.LunchFinder(plan, MealType.Lunch, FoodType.Protein, proteinPerMeal);
                plan.User.NutritionPlan.Lunches.LunchCarbs = FoodFinder.LunchFinder(plan, MealType.Lunch, FoodType.Carb, carbsPerMeal);
            }

        }

        public void GenerateSnacks(Plan plan, double proteinPerMeal, double carbsPerMeal, double fatPerMeal, List<string> localShopsList = null)
        {
                plan.User.NutritionPlan.Snacks.SnackMixes = FoodFinder.FindLunchSnacks(plan, MealType.Snack, FoodType.Mix, proteinPerMeal + carbsPerMeal + fatPerMeal, SnackAdder.Sandwich);
        }

        public void GenerateDinner(Plan plan, double proteinPerMeal, double carbsPerMeal, double fatPerMeal)
        {
            //use the foodFinder to populate the dinner protein options, carb options, etc
            plan.User.NutritionPlan.Dinners.DinnerProteins = FoodFinder.FindFood(plan, MealType.Dinner, FoodType.Protein, proteinPerMeal);
            plan.User.NutritionPlan.Dinners.DinnerCarbs = FoodFinder.FindFood(plan, MealType.Dinner, FoodType.Carb, carbsPerMeal);
            plan.User.NutritionPlan.Dinners.DinnerVeg = FoodFinder.FindFood(plan, MealType.Dinner, FoodType.Veg, 0);
        }

        public void GenerateBreakfast(Plan plan, double proteinPerMeal, double carbsPerMeal, double fatPerMeal)
        {
            //use the foodFinder to populate the dinner protein options, carb options, etc
            plan.User.NutritionPlan.Breakfasts.BreakfastProteins = FoodFinder.FindFood(plan, MealType.Breakfast, FoodType.Protein, proteinPerMeal);
            plan.User.NutritionPlan.Breakfasts.BreakfastCarbs = FoodFinder.FindFood(plan, MealType.Breakfast, FoodType.Carb, carbsPerMeal);
            plan.User.NutritionPlan.Breakfasts.BreakfastFruit = FoodFinder.FindFood(plan, MealType.Breakfast, FoodType.Fruit, 0);
        }

    }
}
