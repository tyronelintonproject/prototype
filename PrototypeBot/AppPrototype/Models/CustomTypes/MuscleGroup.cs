﻿using AppPrototype.Models.Enums;
using System;

namespace AppPrototype.Models.CustomTypes
{
    [Serializable]
    public class MuscleGroup
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Size Size { get; set; }
        public GroupType GroupType { get; set; }
    }
}
