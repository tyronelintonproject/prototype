﻿using AppPrototype.Models.CustomTypes.Food;
using AppPrototype.Models.CustomTypes.MealLogic;
using AppPrototype.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppPrototype.Models.CustomTypes
{
    [Serializable]
    public class NutritionPlan
    { 
        public MacroList MacroList { get; set; }
        public bool HasBreakfast { get; set; }
        public bool HasLunch { get; set; }
        public bool HasDinner { get; set; }
        public bool HasSnack { get; set; }
        
        public Breakfast Breakfasts { get; set; }
        public Lunch Lunches { get; set; }
        public Dinner Dinners { get; set; }
        public Snack Snacks { get; set; }
        public NutritionPlanType NutrtionPlanType { get; set; }

        public NutritionPlan()
        {
            MacroList = new MacroList();
            Breakfasts = new Breakfast();
            Lunches = new Lunch();
            Dinners = new Dinner();
            Snacks = new Snack();

        }

    }



}


