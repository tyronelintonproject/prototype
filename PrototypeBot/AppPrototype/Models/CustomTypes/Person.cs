﻿using AppPrototype.Models.Enums;
using System;

namespace AppPrototype.Models
{
    [Serializable]
    public class Person
    {
        //Props direct from user
        public int Id { get; set; }
        public string Name { get; set; } //Maybe?
        public FitnessLevel FitnessLevel { get; set; }
        public Activity Activity { get; set; }
        public TimeSpan SetTime { get; set; }
        public TimeSpan RestTime { get; set; }
        public Age Age { get; set; }
        public Models.Enums.Gender Gender { get; set; }
        public int DaysPerWeek { get; set; }
        public WeightGoal WeightGoal { get; set; }
        public int Calories { get; set; }
        public int Protein { get; set; }
        public bool EnjoysTraining { get; set; }
        public bool StrugglesToEatHealthy { get; set; }
        public GymOrHome GymHome { get; set; }
        public bool HasTrainedBefore { get; set; }
        public bool HaveARegularRoutine { get; set; }
        public int Exercises { get; set; }
        public bool DoesYourBodyBringYouDown { get; set; }

        //Indirect props (Assumption Props)  --> we could add the above props up and if more than 6/10 e.g. they set the assumption prop to true?

        //HealthAndFitnessKnowledge
        public int FitnessKnowledgeLevel
        {
            get
            {
                if (FitnessLevel == FitnessLevel.Advanced)
                    return 10;
                if (FitnessLevel != FitnessLevel.Beginner && HasTrainedBefore)
                    return 8;
                if (Gender == Enums.Gender.M && FitnessLevel != FitnessLevel.Beginner)
                    return 6;
                else return 5;
            }
        }

        //InterestsAndLifestyle
        public bool IsIntoSports
        {
            get
            {
                return (Gender == Enums.Gender.M && GymHome == GymOrHome.Gym);
            }
        }
        public bool HasABusySocialLife
        {
            get
            {
                return ((Age == Age.Under25 || Age == Age.TwentyFiveToThirty) && StrugglesToEatHealthy);
            }
        }

        public bool HasABusyFamilyLife
        {
            get
            {
                return (Age != Age.Under25 && Gender == Enums.Gender.F) || ((Age == Age.OverForty || Age == Age.ThirtyToForty) && Gender == Enums.Gender.M);
            }
        }

        public bool WorksAlotOfHours
        {
            get
            {
                if (StrugglesToEatHealthy) return true;
                else return false;
            }
            set
            {
                WorksAlotOfHours = value;
            }
        }
        //if i dont allow these variables to be set i loose the flexibility of being able to ask the user the additional q's for example this workalothours could be directly asked to the user, and i could then set the
        //struggles to eat healthy based on this? 
        //take their age, struggles to eat healthy
        public bool IsInARelationship { get { return (HasABusySocialLife || HasABusyFamilyLife); } set { IsInARelationship = value; } }
        public int TechnologyLevel
        {
            get
            {
                if (Age == Age.Under25)
                    return (10);
                if (Age == Age.TwentyFiveToThirty)
                    return (8);
                if (Age == Age.ThirtyToForty)
                    return (6);
                return (4);
            }
        }

        //EmotionsAndViews

        //if sex male && fitness level != advanced && train at home || or //if sex male && fitness level == beginner || male and weightgoal looseweight || female and looseweight
        //female training at home and beginner
        public bool IsLowInConfidence
        {
            get
            {           //man and not very the most fit          //man trainin at home  //unfit female
                return ((Gender == Enums.Gender.M && FitnessLevel != FitnessLevel.Advanced) || (Gender == Enums.Gender.M && GymHome == GymOrHome.Home) || (Gender == Enums.Gender.F && FitnessLevel == FitnessLevel.Beginner));
            }
            set { IsLowInConfidence = value; }
        }
        public bool IsDepressedBecauseOfWeight { get { return (WeightGoal == WeightGoal.LooseFat && FitnessLevel == FitnessLevel.Beginner); } } //take the question, weightgoal, enjoys training, hastrainedbefore, fitness level << e.g. if more than 4 of these fit the criteria add 1 point if more than 3 points 
                                                                                                                                                //set this to true.
        public bool WantsAttention { get { return ((Age == Age.Under25 || Age == Age.TwentyFiveToThirty) && FitnessLevel != FitnessLevel.Beginner); } } //weightgoal isnt loose fat (because if u want to loosfat ur not lookin for attention just yet) 

        //trying to loosefat     poor fitness          hasnt got great fitness and is trying to tone or loose
        public bool NeedsMotivation { get { return ((WeightGoal == WeightGoal.LooseFat) || (FitnessLevel == FitnessLevel.Beginner) || (FitnessLevel != FitnessLevel.Beginner && WeightGoal != WeightGoal.GainMuscle)); } set { NeedsMotivation = value; } }
        public bool IsANoNosenseKindOfPerson { get; set; } //based off if their question was "blunt"
        public bool IsEatingEnough { get; set; }
    }


}



