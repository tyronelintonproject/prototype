﻿using System;
using System.Collections.Generic;
using System.Text;
using AppPrototype.Models.CustomTypes;
using AppPrototype.Models.Enums;

namespace AppPrototype.CustomTypes
{
    [Serializable]
    public class Route
    {
        public string Id { get; }

        public List<MuscleGroup> MuscleGroups = new List<MuscleGroup>()
        {
            new MuscleGroup(){ GroupType = GroupType.Resistance, Name = "Shoulders", Size = Size.Large},
            new MuscleGroup(){ GroupType = GroupType.Resistance, Name = "Chest", Size = Size.Large},
            new MuscleGroup(){ GroupType = GroupType.Resistance, Name = "Triceps",Size = Size.Small},
            new MuscleGroup(){ GroupType = GroupType.Resistance, Name = "Back", Size = Size.Large},
            new MuscleGroup(){ GroupType = GroupType.Resistance, Name = "Biceps", Size = Size.Small},
            new MuscleGroup(){ GroupType = GroupType.Resistance, Name = "Core",Size = Size.Small},
            new MuscleGroup(){ GroupType = GroupType.Resistance, Name = "Qauds", Size = Size.Large},
            new MuscleGroup(){ GroupType = GroupType.Resistance, Name = "Hamms",Size = Size.Small},
            new MuscleGroup(){ GroupType = GroupType.Resistance, Name = "Calves", Size = Size.Small},
            new MuscleGroup(){ GroupType = GroupType.Resistance, Name = "Glutes",Size = Size.Small},
            new MuscleGroup(){ GroupType = GroupType.Resistance, Name = "Traps",Size = Size.Small},
            new MuscleGroup(){ GroupType = GroupType.Cardiovasular, Name = "Full",Size = Size.Large},
            new MuscleGroup(){ GroupType = GroupType.Resistance, Name = "AllFullBodySession",Size = Size.Large},
        };

        public Route(string id)
        {
            Id = id;
        }

        public virtual Plan Execute(Plan plan)
        {
            //This method takes in an unprocessed plan meaning that the plan has all the user/plan variables but it has not been populated with sessions and exercises
            return plan;
        } 

    }
}
