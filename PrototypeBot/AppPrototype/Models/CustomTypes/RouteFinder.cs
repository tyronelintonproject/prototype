﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppPrototype.Models.CustomTypes;
using AppPrototype.Models.Enums;
using AppPrototype.Models.Routes;

namespace AppPrototype.CustomTypes
{
    public class RouteFinder
    {
        private List<Route> Routes { get; set; }

        public RouteFinder()
        {
            var route1 = new WeightliftingPPLGainMuscle("WeightliftingPPLGainMuscle");
            var route2 = new Route("WeightliftingFullBodyLooseFat");         
            var route3 = new Route("BodyweightTrainingFullBodyLooseFat");
            var route4 = new Route("BodyweightTrainingPPLLooseFat");
            var route5 = new Route("WeightliftingPPLLooseFat");
            var route6 = new Route("CardioCardioMixLooseFat");
            var route7 = new Route("WeightliftingFullBodyToneUp");
            var route8 = new Route("WeightliftingFullBodyGainMuscle");
            var route9 = new Route("CardioCircuitLooseFat");

            Routes = new List<Route>(){route1,route2,route3,route4,route5,route6,route7,route8,route9};
        }

        public void FindAndExecute(Plan plan)
        {
            TrainingSplit trainingSplit;

            if (plan.DaysPerWeek < 4)
            {
                trainingSplit = (plan.Activity == Activity.Cardio) ? TrainingSplit.Circuit : TrainingSplit.FullBody; 
            }
            else
            {
                trainingSplit = (plan.Activity == Activity.Cardio) ? TrainingSplit.CardioMix : TrainingSplit.PPL;
            }

            var selectedRoute = Routes.SingleOrDefault(
                i => i.Id == plan.Activity.ToString() + trainingSplit.ToString() + plan.WeightGoal.ToString());

            if (selectedRoute != null)
            {
                plan.RouteId = selectedRoute.Id;
                selectedRoute.Execute(plan);
            }
            else
            {
                throw new InvalidOperationException();
            }
        }
        
    }
}
