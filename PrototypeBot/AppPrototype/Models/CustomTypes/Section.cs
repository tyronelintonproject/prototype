﻿using System;
using System.Collections.Generic;

namespace AppPrototype.Models.CustomTypes
{
    [Serializable]
    public class Section
    {
        public List<MuscleGroup> MuscleGroups { get; set; }
        public bool IsSuperSetted { get; set; }
        public TimeSpan AvailableTime { get; set; }
        public TimeSpan UsedTime { get; set; }
        public List<Exercise> Exercises { get; set; }
        public int NoOfSuperSets { get; set; }
        public int NoOfNormalSets { get; set; }
        public int TotalSets { get { return NoOfNormalSets + NoOfSuperSets; } }

        public Section()
        {
            Exercises = new List<Exercise>();
            MuscleGroups = new List<MuscleGroup>();
        }

    }
}
