﻿using System;
using System.Collections.Generic;
using System.Linq;
using AppPrototype.Models.Enums;

namespace AppPrototype.Models.CustomTypes
{
    [Serializable]
    public class Session
    {
        public int Version { get; set; }
        public string Name { get; set; }
        public int PlanId { get; set; }
        public List<Section> Sections { get; set; }
        public Activity ActivityType { get; set; }
        public bool IsOverPrescribed { get; set; }
        public MuscleGroup PrioritizedMuscleGroup { get; set; }
        public bool IsPriortized { get; set; }
        public int NoOfRounds { get; set; }
        public int ExPerRound { get; set; }
        public int NoOfSuperSets { get; set; }
        public int NoOfNormalSets { get; set; }
        public Level Level { get; set; }
        public List<SessionGoal> SessionGoals { get; set; }
        public int TotalSets
        {
            get { return NoOfNormalSets + NoOfSuperSets; }
        }

        public TimeSpan AvailableTime { get; set; }

        public TimeSpan UsedTime
        {
            get
            {
                if (Sections.Count == 0 || Sections.FirstOrDefault().Exercises.Count == 0)
                    return new TimeSpan();

                TimeSpan usedTime = new TimeSpan();

                foreach (var section in Sections)
                {
                    usedTime += section.UsedTime;
                }
                return usedTime;
            }
        }

        public Session()
        {
           Sections = new List<Section>();
           SessionGoals = new List<SessionGoal>();
           PrioritizedMuscleGroup = new MuscleGroup();

        }

    }
}
