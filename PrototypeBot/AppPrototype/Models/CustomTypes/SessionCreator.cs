﻿using AppPrototype.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppPrototype.Models.CustomTypes
{
    public class SessionCreator
    {
        private ExerciseAdder _exAdder;
        private SetCalculator _calc;

        public SessionCreator()
        {
            _exAdder = new ExerciseAdder();
            _calc = new SetCalculator();
        }

        public void Dedicate(Plan plan, Session session,List<Session> containingSessions, MuscleGroup selectedMG, ResultExpectancy resultExpectancy, List<Exercise> dontIncludeTheseExercises, Caller caller = Caller.None)
        {
            session.IsPriortized = true;

            if (plan.SessionLength <= new TimeSpan(00, 30, 00))
            {   //find the section to prioritize

                Section prioritizedSection;

                if (session.Sections.Any(sec => sec.MuscleGroups.Any(mg => mg.Name == selectedMG.Name)))
                {
                    prioritizedSection = session.Sections.FirstOrDefault(i => i.MuscleGroups.Any(mg => mg.Name == selectedMG.Name));
                }
                else
                {
                    prioritizedSection = session.Sections.FirstOrDefault();
                }
                //remove any other mgs from section
                prioritizedSection.MuscleGroups = new List<MuscleGroup>{ selectedMG };
                //priortize the section time
                prioritizedSection.AvailableTime = session.AvailableTime;
                //superset the section for max growth
                prioritizedSection.IsSuperSetted = true;

                //removeOtherSections
                session.Sections = new List<Section>() { prioritizedSection };

                    RefreshSession(session);
                    //calc sets
                    _calc.CalculateSets(plan, session, true);
                    //add exercises
                    _exAdder.PopulateOneSession(plan, session, (plan.DaysPerWeek <= 3) ? UpdateType.Dedicate : UpdateType.None, caller, resultExpectancy);
                                
            }
        }


        public void Prioritize(Plan plan, Session session, List<Session> containingSessions, MuscleGroup selectedMG, ResultExpectancy resultExpectancy, List<Exercise> dontIncludeTheseExercises, Caller caller = Caller.None)
        {
            try
            {
                //set it priortized
                session.IsPriortized = true;

                if (caller == Caller.IWantToLoose)
                {
                    RefreshSession(session);
                    _calc.CalculateSets(plan, session, true);
                    session.PrioritizedMuscleGroup = selectedMG;
                    _exAdder.PopulateOneSession(plan, session, (plan.DaysPerWeek <= 3) ? UpdateType.Prioritize : UpdateType.None, caller, resultExpectancy);
                    
                    return;
                }

                //find the section to prioritize
                Section prioritizedSection;

                    if (session.Sections.Any(sec => sec.MuscleGroups.Any(mg => mg.Name == selectedMG.Name)))
                    {
                        prioritizedSection = session.Sections.FirstOrDefault(i => i.MuscleGroups.Any(mg => mg.Name == selectedMG.Name));
                    }
                    else
                    {
                        prioritizedSection = session.Sections.FirstOrDefault();
                    }
                    //remove any other mgs from section
                    prioritizedSection.MuscleGroups = new List<MuscleGroup> { selectedMG };
                    //priortize the section time
                    prioritizedSection.AvailableTime = new TimeSpan(00, 00, (Convert.ToInt32(plan.SessionLength.TotalSeconds) / 3) * 2);
                    //superset the section for max growth
                    prioritizedSection.IsSuperSetted = true;

                    var otherMgs = new List<MuscleGroup>();

                    foreach (var i in session.Sections)
                    {
                        otherMgs.AddRange(i.MuscleGroups.Where(mg => mg.Name != selectedMG.Name));
                    }

                    if (otherMgs != null && otherMgs.Count > 2)
                    {
                        //find section
                        var selectedSection = session.Sections.FirstOrDefault(sec => sec.MuscleGroups.All(smg => smg != selectedMG));
                        //set the mgs in the section
                        selectedSection.MuscleGroups = selectedSection.MuscleGroups.OrderBy(i => i.Size).Take(2).ToList();
                        //superset the section for max growth
                        selectedSection.IsSuperSetted = true;
                        //remove the exercises in the section
                        selectedSection.Exercises = new List<Exercise>();
                        //set time
                        selectedSection.AvailableTime = new TimeSpan(00, 00, (Convert.ToInt32(plan.SessionLength.TotalSeconds) / 3) * 1);
                        //Refresh the props of the session and sections
                        RefreshSession(session);
                        //calc sets
                        _calc.CalculateSets(plan, session, true);
                        //add exercises
                        _exAdder.PopulateOneSession(plan, session, (plan.DaysPerWeek <= 3) ? UpdateType.Prioritize : UpdateType.None, caller, resultExpectancy);
                    }
                    if (otherMgs != null && otherMgs.Count <= 2)
                    {
                        //find section
                        var selectedSection = session.Sections.FirstOrDefault(sec => sec.MuscleGroups.All(smg => smg != selectedMG));
                        //superset the section for max growth
                        selectedSection.IsSuperSetted = true;
                        //set time
                        selectedSection.AvailableTime = new TimeSpan(00, 00, (Convert.ToInt32(plan.SessionLength.TotalSeconds) / 3) * 1);
                        //remove the exercises in the section
                        selectedSection.Exercises = new List<Exercise>();
                        RefreshSession(session);
                        //calc sets
                        _calc.CalculateSets(plan, session, true);
                        //add exercises
                        _exAdder.PopulateOneSession(plan, session, (plan.DaysPerWeek <= 3) ? UpdateType.Prioritize : UpdateType.None, caller, resultExpectancy);
                    }
                }
            
            catch (Exception ex)
            {
            }
        }





        public void RefreshSession(Session session)
        {
            session.ExPerRound = 0;
            session.IsOverPrescribed = false;
            session.Name = "HasBeenUpdated";
            session.NoOfNormalSets = 0;
            session.NoOfRounds = 0;
            session.NoOfSuperSets = 0;
            
            foreach (var section in session.Sections)
            {
                section.Exercises = new List<Exercise>();
                section.NoOfNormalSets = 0;
                section.NoOfSuperSets = 0;
                section.UsedTime = new TimeSpan();
            }
        }


    }
}

