﻿
using AppPrototype.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppPrototype.Models.CustomTypes
{
    [Serializable]
    public enum SessionGoalType { None, Heavier, Harder, More }
    [Serializable]
    public enum IntensifyTechnique { None, Pause, Slow }
    [Serializable]
    public enum Level { Easy, Medium, Hard }
    [Serializable]
    public class SessionGoal
    {
        public SessionGoalType Type { get; set; }
        public bool Completed { get; set; }
        public string Name { get; set; }
        public double WeightIncrease { get; set; }
        public IntensifyTechnique IntensityOption { get; set; }
        public double DurationIncrease { get; set; }
        public int RepsIncrease { get; set; }
        public Exercise Exercise { get; set; }
    }
    [Serializable]
    public class GoalLevelContainer
    {
        public Level HeavierLevel { get; set; }
        public Level MoreLevel { get; set; }
        public Level HarderLevel { get; set; }
    }

    //public class Plan
    //{
    //    public GoalLevelContainer GoalLevelContainer { get; set; }
    //}

    public class SessionGoalGenerator
    {
        
        public void Generate(Plan plan)
        {
            //find the right goalTypes for the plan
            var goalTypes = new List<SessionGoalType>() { SessionGoalType.Harder, SessionGoalType.Heavier, SessionGoalType.More };
            var possibleGoalTypes = new List<SessionGoalType>();
            var selectedGoalType = new SessionGoalType();

            switch (plan.WeightGoal)
            {
                case WeightGoal.GainMuscle:
                    {
                        if (plan.Activity == Activity.Weightlifting)
                        {
                            possibleGoalTypes.Add(SessionGoalType.Heavier);
                            possibleGoalTypes.Add(SessionGoalType.Harder);
                            possibleGoalTypes.Add(SessionGoalType.More);
                        }
                        else
                        {
                            possibleGoalTypes.Add(SessionGoalType.Harder);
                            possibleGoalTypes.Add(SessionGoalType.More);
                        }
                    }
                    break;
                case WeightGoal.ToneUp:
                    {
                        if (plan.Activity == Activity.Weightlifting)
                        {
                            possibleGoalTypes.Add(SessionGoalType.Heavier);
                            possibleGoalTypes.Add(SessionGoalType.Harder);
                            possibleGoalTypes.Add(SessionGoalType.More);
                        }
                        else
                        {
                            possibleGoalTypes.Add(SessionGoalType.Harder);
                            possibleGoalTypes.Add(SessionGoalType.More);
                        }
                    }
                    break;
                case WeightGoal.LooseFat:
                    {
                        if (plan.Activity == Activity.Weightlifting)
                        {
                            possibleGoalTypes.Add(SessionGoalType.Heavier);
                            possibleGoalTypes.Add(SessionGoalType.Harder);
                            possibleGoalTypes.Add(SessionGoalType.More);
                        }
                        else
                        {
                            possibleGoalTypes.Add(SessionGoalType.Harder);
                            possibleGoalTypes.Add(SessionGoalType.More);
                        }
                    }
                    break;
            }

            //now i have the possible goalTypes i can look at the sessions
            foreach (var session in plan.Sessions)
            {

                //get the section
                var selectedSection = (session.IsPriortized) ? session.Sections.FirstOrDefault(i => i.MuscleGroups.Contains(session.PrioritizedMuscleGroup)) : session.Sections.FirstOrDefault();

                //Get the exercise the goal will relate to and ensure its not null
                var selectedExercise = selectedSection.Exercises.FirstOrDefault(i => i.CompOrIso == CompOrIso.Compound && i.IsSuperSetted == false);

                if (selectedExercise == null)
                {
                    selectedExercise = selectedSection.Exercises.FirstOrDefault(i => i.CompOrIso == CompOrIso.Compound);
                }
                if (selectedExercise == null)
                {
                    selectedExercise = selectedSection.Exercises.FirstOrDefault(i => i.IsSuperSetted == false);
                }
                if (selectedExercise == null)
                {
                    selectedExercise = selectedSection.Exercises.FirstOrDefault();
                }
                if (selectedExercise == null)
                    throw new Exception();

                //now look at the session and see if there is a sessionGoal existing, if there is make sure the new goalType isnt the same as the previous one
                if (session.SessionGoals.Count > 0)
                {
                    // i want to make sure its not eqaul to the last goaltype then check if theres a goal type we've not had yet
                    //if there is use that if theres not use 
                    selectedGoalType = SessionGoalType.None;

                    if (!session.SessionGoals.Any(i => i.Type == possibleGoalTypes.FirstOrDefault()))
                    {
                        selectedGoalType = possibleGoalTypes.FirstOrDefault();
                    }
                    else if (!session.SessionGoals.Any(i => i.Type == possibleGoalTypes[1]))
                    {
                        selectedGoalType = possibleGoalTypes[1];
                    }
                    if (selectedGoalType == SessionGoalType.None && possibleGoalTypes.Count() >= 3)
                    {
                        possibleGoalTypes = possibleGoalTypes.Where(i => i != session.SessionGoals.LastOrDefault().Type).ToList();
                        var rnd = new Random();
                        var result = rnd.Next();
                        if (result % 2 == 0)
                        {
                            selectedGoalType = possibleGoalTypes[0];
                        }
                        else
                        {
                            selectedGoalType = possibleGoalTypes[1];
                        }
                    }
                    if (selectedGoalType == SessionGoalType.None)
                    {
                        selectedGoalType = possibleGoalTypes.FirstOrDefault(i => i != session.SessionGoals.LastOrDefault().Type);
                    }

                }
                else
                {
                    //find out the index of the session.
                    int index = plan.Sessions.FindIndex(ses => ses == session);
                    var previousSession = new Session();

                    if (index > 0)
                    {
                        previousSession = plan.Sessions[index - 1];
                        selectedGoalType = possibleGoalTypes.FirstOrDefault(i => i != previousSession.SessionGoals.LastOrDefault().Type);
                    }
                    else
                    {
                        selectedGoalType = possibleGoalTypes.FirstOrDefault();
                    }
                }

                //so far we know the exercise we want to add the goal for and the type of goal we want to add. 
                //now we need to create the sessionGoal and assign it to the session.
                //Lets do a switch on the selectedGoalType. and then we will further break it into the plan.WeightGoal and activity.

                switch (selectedGoalType)
                {
                    case SessionGoalType.Heavier:
                        {
                            //call the method that deals with adding "heavier" goals the method should return the sessionGoal so we can add it to the session.
                            session.SessionGoals.Add(GetHeavierGoal(plan, session, selectedExercise));
                        }
                        break;
                    case SessionGoalType.Harder:
                        {
                            //call the method that deals with adding "harder" goals the method should return the sessionGoal so we can add it to the session.
                            if (selectedExercise.CanHaveReps)
                            {
                                session.SessionGoals.Add(GetHarderGoal(plan, session, selectedExercise));
                            }
                            else
                            {
                                session.SessionGoals.Add(GetMoreGoal(plan, session, selectedExercise));
                            }
                        }
                        break;
                    case SessionGoalType.More:
                        {
                            //call the method that deals with adding "more" goals the method should return the sessionGoal so we can add it to the session.
                            session.SessionGoals.Add(GetMoreGoal(plan, session, selectedExercise));
                        }
                        break;
                }
            }
        }


        private SessionGoal GetHeavierGoal(Plan plan, Session session, Exercise exercise)
        {
            var goal = new SessionGoal() { Type = SessionGoalType.Heavier };
            goal.WeightIncrease = GetWeightIncrease(plan, session);
            goal.Name = "Add " + goal.WeightIncrease + "kg to your usuall weight when doing " + exercise.Name + " you must be able to complete all of the reps";
            goal.Exercise = exercise;
            return goal;

        }

        private SessionGoal GetMoreGoal(Plan plan, Session session, Exercise exercise)
        {
            var goal = new SessionGoal() { Type = SessionGoalType.More };

            if (!exercise.CanHaveReps)
            {
                goal.DurationIncrease = GetVolumeIncrease(plan, session, exercise);
                goal.Name = "Add " + goal.DurationIncrease + " extra seconds to your usuall time when doing " + exercise.Name;
                goal.Exercise = exercise;
                return goal;
            }
            else
            {
                goal.RepsIncrease = (int) GetVolumeIncrease(plan, session, exercise);
                goal.Name = "Add " + goal.RepsIncrease + " extra reps to your usuall amount for " + exercise.Name;
                return goal;
            }
        }

        private SessionGoal GetHarderGoal(Plan plan, Session session, Exercise exercise)
        {
            var goal = new SessionGoal() { Type = SessionGoalType.Harder };
            goal.IntensityOption = GetIntensityOption(plan, session, exercise);
            goal.Exercise = exercise;

            if (goal.IntensityOption == IntensifyTechnique.Pause)
            {
                goal.Name = "When doing " + exercise.Name + " pause for " + 2 + " seconds on the downward part of movement, ensure you complete all reps";
            }
            else
            {
                goal.Name = "When doing " + exercise.Name + " do the exercise twice as slow as you normally would, ensure you complete all reps";
            }

            return goal;
        }

        private double GetWeightIncrease(Plan plan, Session session)
        {
            //What do we want to do here? 
            //We want to look at the last time we increased the weight in this session as a the goal. We need to see how much we increased it by, so that we can give a intelligent increase,
            //e.g. if they preivously increased by 5kg on a bench press its unlikely they will be able to do 5kg again this time so we do 2kg
            //what do we need first, we need there last weight increase number
            //Using this formula we can realistically build the users strength and we can also show them how much stronger they have gotten since using the app, we can show them their 1RM compared to when 
            //they first gott it. How? Wen they say i completed the goal, we ask them (if its a heavierGoal) how much can you lift for the number of reps now? then we store that and workout their 1RM

            switch (plan.GoalLevelContainer.HeavierLevel)
            {
                case Level.Easy:
                    {
                        var noOfHeavierGoals = session.SessionGoals.Where(i => i.Type == SessionGoalType.Heavier);

                        if (noOfHeavierGoals.Count() % 2 == 0)
                            return 2;
                        return 1;
                    }
                case Level.Medium:
                    {
                        var noOfHeavierGoals = session.SessionGoals.Where(i => i.Type == SessionGoalType.Heavier);

                        if (noOfHeavierGoals.Count() % 2 == 0)
                            return 4;
                        return 2;
                    }
                case Level.Hard:
                    {
                        var noOfHeavierGoals = session.SessionGoals.Where(i => i.Type == SessionGoalType.Heavier);

                        if (noOfHeavierGoals.Count() % 2 == 0)
                            return 6;
                        return 4;
                    }
            }
            return 0;
        }


        private double GetVolumeIncrease(Plan plan, Session session, Exercise exercise)
        {
            var noOfMoreGoals = session.SessionGoals.Where(i => i.Type == SessionGoalType.More);

            if (exercise.CanHaveReps)
            {
                if (plan.Activity == Activity.Weightlifting)
                {
                    switch (plan.GoalLevelContainer.MoreLevel)
                    {
                        case Level.Easy:
                            {
                                if (noOfMoreGoals.Count() % 2 == 0)
                                    return 2;
                                return 1;
                            }
                        case Level.Medium:
                            {
                                if (noOfMoreGoals.Count() % 2 == 0)
                                    return 3;
                                return 2;
                            }
                        case Level.Hard:
                            {
                                if (noOfMoreGoals.Count() % 2 == 0)
                                    return 4;
                                return 3;
                            }
                    }
                }
                else
                {
                    switch (plan.GoalLevelContainer.MoreLevel)
                    {
                        case Level.Easy:
                            {
                                if (noOfMoreGoals.Count() % 2 == 0)
                                    return 4;
                                return 2;
                            }
                        case Level.Medium:
                            {
                                if (noOfMoreGoals.Count() % 2 == 0)
                                    return 6;
                                return 4;
                            }
                        case Level.Hard:
                            {
                                if (noOfMoreGoals.Count() % 2 == 0)
                                    return 10;
                                return 6;
                            }
                    }
                }
            }
            else
            {
                switch (plan.GoalLevelContainer.MoreLevel)
                {
                    case Level.Easy:
                        {
                            if (noOfMoreGoals.Count() % 2 == 0)
                                return 8;
                            return 4;
                        }
                    case Level.Medium:
                        {
                            if (noOfMoreGoals.Count() % 2 == 0)
                                return 15;
                            return 8;
                        }
                    case Level.Hard:
                        {
                            if (noOfMoreGoals.Count() % 2 == 0)
                                return 20;
                            return 10;
                        }
                }
            }
            return 0;
        }


        private IntensifyTechnique GetIntensityOption(Plan plan, Session session, Exercise exercise)
        {
            var noOfHarderGoals = session.SessionGoals.Where(i => i.Type == SessionGoalType.Harder);
            var lastHardGoal = session.SessionGoals.LastOrDefault(i => i.Type == SessionGoalType.Harder);

            if (lastHardGoal == null)
                return IntensifyTechnique.Pause;

            if (lastHardGoal.IntensityOption == IntensifyTechnique.Pause)
                return IntensifyTechnique.Slow;
            return IntensifyTechnique.Pause;
        }



    }


}









