﻿using System;
using AppPrototype.Models.Enums;

namespace AppPrototype.Models.CustomTypes
{
    [Serializable]
    public class SessionScore
    {
        public DateTime SessionDate { get; set; }
        public Score Score { get; set; }
    }
}
