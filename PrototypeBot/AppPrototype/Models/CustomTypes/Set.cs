﻿using System;
using System.Collections.Generic;
using System.Text;
using AppPrototype.Models.Enums;

namespace AppPrototype.Models.CustomTypes
{
    [Serializable]
    public class Set
    {
        public SetType SetType { get; set; }
    }
}
