﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppPrototype.Models.Enums;

namespace AppPrototype.Models.CustomTypes
{
    public class SetCalculator
    {
        private double _cardioRatio { get; set; }

        private void CalculateWorkAndRestLengths(Plan plan)
        {
            if (plan.Activity == Activity.Cardio)
            {

                switch (plan.FitnessLevel)
                {
                    case FitnessLevel.Beginner:
                        if (plan.WeightGoal == WeightGoal.LooseFat)
                            plan.User.SetTime = new TimeSpan(00, 00, 60);
                        plan.User.CardioRatio = 1.2;
                        if (plan.WeightGoal == WeightGoal.ToneUp)
                            plan.User.SetTime = new TimeSpan(00, 00, 60);
                        plan.User.CardioRatio = 1.2;
                        break;
                    case FitnessLevel.Intermediate:
                        if (plan.WeightGoal == WeightGoal.LooseFat)
                            plan.User.SetTime = new TimeSpan(00, 00, 60);
                        plan.User.CardioRatio = 1.1;
                        if (plan.WeightGoal == WeightGoal.ToneUp)
                            plan.User.SetTime = new TimeSpan(00, 00, 60);
                        plan.User.CardioRatio = 1.1;
                        break;

                    case FitnessLevel.Advanced:
                        if (plan.WeightGoal == WeightGoal.LooseFat)
                            plan.User.SetTime = new TimeSpan(00, 00, 50);
                        plan.User.CardioRatio = 2.1;
                        if (plan.WeightGoal == WeightGoal.ToneUp)
                            plan.User.SetTime = new TimeSpan(00, 00, 50);
                        plan.User.CardioRatio = 2.1;
                        break;
                }
            }
            else
            { 
                switch (plan.FitnessLevel)
                {
                    case FitnessLevel.Beginner:
                        if (plan.WeightGoal == WeightGoal.LooseFat)
                        {
                            plan.User.SetTime = new TimeSpan(00, 00, 90);
                            plan.User.RestTime = new TimeSpan(00, 00,60);
                        }
                        if (plan.WeightGoal == WeightGoal.ToneUp)
                        { 
                            plan.User.SetTime = new TimeSpan(00, 00, 126);
                            plan.User.RestTime = new TimeSpan(00, 00, 90);
                        }
                        if (plan.WeightGoal == WeightGoal.GainMuscle)
                        {
                            plan.User.SetTime = new TimeSpan(00, 00, 126);
                            plan.User.RestTime = new TimeSpan(00, 00, 90);
                        }
                        break;

                    case FitnessLevel.Intermediate:
                        if (plan.WeightGoal == WeightGoal.LooseFat)
                        {
                            plan.User.SetTime = new TimeSpan(00, 00, 100);
                            plan.User.RestTime = new TimeSpan(00, 00, 60);
                        }
                        if (plan.WeightGoal == WeightGoal.ToneUp)
                        {
                            plan.User.SetTime = new TimeSpan(00, 00, 126);
                            plan.User.RestTime = new TimeSpan(00, 00, 90);
                        }
                        if (plan.WeightGoal == WeightGoal.GainMuscle)
                        {
                            plan.User.SetTime = new TimeSpan(00, 00, 126);
                            plan.User.RestTime = new TimeSpan(00, 00, 90);
                        }
                        break;

                    case FitnessLevel.Advanced:
                        if (plan.WeightGoal == WeightGoal.LooseFat)
                        {
                            plan.User.SetTime = new TimeSpan(00, 00, 90);
                            plan.User.RestTime = new TimeSpan(00, 00, 45);
                        }
                        if (plan.WeightGoal == WeightGoal.ToneUp)
                        {
                            plan.User.SetTime = new TimeSpan(00, 00, 110);
                            plan.User.RestTime = new TimeSpan(00, 00, 60);
                        }
                       if (plan.WeightGoal == WeightGoal.GainMuscle)
                        {
                            plan.User.SetTime = new TimeSpan(00, 00, 126);
                            plan.User.RestTime = new TimeSpan(00, 00, 90);
                        }
                        break;
                }
            }
        }

        public void CalculateSets(Plan plan, Session selectedSession = null, bool oneSessionOnly = false)
        {
            CalculateWorkAndRestLengths(plan);

            if (plan.Activity == Activity.Weightlifting || plan.Activity == Activity.BodyweightTraining)
            {
                CalculateNoOfSessionSetsWLAndBWOnly(plan, selectedSession, oneSessionOnly);
            }
            else
            {
                CreateSessionCircuits(plan);
            }
        }

        private void CalculateNoOfSessionSetsWLAndBWOnly(Plan plan, Session selectedSession = null, bool oneSessionOnly = false)
        {
            if (selectedSession != null && oneSessionOnly)
            {
                var sessionSeconds = selectedSession.AvailableTime.TotalSeconds;

                var maxNoOfNormalSetsInSession = sessionSeconds / plan.User.SetTime.TotalSeconds;
                var maxNoOfSuperSetInSession = sessionSeconds / (plan.User.SetTime.TotalSeconds * 1.25);
                var actualMaxNoOfNormalSetsInSession = maxNoOfNormalSetsInSession * 0.45;
                //Set
                selectedSession.NoOfNormalSets = Convert.ToInt32(actualMaxNoOfNormalSetsInSession);
                var actualMaxNoOfSuperSetsInSession = maxNoOfSuperSetInSession * 0.55;
                //Set
                selectedSession.NoOfSuperSets = Convert.ToInt32(actualMaxNoOfSuperSetsInSession);
                var actualAmountOfSetsInSession =
                    actualMaxNoOfSuperSetsInSession + actualMaxNoOfNormalSetsInSession;

                CalculateNoOfSectionSetsWLAndBWOnly(plan, selectedSession);
                
                return;
            }

            //This method works out the no of session sets and calls below method which works out section sets
            foreach (var session in plan.Sessions)
            {
                if (session == null)
                    throw new NullReferenceException();


                var sessionSeconds = session.AvailableTime.TotalSeconds;

                var maxNoOfNormalSetsInSession = sessionSeconds / plan.User.SetTime.TotalSeconds;
                    var maxNoOfSuperSetInSession = sessionSeconds / (plan.User.SetTime.TotalSeconds * 1.25);
                    var actualMaxNoOfNormalSetsInSession = maxNoOfNormalSetsInSession * 0.45;
                    //Set
                    session.NoOfNormalSets = Convert.ToInt32(actualMaxNoOfNormalSetsInSession);
                    var actualMaxNoOfSuperSetsInSession = maxNoOfSuperSetInSession * 0.55;
                    //Set
                    session.NoOfSuperSets = Convert.ToInt32(actualMaxNoOfSuperSetsInSession);
                    var actualAmountOfSetsInSession =
                        actualMaxNoOfSuperSetsInSession + actualMaxNoOfNormalSetsInSession;
            }
                            foreach (var session in plan.Sessions)
                            {
                                CalculateNoOfSectionSetsWLAndBWOnly(plan, session);
                            }
        }
             
                
        
        private void CalculateNoOfSectionSetsWLAndBWOnly(Plan plan, Session session)
        {

            if (session == null)
                    throw new NullReferenceException();

            foreach (var section in session.Sections)
            {
                
                if (session.AvailableTime < section.AvailableTime)
                    throw new InvalidOperationException();
                {
                    var sectionSeconds = section.AvailableTime.TotalSeconds;
                    var fractionOfSession = sectionSeconds / (session.AvailableTime.TotalSeconds);

                    //Must check if this code using "!" and "any" work as intended. 
                    if (!session.Sections.Any(i => i.IsSuperSetted))
                    {
                        //workout no of normal sets in section
                        var noOfSetsInSectionNormal = fractionOfSession * session.NoOfNormalSets;
                        //Set
                        section.NoOfNormalSets = Convert.ToInt32(noOfSetsInSectionNormal);

                        //workout no of super sets in section
                        var noOfSetsInSectionSuper = fractionOfSession * session.NoOfSuperSets;
                        //Set
                        section.NoOfSuperSets = Convert.ToInt32(noOfSetsInSectionSuper);
                    }
                    else
                    {
                        //explanation.. if its an SS session we use that method to do all the work, that means that below the method call even if its not a SS
                        //section but an SS section occurs in the session then we will ONLY add normal sets as that below method takes care of adding the SS's for all sections.

                        if (section.IsSuperSetted)
                        {
                            CalculateNoOfSectionSetsWLAndBWOnlyForContainingSuperSetAllSection(plan,session, section);
                        }
                        else
                        {
                            //workout no of normal sets in section
                            var noOfSetsInSectionNormal = fractionOfSession * session.NoOfNormalSets;
                            //Set
                            section.NoOfNormalSets = Convert.ToInt32(noOfSetsInSectionNormal);
                        }
                        
                    }
                }
            }
        }

       private void CalculateNoOfSectionSetsWLAndBWOnlyForContainingSuperSetAllSection(Plan plan, Session session, Section section)
        {

            var remainingAvailableTime = section.AvailableTime;
            TimeSpan timeForSuperSet = TimeSpan.FromSeconds(plan.User.SetTime.TotalSeconds * 1.25);
            //check if we have enough time in section for another set
            while (remainingAvailableTime >= timeForSuperSet)
            {   //why are we checking this
                //
                //if (session.NoOfSuperSets > section.NoOfSuperSets)
                //{
                    section.NoOfSuperSets++;
                    remainingAvailableTime -= timeForSuperSet;

                    //if (remainingAvailableTime < timeForSuperSet && session.NoOfSuperSets > section.NoOfSuperSets) //im checking to see if we have enough sets to fill the SS section
                    //{
                    //    var remainingSets = session.NoOfSuperSets - section.NoOfSuperSets;
                    //    session.Sections.FirstOrDefault(i => i != section).NoOfSuperSets += remainingSets;  //add remaining SS to other section
                    //}
                //}
                //else
                //{
                    //section.NoOfSuperSets++;
                    //remainingAvailableTime -= timeForSuperSet;

                    //if (remainingAvailableTime < timeForSuperSet && session.NoOfSuperSets > section.NoOfSuperSets) //im checking to see if we have enough sets to fill the SS section
                    //{
                    //    var remainingSets = session.NoOfSuperSets - section.NoOfSuperSets;
                    //    session.Sections.FirstOrDefault(i => i != section).NoOfSuperSets += remainingSets;  //add remaining SS to other section
                    //}
                    //if (!session.IsOverPrescribed)
                    //{
                    //    session.IsOverPrescribed = true;
                    //}


                    //var shortBy = Convert.ToInt32(remainingAvailableTime.TotalSeconds / timeForSuperSet.TotalSeconds); //if we are short of SS's in session and need more to fill the section then we add how many we need but we will not give any SS to any other sections else session to long.
                    //    session.NoOfSuperSets += shortBy;
                    //    section.NoOfSuperSets += shortBy;
                    //  remainingAvailableTime -= timeForSuperSet;

                //}
            }
        }

        private void CreateSessionCircuits(Plan plan)
        {
            foreach (var session in plan.Sessions)
            {

                if (session == null)
                    throw new NullReferenceException();

                var sessionSeconds = session.AvailableTime.TotalSeconds;
                var noOfSetsInSession = sessionSeconds / plan.User.SetTime.TotalSeconds;
                var noOfRounds = Convert.ToInt32(noOfSetsInSession / 4);

                if (noOfRounds <= 5)
                {
                    session.NoOfRounds = noOfRounds;
                    session.ExPerRound = 4;
                }
                if (noOfRounds == 6)
                {
                    noOfRounds = Convert.ToInt32(noOfSetsInSession / 5);

                    if (noOfRounds <= 6)
                    {
                        session.NoOfRounds = noOfRounds;
                        session.ExPerRound = 5;
                    }
                    else
                    {   //if more than 6 rounds
                        session.ExPerRound = 6;
                        session.NoOfRounds = 6;
                    }
                }
                if (noOfRounds > 6)
                {
                    session.ExPerRound = 6;
                    session.NoOfRounds = 6;
                }
            }
        }


    }
}