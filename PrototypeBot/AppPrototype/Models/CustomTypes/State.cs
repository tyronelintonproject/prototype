﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppPrototype.Models.CustomTypes
{
    [Serializable]
    public class State
    {
        public Session NextSession { get; set; }
        public Session PreviousSession { get; set; }

        public State()
        {
            NextSession = new Session();
            PreviousSession = new Session();
        }

    }
}
