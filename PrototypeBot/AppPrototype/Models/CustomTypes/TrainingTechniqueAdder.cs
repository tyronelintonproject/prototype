﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppPrototype.Models.CustomTypes;
using AppPrototype.Models.Enums;

namespace AppPrototype.Models
{
    public class TrainingTechniqueAdder
    {
        public void AddTrainingTechniques(Plan plan)
        {
            if (plan.Activity == Activity.Cardio || plan.FitnessLevel == FitnessLevel.Beginner)
            {
                return;
            }
            else
            {
                if (plan.Activity == Activity.BodyweightTraining)
                {
                    //Will only set the drop set if an easier exercise exists otherwise it will end up being too much like a super set, so if doing normal push ups
                    //the easier exercise is kneeling, or if doing jump sqauts or single leg the easier exercise is normal sqauts, but we want it to be a immediate transition
                    //and not a completely different exercise because user is already doing enough super sets.

                        if (plan.FitnessLevel == FitnessLevel.Intermediate && plan.WeightGoal == WeightGoal.GainMuscle || plan.FitnessLevel == FitnessLevel.Advanced)
                        {
                            foreach (var session in plan.Sessions)
                            {
                                foreach (var section in session.Sections)
                                {
                                    foreach (var exercise in section.Exercises.Where(ex => ex.IsSuperSetted == false && ex.Activities.Contains(Activity.BodyweightTraining)))
                                    {
                                    exercise.IsDropSetted = true;

                                    new ExerciseAdder().SetDropSetExerciseForBodyweightTraining(exercise);
                                    }
                                }
                            }
                        }
                }

                if (plan.Activity == Activity.Weightlifting)
                {
                    if (plan.FitnessLevel == FitnessLevel.Intermediate && plan.WeightGoal == WeightGoal.GainMuscle || plan.FitnessLevel == FitnessLevel.Advanced)
                    {
                        foreach (var session in plan.Sessions)
                        {
                            foreach (var section in session.Sections)
                            {
                                foreach (var exercise in section.Exercises.Where(ex=>ex.IsSuperSetted == false && ex.Activities.Contains(Activity.Weightlifting)))
                                {
                                    exercise.IsDropSetted = true;
                                }
                            }
                        }
                        if (plan.FitnessLevel == FitnessLevel.Advanced)
                        {
                            foreach (var session in plan.Sessions)
                            {
                                foreach (var section in session.Sections)
                                {
                                    var selectedExercise = section.Exercises.FirstOrDefault(ex => ex.IsDropSetted && ex.Activities.Contains(Activity.Weightlifting));

                                    if (selectedExercise != null)
                                    {
                                        selectedExercise.IsDropSetted = false;
                                        selectedExercise.IsTripleDropSetted = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

    }
}
