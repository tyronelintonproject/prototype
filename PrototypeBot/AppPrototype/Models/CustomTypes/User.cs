﻿using System;
using System.Collections.Generic;
using System.Text;
using AppPrototype.Models.Enums;
using AppPrototype.Models.CustomTypes.Dictionaries;
using AppPrototype.Models.CustomTypes.MealLogic;

namespace AppPrototype.Models.CustomTypes
{
    [Serializable]
    public class User
    {
        public string UserName { get; set; }
        public int Age { get; set; }
        public Gender Gender { get; set; }
        public TimeSpan SetTime { get; set; }
        public TimeSpan RestTime { get; set; }
        public double CardioRatio { get; set; }
        public double Height { get; set; }
        public double Weight { get; set; }
        public double UserActivityLevel { get; set; }
        public double Calories { get; set; }
        public NutritionPlan NutritionPlan { get; set; }
        public int StrengthPercentageIncrease { get; set; }
        public int EndurancePercentageIncrease { get; set; }
        public IncreaseContainer IncreaseContainer { get; set; }

        public User()
        {
            SetTime = new TimeSpan();
            RestTime = new TimeSpan();
            NutritionPlan = new NutritionPlan();
            IncreaseContainer = new IncreaseContainer();
        }

    }
}
