﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppPrototype.Models.CustomTypes
{
    public class WeightConverter
    {
        public double ConvertPoundsToKg(double Pounds)
        {
            return Pounds * 0.453592;
        }

    }
}
