﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppPrototype.Models.Enums
{
    [Serializable]
    public enum Activity
    {
        Weightlifting,
        BodyweightTraining,
        Cardio
    }
}
