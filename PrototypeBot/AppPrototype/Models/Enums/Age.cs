﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppPrototype.Models.Enums
{
    [Serializable]
    public enum Age
    {
            Under25,
            TwentyFiveToThirty,
            ThirtyToForty,
            OverForty
    }
}
