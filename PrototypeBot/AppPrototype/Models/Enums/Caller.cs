﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppPrototype.Models.Enums
{
    [Serializable]
    public enum Caller
    {
        None,
        IWantBigger,
        IWantToLoose

    }
}
