﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppPrototype.Models.Enums
{
    [Serializable]
    public enum EquipmentType
    {//Remember i can add terms here!
        Bodyweight,
        Dumbell,
        Barbell,
        KettelBell,
        Machine
    }
}
