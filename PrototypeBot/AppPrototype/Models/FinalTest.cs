﻿using Bot_Application1;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppPrototype.Models
{
    public class FinalTest
    {
        public MyTask MyTest { get; set; }

        public FinalTest()
        {
            MyTest = new MyTask();
        }

        public void Work()
        {
            MyTest.DoWork(3, 2);
            MyTest = null;
        }

    }
}
