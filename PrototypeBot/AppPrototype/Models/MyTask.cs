﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bot_Application1
{
    public class MyTask
    {
        public MyHelperTask MyHelperTask { get; set; }

        public MyTask()
        {
            MyHelperTask = new MyHelperTask();
        }

        public void DoWork(int a, int b)
        {
            var c = a + b;
            MyHelperTask.DoMoreWork(c);
        }

    }

    public class MyHelperTask
    {
        public void DoMoreWork(int a)
        {
            var c = a * 10;
        }

    }

}