﻿using System;
using System.Collections.Generic;
using System.Linq;
using AppPrototype.Models.Enums;

namespace AppPrototype.Models.CustomTypes
{
    [Serializable]
    public class NewPlan
    {
        public string RouteId { get; set; }
        public int Id { get; set; }
        public User User { get; set; }
        public TrainerType TrainerType { get; set; }
        public WeightGoal WeightGoal { get; set; }
        public FitnessLevel FitnessLevel { get; set; }
        public GymOrHome GymOrHome { get; set; }
        public Activity Activity { get; set; }
        public TimeSpan SessionLength { get; set; }
        public int DaysPerWeek { get; set; }
        public List<Session> Sessions { get; set; }
        public List<Goal> Goals { get; set; }
        public List<Equipment> EquipmentList { get; set; }
        public List<MuscleGroup> InjuryList { get; set; }
        public MuscleGroup InjuryPreferedReplacement { get; set; }
        public DateTime CreationDate { get; set; }
        public bool CycleOn { get; set; }
        public GoalLevelContainer GoalLevelContainer { get; set; }

        public int NoneRepExerciseRange
        {
            get
            {
                switch (WeightGoal)
                {
                    case WeightGoal.GainMuscle:
                        {
                            if (FitnessLevel == FitnessLevel.Advanced)
                                return 30;
                            return 20;
                        }
                    case WeightGoal.ToneUp:
                        {
                            if (FitnessLevel == FitnessLevel.Advanced)
                                return 40;
                            return 30;
                        }
                    case WeightGoal.LooseFat:
                        {
                            if (FitnessLevel == FitnessLevel.Advanced)
                                return 40;
                            return 30;
                        }
                }
                return 30;
            }

        }
        public int WeightliftingRepRangeMin
        {
            get
            {
                switch (WeightGoal)
                {
                    case WeightGoal.GainMuscle:
                        {
                            return 6;
                        }
                    case WeightGoal.ToneUp:
                        {
                            return 8;
                        }
                    case WeightGoal.LooseFat:
                        {
                            return 10;
                        }
                }
                return 8;
            }
        }
        public int WeightliftingRepRangeMax
        {
            get
            {
                switch (WeightGoal)
                {
                    case WeightGoal.GainMuscle:
                        {
                            return 8;
                        }
                    case WeightGoal.ToneUp:
                        {
                            return 10;
                        }
                    case WeightGoal.LooseFat:
                        {
                            return 12;
                        }
                }
                return 8;
            }
        }

        public int BodyweightTrainingRepRangeMin
        {
            get
            {
                switch (WeightGoal)
                {
                    case WeightGoal.GainMuscle:
                        {
                            return 10;
                        }
                    case WeightGoal.ToneUp:
                        {
                            return 12;
                        }
                    case WeightGoal.LooseFat:
                        {
                            return 12;
                        }
                }
                return 10;
            }
        }

        public int BodyweightTrainingRepRangeMax
        {
            get
            {
                switch (WeightGoal)
                {
                    case WeightGoal.GainMuscle:
                        {
                            return 12;
                        }
                    case WeightGoal.ToneUp:
                        {
                            return 15;
                        }
                    case WeightGoal.LooseFat:
                        {
                            return 15;
                        }
                }
                return 15;
            }
        }

        public State State { get; set; }

        public NewPlan()
        {

            InjuryList = new List<MuscleGroup>();
            Sessions = new List<Session>();
            Goals = new List<Goal>();
            GoalLevelContainer = new GoalLevelContainer();
            // InjuryPreferedReplacement = new MuscleGroup();
            EquipmentList = new List<Equipment>();
            CreationDate = DateTime.Now;
            User = new User();
            State = new State();
        }



    }
}
