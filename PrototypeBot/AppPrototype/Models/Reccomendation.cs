﻿using AppPrototype.Models.CustomTypes;
using AppPrototype.Models.CustomTypes.Food;
using AppPrototype.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppPrototype.Models
{
    public class Reccomendation
    {
        public string OpeningMessage { get; set; }
        public string ReccomendedActivity { get; set; }
        public string ReccomendedExercises { get; set; }
        public string ExpertNote { get; set; }
        public string ReccommendedFood { get; set; }

        public override string ToString()
        {
            return ReccomendedActivity + "\r\n\r\n"
                 + ReccomendedExercises + "\r\n\r\n"
                 + ReccommendedFood + "\r\n\r\n"
                 + ExpertNote + "\r\n\r\n";
        }

        //once weve sent this ask them do you want to see how to do the exercises?

    }
}


