﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppPrototype.CustomTypes;
using AppPrototype.Models.CustomTypes;
using AppPrototype.Models.Enums;

namespace AppPrototype.Models.RouteLogic
{
    class LogicWeightliftingPPLGainMuscle
    {
        public Route Route = new Route("TEST");


        /////////////////////////////////////////////////////////30 MINUTES///////////////////////////////////////////////////////////////////////////////////////////


        public void Min30Day4Male(Plan plan)
        {
            //Set plan CycleOn
            plan.CycleOn = true;

            plan.Sessions = new List<Session>()
            {
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Push", Version = 1,
                    Sections =

                        new List<Section>()
                        {
                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)},

                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Shoulders"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps")
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0), IsSuperSetted = true
                            }
                        }
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Pull", Version = 1,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Back")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)
                            },

                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Biceps"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Abs")
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0), IsSuperSetted = true
                            }
                        }
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Legs", Version = 1,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Qauds")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)
                            },

                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Hamms"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Calves")
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0), IsSuperSetted = true
                            }
                        },
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "FullBody", Version = 1,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32(plan.SessionLength.Minutes * 0.2),0)
                            },
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Shoulders")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32(plan.SessionLength.Minutes * 0.2),0)
                            },
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Back")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32(plan.SessionLength.Minutes * 0.2),0)
                            },
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Qauds")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32(plan.SessionLength.Minutes * 0.2),0)
                            },
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Hamms")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32(plan.SessionLength.Minutes * 0.2),0)
                            }
                        },
                },


                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Push", Version = 2,
                    Sections =

                        new List<Section>()
                        {
                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Shoulders")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)},

                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps")
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0), IsSuperSetted = true
                            }
                        }
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Pull", Version = 2,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Biceps")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32((plan.SessionLength.Minutes / 3) * 1.5),0)
                            },

                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Back"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Abs")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32((plan.SessionLength.Minutes / 3) * 1.5),0), IsSuperSetted = true
                            }
                        }
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Legs", Version = 2,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Hamms")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)
                            },

                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Qauds"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Calves")
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0), IsSuperSetted = true
                            }
                        },
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "FullBody", Version = 2,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32(plan.SessionLength.Minutes * 0.2),0)
                            },
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Shoulders")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32(plan.SessionLength.Minutes * 0.2),0)
                            },
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Back")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32(plan.SessionLength.Minutes * 0.2),0)
                            },
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Qauds")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32(plan.SessionLength.Minutes * 0.2),0)
                            },
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Calves")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32(plan.SessionLength.Minutes * 0.2),0)
                            }
                        }
                }
            };
        }
        public void Min30Day5Male(Plan plan)
        {
            //Set plan CycleOff
            plan.CycleOn = false;

            plan.Sessions = new List<Session>()
            {
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Push", Version = 1,
                    Sections =

                        new List<Section>()
                        {
                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)},

                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Shoulders"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps")
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0), IsSuperSetted = true
                            }
                        }
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Pull", Version = 1,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Back")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)
                            },

                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Biceps"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Abs")
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0), IsSuperSetted = true
                            }
                        }
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Legs", Version = 1,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Qauds")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)
                            },

                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Hamms"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Calves")
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0), IsSuperSetted = true
                            }
                        },
                },

                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Push", Version = 2,
                    Sections =

                        new List<Section>()
                        {
                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Shoulders")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)},

                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps")
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0), IsSuperSetted = true
                            }
                        }
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Pull", Version = 2,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Biceps")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32((plan.SessionLength.Minutes / 3) * 1.5),0)
                            },

                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Back"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Abs")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32((plan.SessionLength.Minutes / 3) * 1.5),0), IsSuperSetted = true
                            }
                        }
                },


            };
        }

        public void Min30Day6Male(Plan plan)
        {
            //Set plan CycleOn
            plan.CycleOn = true;

            plan.Sessions = new List<Session>()
            {
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Push", Version = 1,
                    Sections =

                        new List<Section>()
                        {
                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)},

                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Shoulders"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps")
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0), IsSuperSetted = true
                            }
                        }
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Pull", Version = 1,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Back")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)
                            },

                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Biceps"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Abs")
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0), IsSuperSetted = true
                            }
                        }
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Legs", Version = 1,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Qauds")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)
                            },

                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Hamms"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Calves")
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0), IsSuperSetted = true
                            }
                        },
                },

                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Push", Version = 2,
                    Sections =

                        new List<Section>()
                        {
                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Shoulders")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)},

                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Triceps")
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0), IsSuperSetted = true
                            }
                        }
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Pull", Version = 2,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Biceps")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32((plan.SessionLength.Minutes / 3) * 1.5),0)
                            },

                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Back"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Abs")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32((plan.SessionLength.Minutes / 3) * 1.5),0), IsSuperSetted = true
                            }
                        }
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Legs", Version = 2,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Hamms")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)
                            },

                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Qauds"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Calves")
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0), IsSuperSetted = true
                            }
                        },
                }

            };
        }

        public void Min30Day4Female(Plan plan)
        {
            //Set plan CycleOn
            plan.CycleOn = true;

            plan.Sessions = new List<Session>()
            {
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Push", Version = 1,
                    Sections =

                        new List<Section>()
                        {
                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Qauds")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)},

                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Calves"),
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0)
                            }
                        }
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Pull", Version = 1,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Hamms")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)
                            },

                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Glutes"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Abs")
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0), IsSuperSetted = true
                            }
                        }
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Upper", Version = 1,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Shoulders")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)
                            },

                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Back"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest")
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0), IsSuperSetted = true
                            }
                        },
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "FullBody", Version = 1,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Abs")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32(plan.SessionLength.Minutes * 0.2),0)
                            },
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Shoulders")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32(plan.SessionLength.Minutes * 0.2),0)
                            },
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Back")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32(plan.SessionLength.Minutes * 0.2),0)
                            },
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Qauds")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32(plan.SessionLength.Minutes * 0.2),0)
                            },
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Hamms")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32(plan.SessionLength.Minutes * 0.2),0)
                            }
                        },
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Push", Version = 2,
                    Sections =

                        new List<Section>()
                        {
                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Calves")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)},

                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Qauds"),
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0)
                            }
                        }
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Pull", Version = 2,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Glutes")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32((plan.SessionLength.Minutes / 3) * 1.5),0)
                            },

                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Hamms"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Abs")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32((plan.SessionLength.Minutes / 3) * 1.5),0), IsSuperSetted = true
                            }
                        }
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Upper", Version = 2,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Back")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)
                            },

                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Chest"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Shoulders")
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0), IsSuperSetted = true
                            }
                        },
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "FullBody", Version = 2,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Abs")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32(plan.SessionLength.Minutes * 0.2),0)
                            },
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Shoulders")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32(plan.SessionLength.Minutes * 0.2),0)
                            },
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Back")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32(plan.SessionLength.Minutes * 0.2),0)
                            },
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Qauds")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32(plan.SessionLength.Minutes * 0.2),0)
                            },
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Calves")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32(plan.SessionLength.Minutes * 0.2),0)
                            }
                        }
                }
            };
        }

        public void Min30Day5Female(Plan plan)
        {
            //Set plan CycleOff
            plan.CycleOn = false;

            plan.Sessions = new List<Session>()
            {
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Push", Version = 1,
                    Sections =

                        new List<Section>()
                        {
                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Qauds")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)},

                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Calves"),
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0)
                            }
                        }
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Pull", Version = 1,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Hamms")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)
                            },

                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Glutes"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Abs")
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0), IsSuperSetted = true
                            }
                        }
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Upper", Version = 1,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Shoulders")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)
                            },

                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Back"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Abs")
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0), IsSuperSetted = true
                            }
                        },
                },

                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Push", Version = 2,
                    Sections =

                        new List<Section>()
                        {
                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Calves")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)},

                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Qauds"),
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0)
                            }
                        }
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Pull", Version = 2,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Glutes")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32((plan.SessionLength.Minutes / 3) * 1.5),0)
                            },

                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Hamms"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Abs")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32((plan.SessionLength.Minutes / 3) * 1.5),0), IsSuperSetted = true
                            }
                        }
                }



            };
        }

        public void Min30Day6Female(Plan plan)
        {
            //Set plan CycleOff
            plan.CycleOn = false;

            plan.Sessions = new List<Session>()
            {
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Push", Version = 1,
                    Sections =

                        new List<Section>()
                        {
                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Qauds")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)},

                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Calves"),
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0)
                            }
                        }
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Pull", Version = 1,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Hamms")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)
                            },

                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Glutes"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Abs")
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0), IsSuperSetted = true
                            }
                        }
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Upper", Version = 1,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Shoulders")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)
                            },

                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Back"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Abs")
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0), IsSuperSetted = true
                            }
                        },
                },

                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Push", Version = 2,
                    Sections =

                        new List<Section>()
                        {
                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Calves")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)},

                            new Section() {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Qauds"),
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0)
                            }
                        }
                },
                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Pull", Version = 2,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Glutes")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32((plan.SessionLength.Minutes / 3) * 1.5),0)
                            },

                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Hamms"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Abs")
                                },
                                AvailableTime = new TimeSpan(0,Convert.ToInt32((plan.SessionLength.Minutes / 3) * 1.5),0), IsSuperSetted = true
                            }
                        }
                },

                new Session()
                {
                    ActivityType = Activity.Weightlifting, AvailableTime = plan.SessionLength, PlanId = plan.Id, Name = "Upper", Version = 2,
                    Sections =
                        new List<Section>()
                        {
                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Abs")
                                },
                                AvailableTime = new TimeSpan(0,(plan.SessionLength.Minutes / 3) * 2,0)
                            },

                            new Section()
                            {
                                MuscleGroups = new List<MuscleGroup>()
                                {
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Back"),
                                    Route.MuscleGroups.SingleOrDefault(i=>i.Name == "Shoulders")
                                },
                                AvailableTime = new TimeSpan(0,plan.SessionLength.Minutes - (plan.SessionLength.Minutes / 3) * 2,0), IsSuperSetted = true
                            }
                        }
                },



            };
        }

    /////////////////////////////////////////////////////////END OF 30 MINUTES///////////////////////////////////////////////////////////////////////////////////////////
    }
}
