﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppPrototype.CustomTypes;
using AppPrototype.Models.CustomTypes;
using AppPrototype.Models.Enums;
using AppPrototype.Models.RouteLogic;

namespace AppPrototype.Models.Routes
{
    public class WeightliftingPPLGainMuscle : Route
    {
        private const string IdForThisRoute = "WeightliftingPPLGainMuscle";
        private LogicWeightliftingPPLGainMuscle _logic;
        
        public WeightliftingPPLGainMuscle(string IdForThisRoute)
            : base(IdForThisRoute)
        {
            _logic = new LogicWeightliftingPPLGainMuscle();
        }

        public override Plan Execute(Plan plan)
        {
            
            switch (plan.SessionLength.Minutes)
            {
                //Create a new list of session based on days 
                //Add sections to the sessions
                //Give each session a name based on sections
                //Update the plan

                case 15:
                    if (plan.DaysPerWeek == 4 && plan.User.Gender == Gender.M)
                    {
                        
                    }
                     break;


                   case 30:
                       if (plan.DaysPerWeek == 4 && plan.User.Gender == Gender.M)
                       {
                         _logic.Min30Day4Male(plan);
                       }
                       if (plan.DaysPerWeek == 4 && plan.User.Gender == Gender.F)
                       {
                           _logic.Min30Day4Female(plan);
                       }
                       if (plan.DaysPerWeek == 5 && plan.User.Gender == Gender.M)
                       {
                           _logic.Min30Day5Male(plan);
                       }
                       if (plan.DaysPerWeek == 5 && plan.User.Gender == Gender.F)
                       {
                           _logic.Min30Day5Female(plan);
                       }
                       if (plan.DaysPerWeek == 6 && plan.User.Gender == Gender.M)
                       {
                           _logic.Min30Day6Male(plan);
                       }
                       if (plan.DaysPerWeek == 6 && plan.User.Gender == Gender.F)
                       {
                           _logic.Min30Day6Female(plan);
                       }

                    break;


                        case 45:

                             break;


                            case 60:

                                break;

            }

            new InjuryHandler().Check(plan);

            return base.Execute(plan);
        }


        




    }
}
