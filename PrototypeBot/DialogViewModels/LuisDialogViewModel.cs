﻿using AppPrototype.CustomTypes;
using AppPrototype.Models.CustomTypes;
using AppPrototype.Models.Enums;
using PrototypeBot.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrototypeBot.DialogViewModels
{
    [Serializable]
    public class LuisDialogViewModel
    {
        private Route _route = new Route("");

        /////////Used for IveGotAnInjury
        public MuscleGroup InjuredMuscleGroup { get; set; }
       // public Joint InjuredJoint { get; set; }
        /////////Used for IveGotAnInjury

        public FitnessLevel SetFitnessLevel(string fitness)
        {
            //Set the fitness level
            switch (fitness.ToLower())
            {
                case "fit":
                    {
                        return FitnessLevel.Advanced;
                    }
                case "average":
                    {
                        return FitnessLevel.Intermediate;
                    }
                case "unfit":
                    {
                        return FitnessLevel.Beginner;
                    }
            }

            return FitnessLevel.Intermediate;
        }

        public WeightGoal SetWeightGoal(string weightGoal)
        {
            //Set the fitness level
            switch (weightGoal.ToLower())
            {
                case "loosefat":
                    {
                        return WeightGoal.LooseFat;
                    }
                case "toneup":
                    {
                        return WeightGoal.ToneUp;
                    }
                case "gainmuscle":
                    {
                        return WeightGoal.GainMuscle;
                    }
            }

            if (weightGoal.Contains("loose") || weightGoal.Contains("trim") || weightGoal.Contains("rid") || weightGoal.Contains("drop") ||
                weightGoal.Contains("lose") || weightGoal.Contains("losing fat") || weightGoal.Contains("losing weight"))
            { return WeightGoal.LooseFat; }

            else if (weightGoal.Contains("tone") || weightGoal.Contains("tighten") || weightGoal.Contains("shred"))
            { return WeightGoal.ToneUp; }

            else if (weightGoal.Contains("gain") || weightGoal.Contains("big") || weightGoal.Contains("muscle") || weightGoal.Contains("strong")
                || weightGoal.Contains("large") || weightGoal.Contains("strength") || weightGoal.Contains("power") || weightGoal.Contains("muscular") || weightGoal.Contains("thick")
                || weightGoal.Contains("wide") || weightGoal.Contains("gaining muscle") || weightGoal.Contains("getting bigger")) 
            { return WeightGoal.GainMuscle; } 

            else return WeightGoal.None;
        }



        public Gender SetGender(string gender)
        {
            //Set the fitness level
            switch (gender.ToLower())
            {
                case "male":
                    {
                        return AppPrototype.Models.Enums.Gender.M;
                    }
                case "female":
                    {
                        return AppPrototype.Models.Enums.Gender.F;
                    }
            }
            return Gender.M;
        }

        public GymOrHome SetGymOrHome(string gymOrHome)
        {
            //Set the fitness level
            switch (gymOrHome.ToLower())
            {
                case "gym":
                    {
                        return AppPrototype.Models.Enums.GymOrHome.Gym;
                    }
                case "home":
                    {
                        return AppPrototype.Models.Enums.GymOrHome.Home;
                    }
            }
                return AppPrototype.Models.Enums.GymOrHome.Gym;
        }

        public MuscleGroup GetMuscleGroup(string muscleGroup)
        {   //  I NEED TO GET THE OTHER VERSION OF APP PROTOTYPE ON MY OLD LAPPY THAT HAS THE SYNONYMS 
            //  return _route.MuscleGroups.FirstOrDefault(i => i.Synonyms.Any(name => name.ToLower().Trim() == muscleGroup.ToLower().Trim()));
            return new MuscleGroup();
        }

        //UNCOMMENT THIS METHOD
        //public Joint GetJoint(string joint)
        //{
        //    var joints = new Route("").Joints;
        //    return _route.Joints.FirstOrDefault(i => i.Synonyms.Any(name => name.ToLower().Trim() == joint.ToLower().Trim()));
        //}


        public double GetWeightInPounds(WeightOption weightOption, double bodyWeight)
        {
            switch (weightOption)
            {
                case WeightOption.kilos:
                    {
                        return (bodyWeight * 2.2046);
                    }
                case WeightOption.pounds:
                    {
                        return bodyWeight;
                    }
                case WeightOption.stones:
                    {
                        return (bodyWeight * 14);
                    }

            }
            return bodyWeight;
        }


        public List<Exercise> GetExericises(MuscleGroup muscleGroup, WeightGoal weightGoal, FitnessLevel fitnessLevel, GymOrHome gymOrHome)
        {
            var exFilter = new ExerciseFilterer();
            return exFilter.GetExercises(muscleGroup, weightGoal, fitnessLevel, gymOrHome);
        }

        public Exercise GetSingleExericise(string exerciseName = null, MuscleGroup mg = null, GymOrHome gymOrHome = GymOrHome.Unassigned, WeightGoal weightGoal = WeightGoal.None)
        {
            var exFilter = new ExerciseFilterer();
            return exFilter.FindSingle(exerciseName,mg,gymOrHome,weightGoal);
        }

        public double SetUserActivityLevel(int noOfDays)
        {
            if (noOfDays < 1 || noOfDays > 7)
                throw new InvalidOperationException();

            var dictionary = new AppPrototype.Models.CustomTypes.Dictionaries.UserActivityLevel();

            switch (noOfDays)
            {
                case 1: { return 1.375; } ;
                case 2: { return 1.375; } ;
                case 3: { return 1.55; } ;
                case 4: { return 1.55; } ;
                case 5: { return 1.9; } ;
                case 6: { return 1.9; } ;
                case 7: { return 1.9; } ;
            }
            return 1.55;
        }




    }
}