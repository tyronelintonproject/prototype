﻿using Microsoft.Bot.Builder.Dialogs;
using PrototypeBot.DialogViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PrototypeBot.Dialogs
{
    [Serializable]
    public class HowMuchRestDialog : IDialog<object>
    {
        public string Fitness { get; set; }
        public string Goal { get; set; }
        public string Activity { get; set; }

        public HowMuchRestDialog(string weightGoal = null)
        {
            Goal = (!string.IsNullOrEmpty(weightGoal)) ? weightGoal : "";
        }


        async Task IDialog<object>.StartAsync(IDialogContext context)
        {
            var goal = new LuisDialogViewModel().SetWeightGoal(Goal);


            if (goal != AppPrototype.Models.Enums.WeightGoal.None)
            {
                PromptDialog.Choice(
            context: context,
            resume: LevelRecievedAsync,
             options: new List<string>() { "Fit", "Average", "Unfit"},
            prompt: "What best describes you?",
            retry: "Please try again.",
            attempts: 3,
            promptStyle: PromptStyle.Auto);
                return;
            }

            PromptDialog.Choice(
            context: context,
            resume: LevelRecievedAsync,
                   options: new List<string>() { "Fit", "Average", "Unfit" },
            prompt: "What best describes you?",
            retry: "Please try again.",
            attempts: 3,
            promptStyle: PromptStyle.Auto);
        }


        private async Task LevelRecievedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            var resultString = await activity;

            Fitness = resultString;
            
            PromptDialog.Choice(
          context: context,
          resume: GoalRecievedAsync,
          options: new List<string>() { "Gain muscle", "Loose Fat" },
          prompt: "Whats your goal?",
          retry: "Please try again.",
          attempts: 3,
          promptStyle: PromptStyle.Auto);
        }

     
        private async Task GoalRecievedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            Goal = await activity;

            PromptDialog.Choice(
          context: context,
          resume: ActivityRecievedAsync,
          options: new List<string>() { "Weightlifting/Bodyweight training", "HIIT/Cardio" },
          prompt: "What activity are you doing?",
          retry: "Please try again.",
          attempts: 3,
          promptStyle: PromptStyle.Auto);
        }

        private async Task ActivityRecievedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            Activity = await activity;

            context.Done(this);
        }

        
    }
}