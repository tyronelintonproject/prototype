﻿using AppPrototype.CustomTypes;
using AppPrototype.Models.Enums;
using Microsoft.Bot.Builder.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PrototypeBot.Dialogs
{
    [Serializable]
    public class InjuredMuscleGroupDialog : IDialog<object>
    {
        public string Gender { get; set; }
        public string Fitness { get; set; }
        public string MuscleGroup { get; set; }
        public string Joint { get; set; }
        public string Diet { get; set; }
        public string GymHome { get; set; }
        public string WeightGoal { get; set; }

        public InjuredMuscleGroupDialog(string muscleGroup = null, string joint = null)
        {
            MuscleGroup = (!string.IsNullOrEmpty(muscleGroup)) ? muscleGroup : "";
            Joint = (!string.IsNullOrEmpty(joint)) ? joint : "";
        }


        async Task IDialog<object>.StartAsync(IDialogContext context)
        {
            if (!string.IsNullOrEmpty(MuscleGroup) || !string.IsNullOrEmpty(Joint))
            {
                PromptDialog.Choice(
            context: context,
            resume: GenderAndLevelRecievedSkipToDietAsync,
            options: new List<string>() { "Fit man", "Fit woman", "Average man", "Average woman", "Unfit man",
            "Unfit woman" },
            prompt: "Which best describes you?",
            retry: "Please try again.",
            attempts: 3,
            promptStyle: PromptStyle.Auto);
                return;
            }

            PromptDialog.Choice(
            context: context,
            resume: GenderAndLevelRecievedAsync,
          options: new List<string>() { "Fit man", "Fit woman", "Average man", "Average woman", "Unfit man",
            "Unfit woman" },
            prompt: "Which best describes you?",
            retry: "Please try again.",
            attempts: 3,
            promptStyle: PromptStyle.Auto);
        }


        private async Task GenderAndLevelRecievedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            var resultString = await activity;

            Fitness = resultString.Substring(0, resultString.IndexOf(" "));
            Gender = resultString.Substring(resultString.IndexOf(" "), (resultString.Length - resultString.IndexOf(" ")));

            PromptDialog.Choice(
          context: context,
          resume: MuscleGroupRecievedAsync,
          options: new List<string>() { "Shoulders", "Chest", "Back", "Triceps", "Core", "Qauds", "Hamms", "Calves",
           "Wrist", "Ankle", "Hip", "Knee", "Elbow"},
          prompt: "Whats injured (please choose one)?",
          retry: "Please try again.",
          attempts: 3,
          promptStyle: PromptStyle.Auto);
        }

        private async Task GenderAndLevelRecievedSkipToDietAsync(IDialogContext context, IAwaitable<string> activity)
        {
            var resultString = await activity;

            Fitness = resultString.Substring(0, resultString.IndexOf(" "));
            Gender = resultString.Substring(resultString.IndexOf(" "), (resultString.Length - resultString.IndexOf(" ")));

            PromptDialog.Choice(
         context: context,
         resume: DietRecievedAsync,
         options: new List<string>() { "Very good", "Average", "Not that great" },
         prompt: "How heathy do you eat?",
         retry: "Please try again.",
         attempts: 3,
         promptStyle: PromptStyle.Auto);
        }

        private async Task MuscleGroupRecievedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            var result = await activity;

           // var joints = new Route("").Joints;

            //if (joints.Any(i => i.Synonyms.Any(si => si.ToString().ToLower() == result.ToLower())))
            //{
            //    Joint = result;
            //}
            //else
            //{
            //    MuscleGroup = result;
            //}
            PromptDialog.Choice(
          context: context,
          resume: DietRecievedAsync,
          options: new List<string>() { "Very good", "Average", "Not that great" },
          prompt: "How heathy do you eat?",
          retry: "Please try again.",
          attempts: 3,
          promptStyle: PromptStyle.Auto);
        }

        private async Task DietRecievedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            Diet = await activity;

            PromptDialog.Choice(
          context: context,
          resume: GymHomeRecievedAsync,
          options: new List<string>() { "Gym", "Home" },
          prompt: "Where will you be working out?",
          retry: "Please try again.",
          attempts: 3,
          promptStyle: PromptStyle.Auto);
        }

        private async Task GymHomeRecievedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            GymHome = await activity;

            PromptDialog.Choice(
                context: context,
                resume: GoalRecievedAsync,
                options: new List<string>() { "Gain muscle", "Loose Fat" },
                prompt: "Whats your goal?",
                retry: "Please try again.",
                attempts: 3,
                promptStyle: PromptStyle.Auto);
        }

        private async Task GoalRecievedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            WeightGoal = await activity;

            context.Done(this);
        }

    }
}