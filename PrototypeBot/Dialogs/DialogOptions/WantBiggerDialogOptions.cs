﻿using Microsoft.Bot.Builder.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PrototypeBot.Dialogs.DialogOptions
{
    [Serializable]
    public class WantBiggerDialogOptions : IDialog<object>
    {
        public bool ShowExercises { get; set; }
        public bool WantExpertPlan { get; set; }
        public bool WantStandardPlan { get; set; }
        public bool AskAQuestion { get; set; }
        public string Expert { get; set; }
        public string MuscleGroup { get; set; }

        public WantBiggerDialogOptions(string expert, string muscleGroup)
        {
            Expert = expert;
            muscleGroup = muscleGroup;
        }

        public async Task StartAsync(IDialogContext context)
        {

            PromptDialog.Choice(
                context: context,
                resume: OptionRecievedAsync,
                options: new List<string>() { "Show me how to do the exercises", /*"I want " + Expert + " to tailor me a plan", "I want a standard plan",*/ "Ask a question" },
                prompt: "Anything else?",
                retry: "Connectivity issue . Please try again.",
                promptStyle: PromptStyle.Auto);
        }

        public async Task OptionRecievedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            var response = await activity;

            switch (response.ToLower().Trim())
            {
                case "ask a question":
                    {
                        AskAQuestion = true;
                        context.Done(this);
                    }
                    break;
                case "i want a standard plan":
                    {
                        WantStandardPlan = true;
                        context.Done(this);
                    }
                    break;
                case "show me how to do the exercises":
                    {
                        ShowExercises = true;
                        context.Done(this);
                    }
                    break;
                default:
                    {
                        WantExpertPlan = true;
                        context.Done(this);
                    }
                    break;
            }

        }

    }
}
