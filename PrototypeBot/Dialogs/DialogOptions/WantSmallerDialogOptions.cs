﻿    using Microsoft.Bot.Builder.Dialogs;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;

    namespace PrototypeBot.Dialogs.DialogOptions
    {
    [Serializable]
        public class WantSmallerDialogOptions : IDialog<object>
        {
            public bool ShowExercises { get; set; }

            public bool MoreOnFood { get; set; }
            public bool WantExpertPlan { get; set; }
            public bool WantStandardPlan { get; set; }
            public bool AskAQuestion { get; set; }
            public string Expert { get; set; }

            public WantSmallerDialogOptions(string expert)
            {
                Expert = expert;
            }

            public async Task StartAsync(IDialogContext context)
            {

                PromptDialog.Choice(
                    context: context,
                    resume: OptionRecievedAsync,
                    options: new List<string>() { /*"Show me how to do the exercises", "More on food", "I want " + Expert + " to tailor me a plan", "I want a standard plan",*/ "Ask a question" },
                    prompt: "Anything else?",
                    retry: "Selected plan not avilable . Please try again.",
                    promptStyle: PromptStyle.Auto);
            }

            public async Task OptionRecievedAsync(IDialogContext context, IAwaitable<string> activity)
            {
                var response = await activity;

                switch (response.ToLower().Trim())
                {
                    case "ask a question":
                        {
                            AskAQuestion = true;
                            context.Done(this);
                        }
                        break;
                    case "i want a standard plan":
                        {
                            WantStandardPlan = true;
                            context.Done(this);
                        }
                        break;
                    case "show me how to do the exercises":
                        {
                            ShowExercises = true;
                            context.Done(this);
                        }
                        break;
                    case "more on food":
                        {
                            MoreOnFood = true;
                            context.Done(this);
                        }
                        break;
                    default:
                        {
                            WantExpertPlan = true;
                            context.Done(this);
                        }
                        break;
                }

            }

        }
    }