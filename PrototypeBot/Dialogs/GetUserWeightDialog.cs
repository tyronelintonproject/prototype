﻿using AppPrototype.Models.Enums;
using Microsoft.Bot.Builder.Dialogs;
using PrototypeBot.DialogViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace PrototypeBot.Dialogs
{
    [Serializable]
    public enum WeightOption { pounds, stones, kilos };
    [Serializable]
    public class GetUserWeightDialog : IDialog<object>
    {
        public WeightOption WeightOption { get; set; }
        public double BodyweightKG { get; set; }
        public double BodyweightLbs { get; set; }
        public double BodyweightStones { get; set; }
        public int HowOftenExercise { get; set; }
        public WeightGoal WeightGoal { get; set; }

        public GetUserWeightDialog(string weightGoal = null)
        {
            var goal = new LuisDialogViewModel().SetWeightGoal(weightGoal);
            WeightGoal = goal;
        }

        async Task IDialog<object>.StartAsync(IDialogContext context)
        {
            PromptDialog.Choice(
         context: context,
         resume: WeightOptionRecievedAsync,
         options: new List<string>() { "In pounds", "In Kilos", "In Stones" },
         prompt: "We just need your weight how do you want to give it to us?",
         retry: "Please try again.",
         attempts: 0,
         promptStyle: PromptStyle.Auto);
        }

        private async Task WeightOptionRecievedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            var result = await activity;

            switch (result.ToLower().Trim())
            {
                case "in pounds":
                    {
                        WeightOption = WeightOption.pounds;
                    }
                    break;

                case "in kilos":
                    {
                        WeightOption = WeightOption.kilos;
                    }
                    break;

                case "in stones":
                    {
                        WeightOption = WeightOption.stones;
                    }
                    break;
            }

            PromptDialog.Text(
            context: context,
            resume: WeightRecievedAsync,
            prompt: "How much do you weight in " + WeightOption + "?",
            retry: "Please try again.",
            attempts: 0);
        }

        private async Task WeightRecievedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            var result = await activity;

            switch (WeightOption)
            {
                case WeightOption.stones:
                    {
                        BodyweightStones = Convert.ToInt32(Regex.Match(result, @"\d+").Value);
                    }
                    break;

                case WeightOption.pounds:
                    {
                        BodyweightLbs = Convert.ToInt32(Regex.Match(result, @"\d+").Value);
                    }
                    break;

                case WeightOption.kilos:
                    {
                        BodyweightKG = Convert.ToInt32(Regex.Match(result, @"\d+").Value);
                    }
                    break;
            }

            PromptDialog.Choice(
            context: context,
            resume: ExercisePerWeekRecievedAsync,
            options: new List<string>() { "2", "3", "4", "5", "6" },
            prompt: "How many days per week do you/will you usually work out?",
            retry: "Please try again.",
            attempts: 0,
            promptStyle: PromptStyle.Auto);

        }



        private async Task ExercisePerWeekRecievedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            var result = await activity;

            HowOftenExercise = Convert.ToInt32(result);


            if (WeightGoal != WeightGoal.None)
            {
                context.Done(this);
            }
            else
            {
                PromptDialog.Choice(
             context: context,
             resume: WeightGoalRecievedAsync,
             options: new List<string>() { "Gain Muscle", "Tone Up", "Loose Fat" },
             prompt: "Whats your goal?",
             retry: "Please try again.",
             attempts: 0,
             promptStyle: PromptStyle.Auto);
            }
        }

        private async Task WeightGoalRecievedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            var result = await activity;

            var goals = new List<WeightGoal>() { WeightGoal.GainMuscle, WeightGoal.ToneUp, WeightGoal.LooseFat };

            WeightGoal = goals.FirstOrDefault(i => i.ToString().ToLower().Trim() == result.ToLower().Trim());

            context.Done(this);
        }


    }
}