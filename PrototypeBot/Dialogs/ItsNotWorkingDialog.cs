﻿using Microsoft.Bot.Builder.Dialogs;
using PrototypeBot.DialogViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PrototypeBot.Dialogs
{
    [Serializable]
    public class ItsNotWorkingDialog : IDialog<object>
    {
        public string Gender { get; set; }
        public string Fitness { get; set; }
        public string Goal { get; set; }
        public string Diet { get; set; }
        public string GymHome { get; set; }

        public ItsNotWorkingDialog(string weightGoal = null)
        {
            Goal = (!string.IsNullOrEmpty(weightGoal)) ? weightGoal : "";
        }


        async Task IDialog<object>.StartAsync(IDialogContext context)
        {
            var goal = new LuisDialogViewModel().SetWeightGoal(Goal);


            if (goal != AppPrototype.Models.Enums.WeightGoal.None)
            {
                PromptDialog.Choice(
            context: context,
            resume: GenderAndLevelRecievedSkipToDietAsync,
             options: new List<string>() { "Fit man", "Fit woman", "Average man", "Average woman", "Unfit man",
            "Unfit woman" },
            prompt: "What best describes you?",
            retry: "Please try again.",
            attempts: 3,
            promptStyle: PromptStyle.Auto);
                return;
            }

            PromptDialog.Choice(
            context: context,
            resume: GenderAndLevelRecievedAsync,
            options: new List<string>() { "Fit man", "Fit woman", "Average man", "Average woman", "Unfit man",
            "Unfit woman" },
            prompt: "What best describes you?",
            retry: "Please try again.",
            attempts: 3,
            promptStyle: PromptStyle.Auto);
        }


        private async Task GenderAndLevelRecievedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            var resultString = await activity;

            Fitness = resultString.Substring(0, resultString.IndexOf(" "));
            Gender = resultString.Substring(resultString.IndexOf(" "), (resultString.Length - resultString.IndexOf(" ")));

            PromptDialog.Choice(
          context: context,
          resume: GoalRecievedAsync,
          options: new List<string>() { "Gain muscle", "Loose Fat" },
          prompt: "Whats your goal?",
          retry: "Please try again.",
          attempts: 3,
          promptStyle: PromptStyle.Auto);
        }

        private async Task GenderAndLevelRecievedSkipToDietAsync(IDialogContext context, IAwaitable<string> activity)
        {
            var resultString = await activity;

            Fitness = resultString.Substring(0, resultString.IndexOf(" "));
            Gender = resultString.Substring(resultString.IndexOf(" "), (resultString.Length - resultString.IndexOf(" ")));

            PromptDialog.Choice(
         context: context,
         resume: DietRecievedAsync,
         options: new List<string>() { "Very good", "Average", "Not that great" },
         prompt: "How healthy do you eat?",
         retry: "Please try again.",
         attempts: 3,
         promptStyle: PromptStyle.Auto);
        }

        private async Task GoalRecievedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            Goal = await activity;

            PromptDialog.Choice(
          context: context,
          resume: DietRecievedAsync,
          options: new List<string>() { "Very good", "Average", "Not that great" },
          prompt: "How healthy do you eat?",
          retry: "Please try again.",
          attempts: 3,
          promptStyle: PromptStyle.Auto);
        }

        private async Task DietRecievedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            Diet = await activity;

            PromptDialog.Choice(
          context: context,
          resume: GymHomeRecievedAsync,
          options: new List<string>() { "Gym", "Home" },
          prompt: "Where will you be working out?",
          retry: "Please try again.",
          attempts: 3,
          promptStyle: PromptStyle.Auto);
        }

        private async Task GymHomeRecievedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            GymHome = await activity;

            context.Done(this);
        }
    }
}