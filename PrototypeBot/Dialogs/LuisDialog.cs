using AppPrototype.CustomTypes;
using AppPrototype.Models;
using AppPrototype.Models.CustomTypes;
using AppPrototype.Models.CustomTypes.Food;
using AppPrototype.Models.CustomTypes.MealLogic;
using AppPrototype.Models.Enums;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Connector;
using PrototypeBot.Dialogs.DialogOptions;
using PrototypeBot.DialogViewModels;
using PrototypeBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace PrototypeBot.Dialogs
{
    [Serializable]
    public class MyEntityReccomendation
    {
        public string Entity { get; set; }
        public string Type { get; set; }
    }
    
    [Serializable]
    public class MyLUISResult
    {
        public List<MyEntityReccomendation> Entities { get; set; }

        public MyLUISResult()
        {
            Entities = new List<MyEntityReccomendation>();
        }

    }


    [LuisModel("12c2faf1-908d-4017-ae76-ddd99236660c", "85ab1d8841374a248b5b76f61d2825fd")]
    [Serializable]
    public class LuisDialog : LuisDialog<object>
    {
        //Properties
        public bool UserSaidBack { get; set; }

        public string LastCalledLuisMethod { get; set; }

        public MyLUISResult LastLuisResult { get; set; }

        public Stage CurrentStage { get; set; }

        [Serializable]
        public enum Stage
        {
            None = 0,
            Ask = 1,
            Tailor = 2,
            Answer = 3,
            Done = 4
        }

        public Session Session { get; set; }
        public Plan UpdatedPlan { get; set; }
        public Plan OriginalPlan { get; set; }
        public MuscleGroup SelectedMuscleGroup { get; set; }
        public User OrignalUser = new User();
        public LuisDialogViewModel ViewModel = new LuisDialogViewModel();
        public bool WantSmallerMethodDone { get; set; }
        public bool WantBiggerDialogHasRan { get; set; }
        public bool IveGotAnInjuryDialogHasRan { get; set; }
        public bool BestExerciseForBurningCaloriesDialogHasRan { get; set; }
        public bool WhatsMyProteinDialogHasRan { get; set; }
        public bool WantToLoseDialogHasRan { get; set; }
        public bool ItsNotWorkingDialogHasRan { get; set; }
        private bool _isStarting { get; set; }


        //Constructor
        public LuisDialog(bool isStarting = true)
        {
            _isStarting = isStarting;
        }

        //Helper Methods
        public virtual async Task ItsNotWorkingDialogComplete(IDialogContext context, IAwaitable<object> response)
        {
            ItsNotWorkingDialogHasRan = true;
            await NotWorkingIntentInLUISDialog(context, response, null);

        }

        public async Task UpdateStage(IDialogContext context, Stage stage)
        {
            switch (stage)
            {
                case Stage.Ask:
                    {
                        CurrentStage = Stage.Ask;
                        await context.PostAsync("[Ask (Current)] [Tailor (0)] [Your Answer(*)]");
                    }
                    ;
                    break;

                case Stage.Tailor:
                    {
                        CurrentStage = Stage.Tailor;
                        await context.PostAsync("[Ask (/)] [Tailor (Current)] [Your Answer(*)]");
                    }
                    ;
                    break;

                case Stage.Answer:
                    {
                        CurrentStage = Stage.Answer;
                        await context.PostAsync("[Ask (/)] [Tailor (/)] [Your Answer(Current)]");
                    }
                    ;
                    break;

                case Stage.Done:
                    {
                        CurrentStage = Stage.Done;
                        await context.PostAsync("[Ask (/)] [Tailor (/)] [Your Answer(/)]");
                    }
                    ;
                    break;
            }
        }

        public void Add(IDialogContext context)
        {
            var x = 0;
            context.PostAsync("dfds");
        }

        public virtual async Task NeedMotivationComplete(IDialogContext context, IAwaitable<string> response)
        {
            var result = await response;

            switch (result.ToLower())
            {
                case "yes":
                    {
                        await context.PostAsync("Good! only you can control if you do it, remember if something is always better than nothing! \r\n\r\n" +
                            "Setting training days can help, but if you feel you'll keep to your commitment without having set-in-stone days then go for it!");
                    }
                    break;
                case "no":
                    {
                        await context.PostAsync("A realist! We cannot fault your honesty. \r\n\r\nSetting training days/times will make things easier \U0001F4AD. \r\n\r\nJust remember you've took the time out to ask the question add another" +
                            " 14 mins and you've done a workout, just remember to get your favourite \U0001F3B5 ready. We've got your back!"
                            );
                    }
                    break;
                case "unsure":
                    {
                        await context.PostAsync("A realist! We cannot fault your honesty. \r\n\r\nSetting training days/times will make things easier \U0001F4AD. \r\n\r\nJust remember you've took the time out to ask the question add another" +
                            " 14 mins and you've done a workout, just remember to get your favourite \U0001F3B5 ready. We've got your back!"
                            );
                    }
                    break;
            }
        }

        public virtual async Task BestCalorieBurnerComplete(IDialogContext context, IAwaitable<string> response)
        {

            UpdateStage(context, Stage.Answer);


            var fitness = await response;
            var workingTime = 0;
            var restingTime = 0;
            var exercises = new List<Exercise>();

            switch (fitness.ToLower())
            {
                case "very fit":
                    {

                        exercises = new List<Exercise>() { ViewModel.GetSingleExericise("sled pull"), ViewModel.GetSingleExericise("dumbell lunges"),
                           ViewModel.GetSingleExericise("mountain climbers"), ViewModel.GetSingleExericise("push ups"), };

                        workingTime = 20;
                        restingTime = 20;

                        await context.PostAsync(
                            "\r\n\r\nTraining at the gym? \r\n\r\nIf your gyms got a sled grab it put some weights on and push it to the top then back down \U0001F3C3, rest for " + (restingTime * 2) + " seconds repeat this 3 times! (P.s. You get a little more rest for this, it uses every muscle in your body!)\r\n\r\n"
                            );
                        await context.PostAsync(
                        "\r\n\r\nTraining at home?, Haven't got a sled? \r\n\r\nYour not out the clear... grab a pair of dumbells/tins of beans and start lunge walking. " +
                        " Do it for " + workingTime + " seconds, then rest for " + restingTime + " seconds, again do it 3 Times!\r\n\r\n"
                        );
                        await context.PostAsync(
                       "\r\n\r\nNext its mountain climbers. same timing as above as above.\r\n\r\n"
                        );

                        await context.PostAsync(
                       "If your still with us? (really hope you are!). We're finishing on close grip push ups. same rules as above. " + workingTime + " seconds on " +
                       restingTime + " seconds off! .. Too easy? Do pike push ups instead?!\r\n\r\n"
                        );
                    }
                    break;
                case "average":
                    {
                        exercises = new List<Exercise>() { ViewModel.GetSingleExericise("sled pull"), ViewModel.GetSingleExericise("dumbell lunges"),
                             ViewModel.GetSingleExericise("mountain climbers"), ViewModel.GetSingleExericise("push ups") };

                        workingTime = 20;
                        restingTime = 30;

                        await context.PostAsync(
                            "\r\n\r\nTraining at the gym? \r\n\r\nIf your gyms got a sled grab it put some weights on and push it to the top then back down, rest for " + (restingTime * 2) + " seconds repeat this 3 times! (P.s. You get a little more rest for this, it uses every muscle in your body!)\r\n\r\n"
                            );
                        await context.PostAsync(
                        "\r\n\r\nTraining at home?, Haven't got a sled? \r\n\r\nYour not out the clear... grab a pair of dumbells/tins of beans and start lunge walking. " +
                        " Do it for " + workingTime + " seconds, then rest for " + restingTime + " seconds, again do it 3 Times!\r\n\r\n"
                        );
                        await context.PostAsync(
                       "\r\n\r\nNext its mountain climbers. same timing as above as above.\r\n\r\n"
                        );

                        await context.PostAsync(
                       "If your still with us? (really hope you are!). We're finishing on close grip push ups. same rules as above. " + workingTime + " seconds on " +
                       restingTime + " seconds off! .. Rome wasn't build in a day!\r\n\r\n"
                        );
                    }
                    break;
                case "un fit":
                    {
                        exercises = new List<Exercise>() { ViewModel.GetSingleExericise("sled pull"), ViewModel.GetSingleExericise("dumbell lunges"),
                            ViewModel.GetSingleExericise("push ups"), ViewModel.GetSingleExericise("sqaut jumps"), };

                        workingTime = 20;
                        restingTime = 40;

                        await context.PostAsync(
                             "\r\n\r\nTraining at the gym? \r\n\r\nIf your gyms got a sled grab it put some weights on and push it to the top then back down, rest for " + (restingTime * 2) + " seconds repeat this 3 times! (P.s. You get a little more rest for this, it uses every muscle in your body!)\r\n\r\n"
                             );
                        await context.PostAsync(
                        "\r\n\r\nTraining at home?, Haven't got a sled? \r\n\r\nYour not out the clear... grab a pair of dumbells/tins of beans and start lunge walking. " +
                        " Do it for " + workingTime + " seconds, then rest for " + restingTime + " seconds, again do it 3 Times!"
                        );
                        await context.PostAsync(
                       "\r\n\r\nNext its squat jumps. same timing as above as above.\r\n\r\n"
                        );

                        await context.PostAsync(
                       "If your still with us? (i really hope you are!). We're finishing on close grip push ups. same rules as above. " + workingTime + " seconds on " +
                       restingTime + " seconds off! .. Rome wasn't build in a day!"
                        );
                    }
                    break;
            }

            //if user beginner empty sled
            //if user not put some weight on sled
            //set the user rest time and working time
            //If your gyms got a sled grab it put some weights on and go up and down, (user.RestTime) rest do this 3 times.
            //If not don't worry grab a pair of dumbells you can hold for 20 seconds and do some weighted lunges.
            //then take a (user.RestTime) rest then its back to it! if those same dumbells are now to heavy get a lighter pair!
            //Thats exercise one done, then onto exercise 2 high knees or mountain climbers neither pretty, both effective! take your pic again 3 sets same
            //work rest ratio. 
            //Finally finish up with some push ups same rules apply!
            //
            //At home
            //do a circuit 4 rounds 4 exercises 
            //You'll be starting with some high knees to get your blood pumping do this for (user) seconds take a (user) second rest, do this 3 times 
            //if advanced go into burpes else go into squat jumps same rules apply
            //finally finish off with some push ups 
            //
            PromptDialog.Choice(
context: context,
resume: DialogOptionsComplete,
options: new List<string>() { "Ask question", "How to do exercises?" },
prompt: "Anything else?",
retry: "Selected plan not avilable . Please try again.",
promptStyle: PromptStyle.Auto);

        }

        public virtual async Task WantBiggerDialogComplete(IDialogContext context, IAwaitable<object> response)
        {
            WantBiggerDialogHasRan = true;
            await IWantBigger(context, response, null);
        }

        public virtual async Task IveGotAnInjuryDialogComplete(IDialogContext context, IAwaitable<object> response)
        {
            IveGotAnInjuryDialogHasRan = true;
            await IveGotAnInjury(context, response, null);
        }

        public virtual async Task HowLongRestDialogComplete(IDialogContext context, IAwaitable<object> response)
        {
            var result = await response;

            var howLongDialog = result as HowMuchRestDialog;
            var fitness = ViewModel.SetFitnessLevel(howLongDialog.Fitness);
            var goal = ViewModel.SetWeightGoal(howLongDialog.Goal);

            //weightlifting/bodyweight
            if (howLongDialog.Activity.ToLower().Contains("weight"))
            {
                switch (goal)
                {
                    case WeightGoal.GainMuscle:
                        {
                            if (fitness != FitnessLevel.Beginner)
                            {
                                await context.PostAsync("Rest During Sessions \r\n\r\nWe reccomend 90 seconds rest between sets/exercises. \r\n\r\nTime Between Sessions\r\n\r\nDo you use a body part split? \r\n\r\nE.g. monday back, tuesday" +
                                    "shoulders and arms etc. \r\n\r\nOr do you do full body workouts? \r\n\r\n if you do use a split we reccomend training muscle groups" +
                                    "every few days, so if you trained back yesterday wait until tomorrow to train it again, this gives the muscles time to repair" +
                                    "but not too long that theyve sitting around not doing anything \r\n\r\nOtherwise if you do full body then don't train 3 days in a row, once you've done two days have a day off");
                            }
                            else
                            {
                                await context.PostAsync("Rest During Sessions \r\n\r\nWe reccomend 120 seconds rest between sets/exercises. \r\n\r\nTime Between Sessions\r\n\r\nDo you use a body part split? \r\n\r\nE.g. monday back, tuesday" +
                                    "shoulders and arms etc. \r\n\r\nOr do you do full body workouts? \r\n\r\n if you do use a split we reccomend training muscle groups" +
                                    "every few days, so if you trained back yesterday wait until tomorrow to train it again, this gives the muscles time to repair" +
                                    "but not too long that theyve sitting around not doing anything \r\n\r\nOtherwise if you do full body then don't train 3 days in a row, once you've done two days have a day off");
                            }
                        }
                        break;
                    case WeightGoal.LooseFat:
                        {
                            await context.PostAsync("Rest During Sessions \r\n\r\nWe reccomend 60 seconds rest between sets/exercises. \r\n\r\nTime Between Sessions\r\n\r\nDo you use a body part split? \r\n\r\nE.g. monday back, tuesday" +
                                     "shoulders and arms etc. \r\n\r\nOr do you do full body workouts? \r\n\r\n if you do use a split we reccomend training muscle groups" +
                                     "every few days, so if you trained back yesterday wait until tomorrow to train it again, this gives the muscles time to repair" +
                                     "but not too long that theyve sitting around not doing anything \r\n\r\nOtherwise if you do full body then don't train 3 days in a row, once you've done two days have a day off");
                        }
                        break;
                }
            }
            else
            {
                var restingTime = 0;
                var workingTime = 0;

                switch (fitness.ToString())
                {
                    case "Advanced":
                        {
                            workingTime = 40;
                            restingTime = 20;
                        }
                        break;
                    case "Intermediate":
                        {
                            workingTime = 30;
                            restingTime = 30;
                        }
                        break;
                    case "Beginner":
                        {
                            workingTime = 20;
                            restingTime = 40;
                        }
                        break;
                }



                await context.PostAsync("Rest During Sessions \r\n\r\nWe reccomend doing each exercise for " + workingTime + " seconds followed by " + restingTime + " seconds of rest. ");
                await context.PostAsync("If this is too easy rest for " + (restingTime - 10) + " seconds. \r\n\r\nToo hard rest for " + (restingTime + 10) +
                    " seconds");
                await context.PostAsync("How long between sesions " + (restingTime - 10) + " seconds. \r\n\r\nToo hard rest for " + (restingTime + 10) +
                    " seconds\r\n\r\nTime Between Sessions\r\n\r\n After two days back to back exercise take a day of. Simple.");


            }



        }

        public virtual async Task DialogOptionsComplete(IDialogContext context, IAwaitable<string> response)
        {
            var result = await response;

            if (result.ToLower() == "ask question") { await context.PostAsync("Fire away!"); return; }; //leave the method and wait for the next question

            //How to do exercises picked
            if (result.ToLower() == "how to do exercises?")
            {
                //create the msg to send
                var msg = context.MakeMessage();
                //convert the exercises to attachments and attachment them to the msg
                //ive hack this method below
                //var cards = GetExerciseCards(ViewModel.GetExericises(SelectedMuscleGroup, WeightGoal.GainMuscle, FitnessLevel.Advanced, GymOrHome.Home));


                //var cards = GetExerciseCards(new List<Exercise>() { new ExerciseFilterer().FindSingle("squat"), new ExerciseFilterer().FindSingle("jump squat"),
                //new ExerciseFilterer().FindSingle("shoulder press"),new ExerciseFilterer().FindSingle()});
                //foreach (var card in cards)
                //    msg.Attachments.Add(card);


                var card1 = new HeroCard(title: "Arnold Presses", subtitle: "Bright weight out 45 degrees then raise above your head, control the weight at all times.", images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\Arnold Presses.gif") }).ToAttachment();
                var card2 = new HeroCard(title: "Chair Dips", subtitle: "Slowly bend your elbow to a 45 degree angle (ensure you don't go any lower to prevent injury) explode upwards.", images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\Chair Dips.gif") }).ToAttachment();
                var card3 = new HeroCard(title: "Sqauts", subtitle: "Stand shoulder width apart with your hands infront of you, Slowly bend your knees til they are parralel with your hips. Hold it for a second then explode up.", images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\Sqaut.gif") }).ToAttachment();
                var card4 = new HeroCard(title: "Lunge Walks", subtitle: "Stand shoulder with apart take your right leg and bend at the knee to a right angle the controling your balance bring your left leg forwards and come up, swap legs.", images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\Lunge Walks.gif") }).ToAttachment();
                var card5 = new HeroCard(title: "Sqaut Jumps", subtitle: "Stand shoulder width apart with your hands infront of you, Slowly bend your knees til they are parralel with your hips. Hold it for a second then explode up.", images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\Sqaut Jumps.gif") }).ToAttachment();
                msg.Attachments.Add(card1);
                msg.Attachments.Add(card2);
                msg.Attachments.Add(card3);
                msg.Attachments.Add(card4);
                msg.Attachments.Add(card5);

                await context.PostAsync(msg);
            };
        }

        public virtual async Task WantBiggerDialogOptionsComplete(IDialogContext context, IAwaitable<object> response)
        {
            var iwantBiggerOptionsDone = await response;
            var iWantBiggerDialogOptions = iwantBiggerOptionsDone as WantBiggerDialogOptions;

            //Ask a question
            var askQuestion = iWantBiggerDialogOptions.AskAQuestion;
            if (askQuestion) { await context.PostAsync("Fire away!"); return; }; //leave the method and wait for the next question

            //How to do exercises picked
            var showExercises = iWantBiggerDialogOptions.ShowExercises;
            if (showExercises)
            {
                //create the msg to send
                var msg = context.MakeMessage();
                //convert the exercises to attachments and attachment them to the msg
                //ive hack this method below
                //var cards = GetExerciseCards(ViewModel.GetExericises(SelectedMuscleGroup, WeightGoal.GainMuscle, FitnessLevel.Advanced, GymOrHome.Home));

                //   var cards = GetExerciseCards(new List<Exercise>() { new ExerciseFilterer().FindSingle("shoulder press"), new ExerciseFilterer().FindSingle("chair dips"),
                // new ExerciseFilterer().FindSingle("sqaut jumps"),new ExerciseFilterer().FindSingle()});
                // foreach (var card in cards)

                var card1 = new HeroCard(title: "Arnold Presses", subtitle: "Bright weight out 45 degrees then raise above your head, control the weight at all times.", images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\Arnold Presses.gif") }).ToAttachment();
                var card2 = new HeroCard(title: "Chair Dips", subtitle: "Slowly bend your elbow to a 45 degree angle (ensure you don't go any lower to prevent injury) explode upwards.", images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\Chair Dips.gif") }).ToAttachment();
                var card3 = new HeroCard(title: "Sqauts", subtitle: "Stand shoulder width apart with your hands infront of you, Slowly bend your knees til they are parralel with your hips. Hold it for a second then explode up.", images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\Sqaut.gif") }).ToAttachment();
                var card4 = new HeroCard(title: "Lunge Walks", subtitle: "Stand shoulder with apart take your right leg and bend at the knee to a right angle the controling your balance bring your left leg forwards and come up, swap legs.", images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\Lunge Walks.gif") }).ToAttachment();
                var card5 = new HeroCard(title: "Sqaut Jumps", subtitle: "Stand shoulder width apart with your hands infront of you, Slowly bend your knees til they are parralel with your hips. Hold it for a second then explode up.", images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\Sqaut Jumps.gif") }).ToAttachment();
                msg.Attachments.Add(card1);
                msg.Attachments.Add(card2);
                msg.Attachments.Add(card3);
                msg.Attachments.Add(card4);
                msg.Attachments.Add(card5);

                await context.PostAsync(msg);
            };

            //Want an expert plan
            var wantExpertPlan = iWantBiggerDialogOptions.WantExpertPlan;
            if (wantExpertPlan) { await context.PostAsync("Sorry, expert plans are not availible yet!"); return; };

            //Want a standard plan
            var wantStandardPlan = iWantBiggerDialogOptions.WantStandardPlan;
            if (wantStandardPlan) { context.Call<object>(new PlanGeneratorDialog(), PlanCreatedDialogComplete); };
        }

        public virtual async Task WantSmallerDialogOptionsComplete(IDialogContext context, IAwaitable<object> response)
        {
            var iwantSmallerOptionsDone = await response;
            var iWantSmallerDialogOptions = iwantSmallerOptionsDone as WantSmallerDialogOptions;

            //Ask a question
            var askQuestion = iWantSmallerDialogOptions.AskAQuestion;
            if (askQuestion) { await context.PostAsync("Fire away!"); return; }; //leave the method and wait for the next question

            //How to do exercises picked
            var showExercises = iWantSmallerDialogOptions.ShowExercises;
            if (showExercises)
            {
                //create the msg to send
                var msg = context.MakeMessage();
                //convert the exercises to attachments and attachment them to the msg
                // var cards = GetExerciseCards(ViewModel.GetExericises(SelectedMuscleGroup, WeightGoal.GainMuscle, FitnessLevel.Advanced, GymOrHome.Home));
                var cards = GetExerciseCards(new List<Exercise>() { new ExerciseFilterer().FindSingle("squat"), new ExerciseFilterer().FindSingle("jump squat"),
                new ExerciseFilterer().FindSingle("shoulder press"),new ExerciseFilterer().FindSingle("")});
                foreach (var card in cards)
                    msg.Attachments.Add(card);

                await context.PostAsync(msg);

            };

            //Want an expert plan
            var wantExpertPlan = iWantSmallerDialogOptions.WantExpertPlan;
            if (wantExpertPlan) { await context.PostAsync("Sorry, expert plans are not availible yet!"); return; };

            //Want a standard plan
            var wantStandardPlan = iWantSmallerDialogOptions.WantStandardPlan;
            if (wantStandardPlan) { context.Call<object>(new PlanGeneratorDialog(), PlanCreatedDialogComplete); };

            //Want more food advice
            var wantFoodAdvice = iWantSmallerDialogOptions.MoreOnFood;
            if (wantFoodAdvice)
            {
                //Firstly i need to call this to get the users weight, then i can make a food plan for them.
                context.Call<object>(new GetUserWeightDialog(), LooseWeightGetUserWeightDialogComplete);
            }
        }

        public virtual async Task LooseWeightGetUserWeightDialogComplete(IDialogContext context, IAwaitable<object> response)
        {

            try
            {

                var result = await response;
                var userWeightDialog = result as GetUserWeightDialog;

                var bodyWeight = userWeightDialog.BodyweightLbs;
                var weightOption = userWeightDialog.WeightOption;
                var daysTrainingPerWeek = userWeightDialog.HowOftenExercise;

                //if (userWeightDialog.BodyweightKG > 0)
                //    bodyWeight = userWeightDialog.BodyweightKG;
                //if (userWeightDialog.BodyweightLbs > 0)
                //    bodyWeight = userWeightDialog.BodyweightLbs;
                //if (userWeightDialog.BodyweightStones > 0)
                //    bodyWeight = userWeightDialog.BodyweightStones;

                //bodyWeight = ViewModel.GetWeightInPounds(weightOption, bodyWeight); < This method converts the bodyweight

                var activityLevel = UserActivityLevel.ExtraActive;

                switch (daysTrainingPerWeek)
                {
                    case 3: { activityLevel = UserActivityLevel.LightlyActive; } break;
                    case 4: { activityLevel = UserActivityLevel.ModeratelyActive; } break;
                    case 5: { activityLevel = UserActivityLevel.VeryActive; } break;
                    case 6: { activityLevel = UserActivityLevel.ExtraActive; } break;
                }


                var returnString = "Heres a tailored list of meals you could eat daily based on what i know about you \r\n\r\n";

                //go to the meal plan creator and make a version that doesnt need a plan it just takes the variables that we have and 
                //create the plan.

                var generator = new MealPlanGenerator();

                //OrignalUser = generator.GenerateMeals(WeightGoal.LooseFat, bodyWeight.ToString(), activityLevel, WeightMethod.Pound); // this returns the mealPlan Object

                //returnString += "Breakfast Options \r\n\r\n";

                //returnString += OrignalUser.NutritionPlan.Breakfasts.QuickView + "\r\n\r\n";


                //returnString += "Snack Options \r\n\r\n";

                //returnString += OrignalUser.NutritionPlan.Snacks.QuickView + "\r\n\r\n";

                //returnString += "Lunch Options \r\n\r\n";

                //returnString += OrignalUser.NutritionPlan.Lunches.QuickView + "\r\n\r\n";

                //returnString += "Dinner Options \r\n\r\n";

                //returnString += OrignalUser.NutritionPlan.Dinners.QuickView + "\r\n\r\n";

                await context.PostAsync(returnString);

                //what kind of food advice may they need?? ... this is i want smaller which obvs means they want to loose weight so interested in food. However il be giving this advice on what can i eat also so should this
                //be transferable, as the show exercises method is.. we can make it transferable listening to what there asking for MoreInfo. they want to know How to do the exercises, i should get their calories and give them
                //2 days meal plans becase that gives them more structure, 
                //
                //I know they want to loose weight so once i have their calories i need to take -100 off per day
                //split this between 4 meals as im already doing 
                //I use my meal generator to generate meals however instead of just skimming it i could create a full meal plan for them (meaning the options that ive previously done) 
            }
            catch (Exception ex)
            {
                var i = ex.Message;
            }

        }

        public virtual async Task PlanCreatedDialogComplete(IDialogContext context, IAwaitable<object> response)
        {

            OriginalPlan = new Plan() { /*COPY FROM OLD VERSION just assigning the vars to the props*/ };
            UpdatedPlan = new IWantBigger().WantBigger(OriginalPlan, SelectedMuscleGroup, AppPrototype.Models.Enums.ResultExpectancy.ASAP);

            var sessionsString = "";

            foreach (var ses in UpdatedPlan.Sessions)
                sessionsString += "\r\n\r\n" + ses.ToString();

            var returnString = "Your plans been created heres an overview \r\n\r\n" +
                "Focus: Bigger " + SelectedMuscleGroup + "\r\n" +
                "Activity type: " + UpdatedPlan.Activity + "\r\n" +
                "Weight goal: " + UpdatedPlan.WeightGoal + "\r\n" +
                "Days per week: " + UpdatedPlan.DaysPerWeek + "\r\n" +
                "Session Length: " + UpdatedPlan.SessionLength + "\r\n" +
                "Calories per day: " + UpdatedPlan.User.Calories + "\r\n" +
                sessionsString;

            await context.PostAsync(returnString);
        }

        public virtual async Task PostWantBiggerOptionRecievedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            var result = await activity;

            switch (result.ToLower().Trim())
            {
                case "how to do the exercises":
                    {

                    }
                    break;
                case "i want jordan peters to create me a tailored plan":
                    {

                    }
                    break;
            }
        }

        public virtual async Task WantToLoseDialogComplete(IDialogContext context, IAwaitable<object> response)
        {
            WantToLoseDialogHasRan = true;
            await IWantSmaller(context, response, null);
        }

        private async Task IveGotNoTimeOrIDontLikeToWorkOutComplete(IDialogContext context, IAwaitable<string> result)
        {
            var response = await result;

            var setTime = 0;
            var restTime = 0;
            var exercises = new List<Exercise>();

            switch (response.ToLower())
            {
                case "very fit":
                    {
                        setTime = 20;
                        restTime = 20;

                        exercises = new List<Exercise>() { ViewModel.GetSingleExericise("burpes"), ViewModel.GetSingleExericise("lunges"), ViewModel.GetSingleExericise("close grip push ups") };

                        await context.PostAsync(
                        "Do a set of " + ViewModel.GetSingleExericise("burpes").Name + ",\r\n\r\nFollowed by a set of " + ViewModel.GetSingleExericise("lunges").Name + ", \r\n\r\nFinish off with a set of " + ViewModel.GetSingleExericise("close grip push ups").Name +
                        "\r\n\r\nDo each exercise for " + setTime + " seconds, followed by " + restTime + " seconds rest then move onto next exercise do this for 3 rounds"
                         );

                        await context.PostAsync(
                        "\r\n\r\nToo difficult?" +
                        "\r\n\r\nWork for " + (setTime) + " seconds, rest for " + (restTime + 10) +
                         "\r\n\r\nToo easy?" +
                        "\r\n\r\nWork for " + (setTime + 10) + " seconds, rest for " + (restTime)
                        );
                    }
                    break;
                case "average":
                    {
                        setTime = 20;
                        restTime = 30;

                        exercises = new List<Exercise>() { ViewModel.GetSingleExericise("high knees"), ViewModel.GetSingleExericise("close grip push ups"), ViewModel.GetSingleExericise("sqaut jumps") };

                        await context.PostAsync(
                        "Do a set of " + ViewModel.GetSingleExericise("high knees").Name + ",\r\n\r\nFollowed by a set of " + ViewModel.GetSingleExericise("close grip push ups").Name + ", \r\n\r\nFinish off with a set of " + ViewModel.GetSingleExericise("sqaut jumps").Name +
                        "\r\n\r\nDo each exercise for " + setTime + " seconds, followed by " + restTime + " seconds rest then move onto next exercise do this for 3 rounds"
                         );

                        await context.PostAsync(
                        "\r\n\r\nToo difficult?" +
                        "\r\n\r\nWork for " + (setTime) + " seconds, rest for " + (restTime + 10) +
                         "\r\n\r\nToo easy?" +
                        "\r\n\r\nWork for " + (setTime + 10) + " seconds, rest for " + (restTime)
                        );
                    }
                    break;
                case "un fit":
                    {
                        setTime = 20;
                        restTime = 40;

                        exercises = new List<Exercise>() { ViewModel.GetSingleExericise("high knees"), ViewModel.GetSingleExericise("close grip push ups"), ViewModel.GetSingleExericise("sqauts") };

                        await context.PostAsync(
                        "Do a set of " + ViewModel.GetSingleExericise("high knees").Name + ",\r\n\r\nFollowed by a set of " + ViewModel.GetSingleExericise("close grip push ups").Name + ", \r\n\r\nFinish off with a set of " + ViewModel.GetSingleExericise("sqauts").Name +
                        "\r\n\r\nDo each exercise for " + setTime + " seconds, followed by " + restTime + " seconds rest then move onto next exercise do this for 3 rounds"
                        );

                        await context.PostAsync(
                        "\r\n\r\nToo difficult?" +
                        "\r\n\r\nWork for " + (setTime) + " seconds, rest for " + (restTime + 10) +
                         "\r\n\r\nToo easy?" +
                        "\r\n\r\nWork for " + (setTime + 10) + " seconds, rest for " + (restTime)
                        );
                    }
                    break;
            }

            //call the dialog options show me the exercises, i want a plan, i want to upgrade)"
            PromptDialog.Choice(
context: context,
resume: DialogOptionsComplete,
options: new List<string>() { "Ask question", "How to do exercises?" },
prompt: "Anything else?",
retry: "Selected plan not avilable . Please try again.",
promptStyle: PromptStyle.Auto);
        }

        private async Task HowLongShouldIWorkOutForComplete(IDialogContext context, IAwaitable<string> result)
        {
            var response = await result;

            switch (response.ToLower())
            {
                case "gain muscle":
                    {
                        await context.PostAsync(
                        "\r\n\r\nWith weights \r\n\r\nA sweet spot would be 45 minutes giving you time to really hit the muscle group(s) for growth. Don't worry you can still" +
                        "do a good job with as little as 30 minutes, but any less should be avoided, and any more than 45 maybe overkill.\r\n\r\n"
                        );

                        await context.PostAsync(
                        "\r\n\r\nWithout weights \r\n\r\n30 minutes is probably where you want to be hitting as this gives you time to do alot of exercises, sound fun? Maybe not but youll grow!" +
                        "\r\n\r\nAny longer isn't nesscary. Try not to drop below 20 minutes as when you take out the resting time you wont have much left :(\r\n\r\n"
                        );
                    }
                    break;
                case "tone up":
                    {
                        await context.PostAsync(
                        "\r\n\r\nWith weights \r\n\r\nA sweet spot would be 45 minutes giving you time to really hit the muscle group(s) for growth. Don't worry you can still" +
                        "do a good job with as little as 30 minutes, but any less should be avoided, and any more than 45 maybe overkill.\r\n\r\n"
                        );

                        await context.PostAsync(
                        "\r\n\r\nWithout weights \r\n\r\n30 minutes is probably where you want to be hitting as this gives you time to do alot of exercises, sound fun? Maybe not but youll grow!" +
                        "\r\n\r\nAny longer isn't nesscary. Try not to drop below 20 minutes as when you take out the resting time you wont have much left :(\r\n\r\n"
                        );
                    }
                    break;
                case "loose fat":
                    {
                        await context.PostAsync(
                        "\r\n\r\nWith weights\r\n\r\nA sweet spot would be 30 minutes giving you time to really hit the muscle group(s) yet not too much time that your resting" +
                        " too often between sets which is letting your heart rate drop back down." +
                        "\r\n\r\nTen minutes either side of this is fine but try not to go any further out than that.\r\n\r\n"
                        );

                        await context.PostAsync(
                        "\r\n\r\nWithout weights\r\n\r\n20 minutes is probably the optimal time you want to be working out as it allows you to do a few rounds of intense exercise. \r\n\r\nSound fun? No but you'll loose, weight!\r\n\r\n" +
                        "Ten minutes either side of this is fine but try not to go any further out than that.\r\n\r\n"
                        );
                    }
                    break;
            }
        }

        private async Task WhatsMyProteinOrCalsComplete(IDialogContext context, IAwaitable<object> result)
        {
            var activity1 = await result;
            var whatsMyXDialog = activity1 as GetUserWeightDialog;

            var bodyWeight = 0d;
            var weightOption = whatsMyXDialog.WeightOption;
            var daysTrainingPerWeek = whatsMyXDialog.HowOftenExercise;

            if (whatsMyXDialog.BodyweightKG > 0)
                bodyWeight = whatsMyXDialog.BodyweightKG;
            if (whatsMyXDialog.BodyweightLbs > 0)
                bodyWeight = whatsMyXDialog.BodyweightLbs;
            if (whatsMyXDialog.BodyweightStones > 0)
                bodyWeight = whatsMyXDialog.BodyweightStones;

            //get the call diff from the dialog e.g. less cals if wanna loose weight
            var calDiff = 0;
            switch (whatsMyXDialog.WeightGoal)
            {
                case WeightGoal.GainMuscle:
                    { calDiff = 100; } break;
                case WeightGoal.ToneUp:
                    { calDiff = 0; }
                    break;
                case WeightGoal.LooseFat:
                    { calDiff = -100; }
                    break;

            }

            //get the protein n cals
            var user = new User() { Weight = ViewModel.GetWeightInPounds(weightOption, bodyWeight),
                UserActivityLevel = ViewModel.SetUserActivityLevel(whatsMyXDialog.HowOftenExercise) };
            var generator = new MacroGenerator();

          //  generator.SetCaloriesWeightOnly(user, calDiff);
            // fix this its a hack below
          //  generator.SetMacros(user, (whatsMyXDialog.WeightGoal == WeightGoal.None) ? WeightGoal.ToneUp : whatsMyXDialog.WeightGoal);

            await context.PostAsync("You should consume " + Convert.ToInt32(user.Calories) + " calories per day.\r\n\r\nTry to ensure to have " +
              Convert.ToInt32(user.NutritionPlan.MacroList.Protein) + " grams of protein per day.");

            await context.PostAsync("If you want to track macros \r\n\r\n(not advised for beginners or those who don't want to spend alot of time making food decisions)\r\n\r\n " +
                "your macros could look like this \r\n\r\n Protein: " + Convert.ToInt32(user.NutritionPlan.MacroList.Protein) +
                "\r\n\r\n Carbs: " + Convert.ToInt32(user.NutritionPlan.MacroList.Carbs) +
                "\r\n\r\n Fats: " + Convert.ToInt32(user.NutritionPlan.MacroList.Fat) + "\r\n\r\n Ensure you get your fibre and fruit and veg within this!!");

        }

        public Attachment GetExpertCard(string Expert, string muscleGroup = null)
        {
            //search for the expert
            var card = new HeroCard
            {
                Title = "Hi I'm " + Expert + " \r\n ive trained over 1000 people\r\n\r\n Heres my reccomendation for your getting bigger " + muscleGroup, Images = new List<CardImage> { new CardImage("https://lh3.googleusercontent.com/-fnwLMmJTmdk/WaVt5LR2OZI/AAAAAAAAG90/qlltsHiSdZwVdOENv1yB25kuIvDWCMvWACLcBGAs/h120/annuvalevent.PNG"),
            }
            };
            return card.ToAttachment();
        }

        private List<Attachment> GetExerciseCards(List<Exercise> exercises)
        {
            var cards = new List<HeroCard>();
            var attachments = new List<Attachment>();

            foreach (var ex in exercises)
            {
                //            var card = new HeroCard(title: "You ready to train? \U0001F60F", images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\men_arnoldpress.gif") }).ToAttachment();


                cards.Add(new HeroCard(title: ex.Name, images: new List<CardImage> { new CardImage(@"D:\DDriveDownloads\" + ex.Name + ".gif") }
                    ));
            }

            foreach (var i in cards)
            {
                attachments.Add(i.ToAttachment());
            }
            return attachments;
        }

        private async Task StartingMethod(IDialogContext context)
        {
            await context.PostAsync("Welcome to Answerfit!, \r\n\r\nAsk any fitness question and receive an experts response tailored to you!");
            await context.PostAsync("Ask away..");
        }

        private async Task GoBack(IDialogContext context)
        {
            if (CurrentStage == Stage.Tailor || CurrentStage == Stage.Ask || CurrentStage == Stage.None)
            {
                UpdateStage(context, Stage.Ask);
                await context.PostAsync("Ask us something :) Just type your question and hit send! ");
            }
            else
            {
                MethodInfo lastMethod = this.GetType().GetMethod(LastCalledLuisMethod);

                lastMethod.Invoke(this, new object[] { context, null, new LuisResult() });

                //Type classType = this.GetType();
                //object obj = Activator.CreateInstance(classType);
                //object[] parameters = new object[] { context, null, new LuisResult() };
                //MethodInfo mi = classType.GetMethod(LastCalledLuisMethod);
                //ThreadStart threadMain = delegate () { mi.Invoke(this, parameters); };
                //new System.Threading.Thread(threadMain).Start();


            }
        }

        public async Task<bool> ListenForBack(IDialogContext context, Stage stage, LuisResult result, string activity)
        {
            if (activity != null && (activity.ToLower() == "go back" || activity.ToLower().Contains("go back")))
            {
                await GoBack(context);
                return true;
            }

            if (result != null && result.Query != null && (result.Query.ToLower().Trim() == "go back" || result.Query.ToLower().Trim().Contains("go back")))
            {
                await GoBack(context);
                return true;
            }
            return false;
        }

        public void UpdateChanges(string methodName, LuisResult result)
        {
            LastCalledLuisMethod = methodName;
            LastLuisResult = new MyLUISResult();

            foreach (var i in result.Entities)
            {
                LastLuisResult.Entities.Add(new MyEntityReccomendation() { Entity = i.Entity, Type = i.Type });
            }
        }




        //Luis Methods

        //Training Intents
        [LuisIntent("")]
        public async Task None(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            var myString = "";
            if (activity != null)
            {
                var response = await activity;
                myString = response.Text ?? "";
            }
            if (await ListenForBack(context, CurrentStage, result, myString ?? ""))
                return;

            if (_isStarting)
            {
                //variables
                var typingMsg = context.MakeMessage();
                typingMsg.Type = ActivityTypes.Typing;

                await context.PostAsync(typingMsg);
                Thread.Sleep(2000);

                //send the greeting
                await context.PostAsync("Welcome to Answerfit");

                //wait 2 seconds for the user to read it
                Thread.Sleep(3000);

                //send the typing message
                await context.PostAsync(typingMsg);
                Thread.Sleep(2000);

                //send the follow up message
                await context.PostAsync("Got a fitness question?");

                //wait 2 seconds for the user to read it
                Thread.Sleep(2000);

                //send the typing message
                await context.PostAsync(typingMsg);
                Thread.Sleep(3000);

                await context.PostAsync("Just ask and we'll give you an expertly tailored answer for free!, No more searching online or paying a personal trainer");

                //wait 5 seconds for user to read
                Thread.Sleep(7000);

                ////send the typing message
                //await context.PostAsync(typingMsg);
                //Thread.Sleep(2000);
                //await context.PostAsync("The only thing we cant do is the exercise for you.");

                ////wait 3 seconds for the user to read it
                //Thread.Sleep(3000);

                //send the typing message
                await context.PostAsync(typingMsg);
                //wait 2 seconds for the user to read it
                Thread.Sleep(3000);

                //send the follow up message
                await context.PostAsync("Take a look how it works..");

                //send the typing message
                await context.PostAsync(typingMsg);

                //wait 4 seconds for the user to read it
                Thread.Sleep(4000);

                //send the "how it works" Gif/Video

                var msg = context.MakeMessage();
                msg.Attachments = new List<Attachment>();
                msg.Attachments.Add((new HeroCard()
                {
                    Images = new List<CardImage> { new CardImage("https://lh3.googleusercontent.com/-fnwLMmJTmdk/WaVt5LR2OZI/AAAAAAAAG90/qlltsHiSdZwVdOENv1yB25kuIvDWCMvWACLcBGAs/h120/annuvalevent.PNG") }
                }).ToAttachment());

                await context.PostAsync(msg);



                //wait 14 seconds for the user to read it
                Thread.Sleep(14000);

                //send the typing message
                await context.PostAsync(typingMsg);
                //wait 3 seconds for the user to read it
                Thread.Sleep(3000);

                //send the user options
                PromptDialog.Choice(
                 context: context,
                 resume: OptionDecidedAsync,
                 options: new List<string>() { "Yes", "Help me ask" },
                 prompt: "Got something to ask?",
                 retry: "Please try again.",
                 attempts: 3,
                 promptStyle: PromptStyle.Keyboard);

            }
            else
            {
                await context.PostAsync("Sorry could you rephrase that? Just ask any fitness question and we'll try to answer it");
            }

            //var card = new HeroCard(title: "Welldone", images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\handgif.gif") },
            //       buttons: new List<CardAction>() { new CardAction(type: ActionTypes.ImBack, value: "post", title: "Share") }).ToAttachment();
            //var msg = context.MakeMessage();
            //msg.Attachments = new List<Attachment>() { card };
            //await context.PostAsync(msg);
        }

        private async Task OptionDecidedAsync(IDialogContext context, IAwaitable<string> result)
        {
            //variables
            var typingMsg = context.MakeMessage();
            typingMsg.Type = ActivityTypes.Typing;
            await context.PostAsync(typingMsg);
            Thread.Sleep(1000);

            var response = await result;

            if (response.ToLower().Trim() == "yes")
            {
                //send the progress update
                var msg = context.MakeMessage();
                msg.Attachments = new List<Attachment>() { new HeroCard(images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\stage_ask.jpg") }).ToAttachment() };
                await context.PostAsync(msg);

                //wait 3 seconds
                Thread.Sleep(3000);

                //send typing msg
                await context.PostAsync(typingMsg);

                //wait 2 seconds
                Thread.Sleep(3000);


                await context.PostAsync("Simply type your question and click send!");

                //wait 3 seconds
                Thread.Sleep(3000);

                await context.PostAsync(typingMsg);
                Thread.Sleep(2000);

                await context.PostAsync("heres a few examples: \r\n\r\nI want bigger X \r\n\r\nI want smaller X \r\n\r\nWhat burns the most calories? ");

            }
            else
            {
                //guide the user how it will work, this will explain how things are working before we send the msgs.
            }
        }

        private async Task UserSentAQuestionAsync(IDialogContext context, IAwaitable<string> result)
        {
            var question = await result;



        }

        [LuisIntent("ItsNotWorking")]
        private async Task NotWorkingIntentInLUISDialog(IDialogContext context, IAwaitable<object> activity, LuisResult result)
        {
            //variables
            var typingMsg = context.MakeMessage();
            typingMsg.Type = ActivityTypes.Typing;

            //wait for 2 seconds
            Thread.Sleep(1000);

            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(3000);



            //send the progress update
            var msg = context.MakeMessage();
            msg.Attachments = new List<Attachment>() { new HeroCard(images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\stage_tailor.jpg") }).ToAttachment() };
            await context.PostAsync(msg);

            //wait for 2 seconds
            Thread.Sleep(2000);
            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(1000);



            if (result.Query.ToLower().Trim() == "go back" || result.Query.ToLower().Trim().Contains("go back"))
            {
                await GoBack(context);
                return;
            }

            var goalEntity = new EntityRecommendation();

            if (result != null)
            {
                var entities = new List<EntityRecommendation>(result.Entities);
                goalEntity = (entities.Where((entity) => entity.Type == "WeightGoal").Count() > 0) ? entities.Where((entity) => entity.Type == "WeightGoal").First() : null;


                if (goalEntity != null && !ItsNotWorkingDialogHasRan)
                {
                    context.Call<object>(new ItsNotWorkingDialog(goalEntity.Entity), ItsNotWorkingDialogComplete);
                    return;
                }
            }

            if (!ItsNotWorkingDialogHasRan)
            {
                context.Call<object>(new ItsNotWorkingDialog(), ItsNotWorkingDialogComplete);
                return;
            }


            //send the progress update
            msg.Attachments = new List<Attachment>() { new HeroCard(images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\stage_tailor.jpg") }).ToAttachment() };
            await context.PostAsync(msg);

            var activity1 = await activity;
            var itsNotWorkingDialog = activity1 as ItsNotWorkingDialog;

            var Gender = ViewModel.SetGender(itsNotWorkingDialog.Gender.ToLowerInvariant().Trim());
            var Goal = ViewModel.SetWeightGoal(itsNotWorkingDialog.Goal);
            var Diet = itsNotWorkingDialog.Diet.ToLowerInvariant().Trim();
            var GymHome = ViewModel.SetGymOrHome(itsNotWorkingDialog.GymHome.ToLowerInvariant().Trim());
            var Fitness = ViewModel.SetFitnessLevel(itsNotWorkingDialog.Fitness.ToLowerInvariant().Trim());


            //Set the heading
            var workingTime = 0;
            var restingTime = 0;




            var reccomendation = new Reccomendation
            {
                OpeningMessage = (Goal == WeightGoal.LooseFat) ? "Remember loosing weight and fat arent the same thing , if you've been putting in effort with both training and eating " +
                "chances are you've burnt off some fat and toned your body up with a slight bit of muscle, as muscle weighs more than fat your weight won't have dropped but you'll be looking better \r\n\r\n" :
                "Remember gaining muscle means you'll need to eat alot of clean food, do you know how many calories you need? If not ask 'Whats my calories?' \r\n\r\n"
            };

            if (Goal == WeightGoal.GainMuscle)
            {


                switch (Fitness.ToString())
                {
                    case "Advanced":
                        {
                            restingTime = 90;
                        }
                        break;
                    case "Intermediate":
                        {
                            restingTime = 90;
                        }
                        break;
                    case "Beginner":
                        {
                            restingTime = 120;
                        }
                        break;
                }

                //Set the heading
                var dietSection = (Diet != "very good") ? "Most Important - Diet \r\n\r\n " : "Diet \r\n\r\n";
                dietSection += "Argubly more important then the training because you can only put weight on by consuming more than you burn. That said you still need to eat wisely"
              + "\r\n\r\nEat smaller and more often e.g. instead of 3 big meals have 2 big and 2 small"
                + "\r\n\r\nStick to good carbs such as brown rice, brown pasta, sweet pototoe etc, For meats stick to chicken, turkey breast, beef, ensure you get enough veg/fruit \r\n\r\n";
                reccomendation.ReccommendedFood = dietSection;

                //Set the heading
                var activitySection = "Activity \r\n\r\n";
                activitySection += "We reccommend weight training with weights for the quickest mass gain however if you want to train at home " +
                    "compound bodyweight exercises can also be effective \r\n\r\n";
                reccomendation.ReccomendedActivity = activitySection;

                //Set the heading
                var exercisesSection = "Exercises \r\n\r\n";

                exercisesSection += "As your not steadily gaining muscle mass at the minute, lets try a" +
                    " less is more approach. \r\n\r\nStripping back to focusing on the tried and tested most effective exercises and then building week on week from them is " +
                    " key to finding what works and whats a waste of time! Ensure to just focus on these for 2 weeks \r\n\r\n";

                //Get exercises most pop to least pop
                var exercises = new List<Exercise>();
                if (Gender == AppPrototype.Models.Enums.Gender.M)
                {
                    if (GymHome == GymOrHome.Gym)
                        exercises = new List<Exercise>() { ViewModel.GetSingleExericise("vertical chest press"), ViewModel.GetSingleExericise("pull downs"),
                     ViewModel.GetSingleExericise("squats jumps"), };
                    exercises = new List<Exercise>() { ViewModel.GetSingleExericise("close grip push ups"), ViewModel.GetSingleExericise("chair dips"),
                     ViewModel.GetSingleExericise("sqaut jumps"), };
                }
                else
                {
                    if (GymHome == GymOrHome.Gym)
                        exercises = new List<Exercise>() { ViewModel.GetSingleExericise("arnold presses"), ViewModel.GetSingleExericise("pull downs"),
                     ViewModel.GetSingleExericise("sqauts jumps"), };
                    exercises = new List<Exercise>() { ViewModel.GetSingleExericise("close grip push ups"), ViewModel.GetSingleExericise("chair dips"),
                     ViewModel.GetSingleExericise("sqaut jumps"), };
                }

                //add them to the string
                foreach (var i in exercises)
                    exercisesSection += " " + i.Name + " - Rating: " + i.Popularity.ToString().Substring(0, 3) + "\r\n\r\n";

                reccomendation.ReccomendedExercises = exercisesSection;

                //Trainer advice should include training techniques, frequencies, tips
                var trainerAdvice = "Expert Tips \r\n\r\n";
                trainerAdvice += (GymHome == GymOrHome.Gym) ? "1. For each exercise do 3 sets of 6-8 reps then half the weight and do a final set. " +
                    "Rest for " + restingTime + " seconds between sets"
                    : "1. For each exercise do as many reps as you can. Rest for " + restingTime + " seconds between sets";
                trainerAdvice += "\r\n\r\n 2. Dont rush, take your time with each movement of the exercise, rushing won't get you what you want faster";

                reccomendation.ExpertNote = trainerAdvice;
            }
            else
            {
                //User wants to loose fat

                switch (Fitness.ToString())
                {
                    case "Advanced":
                        {
                            workingTime = 40; restingTime = 20;
                        }
                        break;
                    case "Intermediate":
                        {
                            workingTime = 30; restingTime = 30;
                        }
                        break;
                    case "Beginner":
                        {
                            workingTime = 20; restingTime = 40;
                        }
                        break;
                }


                //Set the heading
                var dietSection = (Diet != "very good") ? "Most Important - Diet \r\n\r\n " : "Diet \r\n\r\n";
                dietSection += "Argubly more important then the training because you can only loose weight by burning more than you consume. That said you still need to eat wisely."
              + "\r\n\r\nIf your calories drop too quickly or you lack foods that give you energy you might end up doing more harm than good. " +
                "\r\n\r\nTry to base your meals around vegetables and proteins, however this doesn't mean avoid carbs or you'll fall into the trap as mentioned above " +
                 "\r\n\r\nRemember carbs aren't your enemy, its the junk foods and drinks that ruin diets."  /*possibleFoods.ToList*/ ;
                reccomendation.ReccommendedFood = dietSection;

                //Set the heading
                var activitySection = "Activity \r\n\r\n";
                activitySection += "We reccommend HIIT (high intensity cardio) its a mix of bodyweight exercises" +
                    " and varied intensities which has been proven to be one of the quickest ways to burn bodyfat";
                reccomendation.ReccomendedActivity = activitySection;

                //Set the heading
                var exercisesSection = "Exercises \r\n\r\n";

                exercisesSection += "As your not loosing body fat at a rate your happy with lets try a" +
                    " less is more approach. \r\n\r\nStripping back to focusing on the tried and tested most effective exercises and then building week on week from them is " +
                    " key to finding what works and whats a waste of time! Ensure to just focus on these for 2 weeks\r\n\r\n";

                var exercises = new List<Exercise>();


                if (Fitness != FitnessLevel.Advanced)
                    exercises = new List<Exercise>() { ViewModel.GetSingleExericise("high knees"), ViewModel.GetSingleExericise("sqaut jumps"),
                     ViewModel.GetSingleExericise("close grip push ups")};
                exercises = new List<Exercise>() { ViewModel.GetSingleExericise("burpes"), ViewModel.GetSingleExericise("mountain climbers"),
                     ViewModel.GetSingleExericise("lunge walks")};



                //add them to the string
                foreach (var i in exercises)
                    exercisesSection += " " + i.Name + " - Rating: " + i.Popularity.ToString().Substring(0, 3) + "\r\n\r\n";

                reccomendation.ReccomendedExercises = exercisesSection;



                //Trainer advice should include training techniques, frequencies, tips
                var trainerAdvice = "Expert Tips \r\n\r\n";

                trainerAdvice += "1. Your aim should be to do each exercise for " + workingTime + " seconds, followed by " + restingTime +
                  " seconds of rest. \r\n\r\n2. Circuit the exercises if you wish or complete 3 sets of each!";
                trainerAdvice += "\r\n\r\n3. Once that becomes too easy, exercise for " + (workingTime + 10) + " seconds  and rest for " + (restingTime - 10) +
                    " or visa versa if too difficult ";

                reccomendation.ExpertNote = trainerAdvice;
            }

            await context.PostAsync(reccomendation.OpeningMessage);
            await context.PostAsync(reccomendation.ReccomendedActivity);
            await context.PostAsync(reccomendation.ReccomendedExercises);
            await context.PostAsync(reccomendation.ExpertNote);
            await context.PostAsync(reccomendation.ReccommendedFood);

            PromptDialog.Choice(
            context: context,
            resume: DialogOptionsComplete,
            options: new List<string>() { "Ask question", "How to do exercises?" },
            prompt: "Anything else?",
            retry: "Selected plan not avilable . Please try again.",
            promptStyle: PromptStyle.Auto);

        }

        [LuisIntent("IWantBigger")]
        private async Task IWantBigger(IDialogContext context, IAwaitable<object> activity, LuisResult result)
        {
            if (result.Query.ToLower().Trim() == "go back" || result.Query.ToLower().Trim().Contains("go back"))
            {
                await GoBack(context);
                return;
            }

            //variables
            var typingMsg = context.MakeMessage();
            typingMsg.Type = ActivityTypes.Typing;

            //wait for 2 seconds
            Thread.Sleep(1000);

            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(3000);



            //send the progress update
            var msg = context.MakeMessage();
            msg.Attachments = new List<Attachment>() { new HeroCard(images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\stage_tailor.jpg") }).ToAttachment() };
            await context.PostAsync(msg);

            //wait for 2 seconds
            Thread.Sleep(2000);
            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(1000);


            var muscleGroupEntity = new EntityRecommendation();

            if (result != null)
            {
                var entities = new List<EntityRecommendation>(result.Entities);

                if (entities.Where((entity) => entity.Type == "MuscleGroup").Count() > 0)
                {
                    muscleGroupEntity = entities.Where((entity) => entity.Type == "MuscleGroup").First();
                    SelectedMuscleGroup = ViewModel.GetMuscleGroup(muscleGroupEntity.Entity);
                }

                if (entities.Any((entitiy) => entitiy.Type == "MuscleGroup") && !WantBiggerDialogHasRan)
                {
                    if (SelectedMuscleGroup != null)
                    {
                        if (SelectedMuscleGroup.Name.Last().ToString().ToLower() == "s")
                        {
                            await context.PostAsync("We see you want bigger " + SelectedMuscleGroup.Name +
                                                  ". \r\n\r\n Let us tailor your answer, we just need abit more info..");
                        }
                        else
                        {
                            await context.PostAsync("We see you want a bigger " + SelectedMuscleGroup.Name +
                                                  ". \r\n\r\n Let us tailor your answer, we just need abit more info..");
                        }
                    }
                    else
                    {
                        await context.PostAsync("We see you want to get bigger." +
                                              "\r\n\r\n Let us tailor your answer, we just need abit more info..");
                    }

                    context.Call<object>(new WantBiggerDialog(muscleGroupEntity.Entity), WantBiggerDialogComplete);
                    return;
                }
            }

            if (!WantBiggerDialogHasRan)
            {
                if (SelectedMuscleGroup != null)
                {
                    if (SelectedMuscleGroup.Name.Last().ToString().ToLower() == "s")
                    {
                        await context.PostAsync("We see you want bigger " + SelectedMuscleGroup.Name +
                                              ". \r\n\r\n Let us tailor your answer, we just need abit more info..");
                    }
                    else
                    {
                        await context.PostAsync("We see you want a bigger " + SelectedMuscleGroup.Name +
                                              ". \r\n\r\n Let us tailor your answer, we just need abit more info..");
                    }
                }
                else
                {
                    await context.PostAsync("We see you want to get bigger." +
                                          "\r\n\r\n Let us tailor your answer, we just need abit more info..");
                }
                context.Call<object>(new WantBiggerDialog(), WantBiggerDialogComplete);
                return;
            }

            var activity1 = await activity;
            var iWantBiggerDialog = activity1 as WantBiggerDialog;

            var Gender = ViewModel.SetGender(iWantBiggerDialog.Gender);
            var Fitness = ViewModel.SetFitnessLevel(iWantBiggerDialog.Fitness);
            var GymHome = ViewModel.SetGymOrHome(iWantBiggerDialog.GymHome);
            var Diet = iWantBiggerDialog.Diet;
            var MuscleGroup = iWantBiggerDialog.MuscleGroup;

            //if its null aka LUIS never picked up a MG on first run, the dialog would have asked for the MG so now we save the MG if we dont have one.
            SelectedMuscleGroup = SelectedMuscleGroup ?? ViewModel.GetMuscleGroup(MuscleGroup);
            //If the MG wasnt null, aka we had one, we ensure its the same that went into the above dialog and not one from a previous method/intent.
            //if (!SelectedMuscleGroup.Synonyms.Any(i => i == MuscleGroup))
            //{
            //    SelectedMuscleGroup = ViewModel.GetMuscleGroup(MuscleGroup);
            //}

            var restingTime = 0;

            switch (Fitness.ToString())
            {
                case "Advanced":
                    {
                        restingTime = 90;
                    }
                    break;
                case "Intermediate":
                    {
                        restingTime = 90;
                    }
                    break;
                case "Beginner":
                    {
                        restingTime = 120;
                    }
                    break;
            }


            var reccomendation = new Reccomendation();

            //Set the heading
            var dietSection = (Diet != "Very good") ? "Most Important - Diet \U0001F34F \r\n\r\n " : "Diet \U0001F34F \r\n\r\n";
            dietSection += "Argubly more important then the training because you can only put weight on by consuming more than you burn. That said you still need to eat wisely"
          + "\r\n\r\n Eat smaller and more often e.g. instead of 3 big meals have 2 big and 2 small"
            + " \r\n\r\n You'll need to consume about 1 gram of protein per pound of bodyweight daily, we can work this out for you just say 'whats my protein'" +
            "\r\n\r\n Stick to good carbs such as brown rice, brown pasta, sweet pototoe etc, meats stick to chicken, turkey breast, beef, ensure you get enough veg/fruit";
            reccomendation.ReccommendedFood = dietSection;

            //Set the heading

            var activitySection = (Gender == AppPrototype.Models.Enums.Gender.M) ? "Activity \U0001F3CB \r\n\r\n" :
            "Activity \U0001F3CB \r\n\r\n";

            activitySection += (SelectedMuscleGroup.Size == Size.Large) ? "We reccommend a mix of weight training mixed with bodyweight training, The reasoning for this is muscle grows best under pressure. Dont worry if your at home " +
            "compound bodyweight exercises can still produce the same effects will help speed things up \U0001F4A8" : "Using extra resistance might be useful, but at minimum you should be" +
            " using compound bodyweight exercises to push your muscles to a high level promoting growth. \U0001F613";

            reccomendation.ReccomendedActivity = activitySection;

            //Set the heading
            var exercisesSection = "Exercises \U0001F4AA \r\n\r\n";
            //Mg specific text
            switch (MuscleGroup.ToLower())
            {
                case "shoulders":
                    {
                        exercisesSection += "Bigger shoulders will give you the illusion of being bigger than you are so good choice! \U0001F609 You'll want to add focus to the side delts (the shoulders are made up of 3 parts front,side,rear)" +
                            "the sides give the width. ";
                    };
                    break;
                case "back":
                    {
                        exercisesSection += " Back is arugbably one of the biggest set of muscle  groups its comprised of smaller groups which all work together, the lats give the width so pull ups/downs will be included for width.";
                    };
                    break;
                case "chest":
                    { exercisesSection += "Chest will give you the full look from the front, try to mix it up so sometimes start on lower chest, other times" +
                            "upper chest"; };
                    break;
                case "quads":
                    { exercisesSection += (Gender == AppPrototype.Models.Enums.Gender.M) ? "As your guy, its tempting to focus on upper body but if you don't train your qauds you'll look out of kilt" :
                            "For women strong qauds give a full look and make your waist appear smaller."; };
                    break;

            }
            //Set the exercises

            if (ViewModel.GetExericises(SelectedMuscleGroup, WeightGoal.GainMuscle, Fitness, GymHome).Count() > 6)
            {
                exercisesSection += "\r\n\r\nPick a handful of these exercises!\r\n\r\n";
            }

            foreach (var i in ViewModel.GetExericises(SelectedMuscleGroup,
                WeightGoal.GainMuscle, Fitness, GymHome))
                exercisesSection += " " + i.Name + " - Rating: " + i.Popularity.ToString().Substring(0, 3) + "\r\n\r\n";

            reccomendation.ReccomendedExercises = exercisesSection;

            //Trainer advice should include training techniques, frequencies, tips
            var trainerAdvice = "Expert Tips \U0001F913 \r\n\r\n";            //this method returns the biggest comp ex
            trainerAdvice += (GymHome.ToString() == "Gym") ? "1. 3 sets of the each exercise for 6-8 reps then drop set it (half the weight and do a final set). Rest for " + restingTime + " seconds between sets"
            : "1. 3 sets of the each exercise for as many reps as you can. Rest for " + restingTime + " seconds between sets";

            trainerAdvice += "\r\n\r\n 2. Superset 2 exercises, meaning do one set of X then immediately do one set of Y then rest and " +
                "do this for 3 sets, this doesn't have to be all of the time but its good to bring in and out to shock the body.";
            trainerAdvice += "\r\n\r\n3. Finally for growth training bigger muscle groups every few days is key to faster results";
            reccomendation.ExpertNote = trainerAdvice;


            var exMsg = context.MakeMessage();
            exMsg.Attachments = new List<Attachment>() { GetExpertCard("Andy", muscleGroupEntity.Entity) };

            //send the expert card
            //await context.PostAsync(msg);
            //send the reccomendation in parts
            //await context.PostAsync(reccomendation.ToString());


            var card = new HeroCard(title: "You ready to train? \U0001F60F", images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\Shoulder Press.gif") }).ToAttachment();
            var pic = context.MakeMessage();
            pic.Attachments = new List<Attachment>() { card };

            await context.PostAsync("Heres the answer you've been awaiting for..");

            await context.PostAsync("1/4 \r\n\r\n" + reccomendation.ReccomendedActivity);
            await context.PostAsync("2/4 \r\n\r\n" + reccomendation.ReccomendedExercises);
            await context.PostAsync(pic);
            await context.PostAsync("3/4 \r\n\r\n" + reccomendation.ReccommendedFood);
            await context.PostAsync("4/4 \r\n\r\n" + reccomendation.ExpertNote);


            //send the after recc options
            context.Call<object>(new WantBiggerDialogOptions("Andy", iWantBiggerDialog.MuscleGroup), WantBiggerDialogOptionsComplete);
        }

        [LuisIntent("IWantSmaller")]
        private async Task IWantSmaller(IDialogContext context, IAwaitable<object> activity, LuisResult result)
        {

            //variables
            var typingMsg = context.MakeMessage();
            typingMsg.Type = ActivityTypes.Typing;

            //wait for 2 seconds
            Thread.Sleep(1000);

            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(3000);



            //send the progress update
            var msg = context.MakeMessage();
            msg.Attachments = new List<Attachment>() { new HeroCard(images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\stage_tailor.jpg") }).ToAttachment() };
            await context.PostAsync(msg);

            //wait for 2 seconds
            Thread.Sleep(2000);
            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(1000);

            var muscleGroupEntity = new EntityRecommendation();

            if (!WantSmallerMethodDone)
            {

                if (result != null)
                {
                    var entities = new List<EntityRecommendation>(result.Entities);

                    if (entities.Where((entity) => entity.Type == "MuscleGroup").Count() > 0)
                    {
                        muscleGroupEntity = entities.Where((entity) => entity.Type == "MuscleGroup").First();
                        SelectedMuscleGroup = ViewModel.GetMuscleGroup(muscleGroupEntity.Entity);
                    }


                    if (muscleGroupEntity != null && !WantToLoseDialogHasRan)
                    {
                        context.Call<object>(new WantToLoseDialog(muscleGroupEntity.Entity), WantToLoseDialogComplete);
                        SelectedMuscleGroup = ViewModel.GetMuscleGroup(muscleGroupEntity.Entity);
                        return;
                    }
                }

                if (!WantToLoseDialogHasRan)
                {
                    context.Call<object>(new WantToLoseDialog(), WantToLoseDialogComplete);
                    return;
                }

                var activity1 = await activity;
                var wantToLoseDialog = activity1 as WantToLoseDialog;

                var Gender = ViewModel.SetGender(wantToLoseDialog.Gender);
                var Fitness = ViewModel.SetFitnessLevel(wantToLoseDialog.Fitness);
                var GymHome = ViewModel.SetGymOrHome(wantToLoseDialog.GymHome);
                var Diet = wantToLoseDialog.Diet;
                var MuscleGroup = wantToLoseDialog.MuscleGroup;

                //if its null aka LUIS never picked up a MG on first run, the dialog would have asked for the MG so now we save the MG if we dont have one.
                SelectedMuscleGroup = SelectedMuscleGroup ?? ViewModel.GetMuscleGroup(MuscleGroup);
                //If the MG wasnt null, aka we had one, we ensure its the same that went into the above dialog and not one from a previous method/intent.
                //if (!SelectedMuscleGroup.Synonyms.Any(i => i == MuscleGroup.ToLower()))
                //{
                //    SelectedMuscleGroup = ViewModel.GetMuscleGroup(MuscleGroup);
                //}

                //var possibleFoods = FoodService.GetFoods(GainMuscle);
                //var possibleExercises = ExerciseService.GetExercises(GainMuscle, FitLevel, GymHome, Gender);

                var reccomendation = new Reccomendation();

                //Set the heading
                var dietSection = (Diet != "Very good") ? "Most Important - Diet \U0001F34F \r\n\r\n " : "Diet \U0001F34F \r\n\r\n";
                dietSection += "Argubly more important then the training because you can only loose weight and tone up by burning less than you consume. " +
                    "\r\n\r\nThat said you still need to eat wisely, skipping breakfast is a big no go when trying to loose weight, eating smaller and more often e.g. " +
                    "instead of 3 big meals have 2 big and 2 small. \r\n\r\nToo help remove temptation of bad food try to plan your meals in advance" +
                    " \r\n\r\n Aim to consume about 1 gram of protein per pound of bodyweight daily, we can work this out for you just say 'whats my protein' (dont worry you don't need any supplements getting this from food is more than enough)" +
            "\r\n\r\n Meals should be based around good carbs such as brown rice, brown pasta, sweet pototoe etc. (Don't be afraid to add your own spin!) The meats you want to aim for are chicken, turkey breast, beef, ensure you get enough veg/fruit"; ;// /*, want us to do it for you?*/" +
                                                                                                                                                                                                                                                             // "just say 'plan my meals or make me a meal plan \r\n List of Food items";

                reccomendation.ReccommendedFood = dietSection;

                //Set the heading
                var activitySection = "Activity \U0001F3CB \r\n\r\n";
                activitySection += "We reccommend HIIT (high intensity cardio) its a mix of bodyweight exercises" +
                        " at varied intensities which has been proven to be one of the quickest ways to burn body fat";

                reccomendation.ReccomendedActivity = activitySection;

                //Set the heading
                var exercisesSection = "Exercises \U0001F4AA \r\n\r\n";
                //Muscle group specific
                switch (SelectedMuscleGroup.Name.ToLower().Trim())
                {
                    case "core":
                        {
                            exercisesSection += "Loosing stomach fat will give you an athletic build, to achieve this be prepared to work hard and be consistant" +
                                "the ab muscles need to be trained at minimum 3 times per week for the quickest results, but remember thers not point having a " +
                                "good set of abs with a layer of fat above hense why the diet plays such an important role! \r\n\r\n";
                        };
                        break;
                    case "back":
                        {
                            exercisesSection += " Back is arugbably one of the biggest set of muscle  groups its comprised of smaller groups which all work together, the lats give the width so pull ups/downs will be included for width.\r\n\r\n";
                        };
                        break;
                    case "chest":
                        { exercisesSection += "";
                        };
                        break;
                    case "quads":
                        { exercisesSection += "";
                        };
                        break;
                }

                //Set the exercises
                foreach (var i in ViewModel.GetExericises(SelectedMuscleGroup,
                    WeightGoal.LooseFat, Fitness, GymHome))
                    exercisesSection += " " + i.Name + " - Rating: " + i.Popularity.ToString().Substring(0, 3) + "\r\n\r\n";

                reccomendation.ReccomendedExercises = exercisesSection;

                var workingTime = 0;
                var restingTime = 0;

                switch (Fitness.ToString())
                {
                    case "Advanced":
                        { workingTime = 40; restingTime = 20;
                        }
                        break;
                    case "Intermediate":
                        { workingTime = 30; restingTime = 30;
                        }
                        break;
                    case "Beginner":
                        { workingTime = 20; restingTime = 40;
                        }
                        break;
                }

                //Trainer advice should include training techniques, frequencies, tips //Set heading
                var trainerAdvice = "Expert Tips \U0001F913 \r\n\r\n";
                trainerAdvice += "1. Your aim should be to do each exercise for " + workingTime + " seconds, followed by " + restingTime +
                    " seconds of rest. \r\n\r\n 3. Circuit the exercises if you wish or complete 3 sets of each!";
                trainerAdvice += "\r\n\r\n 2. Once that becomes too easy, exercise for " + (workingTime + 10) + " seconds  and rest for " + (restingTime - 10) +
                    " or visa versa if too difficult ";

                reccomendation.ExpertNote = trainerAdvice;

                var exMsg = context.MakeMessage();
                exMsg.Attachments = new List<Attachment>() { GetExpertCard("test", SelectedMuscleGroup.Name) };

                //send the expert card
                // await context.PostAsync(msg);

                //send the reccomendation in parts
                //await context.PostAsync(reccomendation.ToString());

                await context.PostAsync(reccomendation.ReccomendedActivity);
                await context.PostAsync(reccomendation.ReccomendedExercises);
                await context.PostAsync(reccomendation.ReccommendedFood);
                await context.PostAsync(reccomendation.ExpertNote);

                //send the after recc options
                //context.Call<object>(new WantSmallerDialogOptions("test"), WantSmallerDialogOptionsComplete);
                PromptDialog.Choice(
context: context,
resume: DialogOptionsComplete,
options: new List<string>() { "Ask question", "How to do exercises?" },
prompt: "Anything else?",
retry: "Selected plan not avilable . Please try again.",
promptStyle: PromptStyle.Auto);
            }
        }

        [LuisIntent("BestCalorieBurner")]
        public async Task BestCalorieBurner(IDialogContext context, IAwaitable<object> activity, LuisResult result = null)
        {

            //variables
            var typingMsg = context.MakeMessage();
            typingMsg.Type = ActivityTypes.Typing;

            //wait for 2 seconds
            Thread.Sleep(1000);

            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(3000);



            //send the progress update
            var msg = context.MakeMessage();
            msg.Attachments = new List<Attachment>() { new HeroCard(images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\stage_tailor.jpg") }).ToAttachment() };
            await context.PostAsync(msg);

            //wait for 2 seconds
            Thread.Sleep(2000);
            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(1000);

            //aka user said go back
            if (result.Query == null && activity == null)
            {
                //use the last luis result entities
                await BestCalorieBurner(context, null, new LuisResult() { Query = "" });
            }
            else
            {
                //use the current ones
            }


            //LastLuisResult = new MyLUISResult();

            // UpdateStage(context, Stage.Tailor);
            //UpdateChanges("BestCalorieBurner", result);
            LastCalledLuisMethod = "BestCalorieBurner";

            await context.PostAsync(
            "It depends if you want to walk on the spot on a rattly machine for an hour a week. \U0001F644");
            await context.PostAsync(
            "\r\n\r\nOn a serious note traditional cardio isn't the way forward however its cousin HIIT \U0001F4A8 (High Intensity Interval Cardio) is proven to burn fat" +
            " and tone your body up at a much faster rate. \r\n\r\nWhats the catch?\r\n\r\n...The intense part! its not as easy as just walking or peddling\r\n\r\n");
            await context.PostAsync(
            "\r\n\r\nNext up is weight lifting as theirs so many different exercises you can do which will get your heart rate up and keep it their!\r\n\r\n"
            );
            await context.PostAsync(
            "\r\n\r\nWhat we reccomend? If your up to going to the gym do mix of both, if your at home HIIT will be more than enough to get you to where you want to be quick! \r\n\r\n ");


            //PromptDialog.Choice(
            // context: context,
            // resume: BestCalorieBurnerComplete,
            // options: new List<string>() { "Very fit","Average","Unfit" },
            // prompt: "Whats your current fitness level?",
            // retry: "Selected plan not avilable . Please try again.",
            // promptStyle: PromptStyle.Auto);
        }


        [LuisIntent("BestTimeOfDayToWorkOut")]
        private async Task BestTimeOfDayToWorkOut(IDialogContext context, LuisResult result)
        {
            //variables
            var typingMsg = context.MakeMessage();
            typingMsg.Type = ActivityTypes.Typing;

            //wait for 2 seconds
            Thread.Sleep(1000);

            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(3000);



            //send the progress update
            var msg = context.MakeMessage();
            msg.Attachments = new List<Attachment>() { new HeroCard(images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\stage_tailor.jpg") }).ToAttachment() };
            await context.PostAsync(msg);

            //wait for 2 seconds
            Thread.Sleep(2000);
            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(1000);


            await context.PostAsync("I don't know you tell me?");
            //sleep for 1 second
            await context.PostAsync(
                "Seriously, It does matter, Imagine Steve the builder has just been hammering away for 9 hours straight, 5 minutes later hes in the gym \r\n\r\n VS \r\n\r\nWell rested saturday morning steve ...Who you backing to be more impressive? \r\n\r\n"
               );
            await context.PostAsync(
                "1. Do it when you can concentrate on it (if your house is busy do it when its quiet). \r\n\r\n 2. Early riser? do it before work, KO'd after work? do it before work.\r\n\r\n" +
                " 3. If your deffinately not a morning person then its probably best you stick to lunch time or later in the day, maybe half an hour after work? \r\n\r\n4. If your usually very hungry after work eat " +
                "then wait an hour before going, you'll be distracted if your hungry  " +
                "\r\n\r\n5. Got a busy social/family life? Training after work might not be for you as you'll be tempted to skip sessions often."
                );

            await context.PostAsync("Weigh up those factors before you make a decision ... Try something, doesn't work? Try something else!");

            //here we could advertise something? Or just send the normal Dialog options question

        }


        [LuisIntent("IveGotNoTimeOrIDontLikeToWorkOut")]
        private async Task IveGotNoTimeOrIDontLikeToWorkOut(IDialogContext context, LuisResult result)
        {

            //variables
            var typingMsg = context.MakeMessage();
            typingMsg.Type = ActivityTypes.Typing;

            //wait for 2 seconds
            Thread.Sleep(1000);

            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(3000);



            //send the progress update
            var msg = context.MakeMessage();
            msg.Attachments = new List<Attachment>() { new HeroCard(images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\stage_tailor.jpg") }).ToAttachment() };
            await context.PostAsync(msg);

            //wait for 2 seconds
            Thread.Sleep(2000);
            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(1000);


            await context.PostAsync("\U0001F552\U0001F552\U0001F552\U0001F552\U0001F552");

            await context.PostAsync(
            "\r\n\r\nFair enough everyone has their likes and dislikes. At least you know its neccessary, even if it does bore you senseless.\r\n\r\n"
            );
            await context.PostAsync(
            "\r\n\r\nWe reccomend you do 15 minute sessions 3 times per week. Yes just 45 minutes per week will help you keep you in good health and if done with vigour will get you the results you want.\r\n\r\n"
            );

            await context.PostAsync(
            "\r\n\r\nDon't worry you'll be doing these at home & and with just bodyweight, \r\n\r\n Tip: As its only 15 mins why not get it out the way when you get out of bed? Tuesday, Thursday and Friday?\r\n\r\n"
            );

            PromptDialog.Choice(
            context: context,
            resume: IveGotNoTimeOrIDontLikeToWorkOutComplete,
            options: new List<string>() { "Very fit", "Average", "Un fit" },
            prompt: "Which best describes you?",
            retry: "Please try again.",
            attempts: 3,
            promptStyle: PromptStyle.Auto);
        }


        [LuisIntent("HowLongShouldIWorkOutFor")]
        private async Task HowLongShouldIWorkOutFor(IDialogContext context, LuisResult result)
        {
            //variables
            var typingMsg = context.MakeMessage();
            typingMsg.Type = ActivityTypes.Typing;

            //wait for 2 seconds
            Thread.Sleep(1000);

            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(3000);



            //send the progress update
            var msg = context.MakeMessage();
            msg.Attachments = new List<Attachment>() { new HeroCard(images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\stage_tailor.jpg") }).ToAttachment() };
            await context.PostAsync(msg);

            //wait for 2 seconds
            Thread.Sleep(2000);
            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(1000);


            await context.PostAsync(
            "It depends on your activity. \r\n\r\nIf your weight training your sessions should range from 20 to 60 minutes, " +
            "if your doing bodyweight or HIIT (high intensity interval cardio) exercises, your sessions should be around 15 to 40 minutes"
            );

            PromptDialog.Choice(
            context: context,
            resume: HowLongShouldIWorkOutForComplete,
            options: new List<string>() { "Gain Muscle", "ToneUp", "Loose Fat" },
            prompt: "Tell us your weight goal we'll get more specific",
            retry: "Please try again.",
            attempts: 3,
            promptStyle: PromptStyle.Auto);
        }


        [LuisIntent("HowManyCaloriesWillIBurn")]
        private async Task HowManyCaloriesWillIBurn(IDialogContext context, LuisResult result)
        {

            //variables
            var typingMsg = context.MakeMessage();
            typingMsg.Type = ActivityTypes.Typing;

            //wait for 2 seconds
            Thread.Sleep(1000);

            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(3000);



            //send the progress update
            var msg = context.MakeMessage();
            msg.Attachments = new List<Attachment>() { new HeroCard(images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\stage_tailor.jpg") }).ToAttachment() };
            await context.PostAsync(msg);

            //wait for 2 seconds
            Thread.Sleep(2000);
            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(1000);


            await context.PostAsync(
                "\r\n\r\nYou'll need a heart rate monitor and a scientist. \U0001F937"
                );
            await context.PostAsync(
        "\r\n\r\nNo but it is almost impossible to tell theres too many variables to comprehend  (guess: a couple hundred if you worked hard)" +


        "\r\n\r\nYeah a packet of crisps and a milkshake can wipe out a good workout like it never even happened :( \r\n\r\n"
        );

            await context.PostAsync(
            "\r\n\r\nIts not all bad news! \r\n\r\nIf your doing 'HIIT' (high intensitiy interval cardio) (the hard version of normal cardio) \r\n\r\n" +
            "'Even when your sitting down 2 hours after the session your bodys still burning calories! excited much? :D \r\n\r\nTake away, if your goals to burn as much as " +
            "possible think HIIT, and weights not your usuall cardio."
            );

        }

        //Food Intents
        [LuisIntent("HowMuchXInThisItem")]
        private async Task HowMuchXInThisItem(IDialogContext context, LuisResult result)
        {
            var foodEntities = new List<EntityRecommendation>(result.Entities.Where(i => i.Type == "Food"));

            if (foodEntities == null || foodEntities.Count() == 0)
            {
                await context.PostAsync("Sorry my food knowledge is a little low but im learning, i know what an egg is? \U0001F913 ");
                return;
            }

            var foodDatabase = new FoodFinder().FoodBank;
            var usersFoodList = new List<FoodItem>();

            foreach (var i in foodEntities)
            {
                //try to find the matching item in the database and save it to add to the foodItems list.
                var currentFoodItem = foodDatabase.FirstOrDefault(item => item.Name.ToLower().Trim() == i.Entity.ToLower().Trim());
                if (currentFoodItem != null)
                {
                    usersFoodList.Add(currentFoodItem);
                }
                else
                {
                    //if we cant match the food item based on the foodEntity name provided by luis, e.g. we dont have it in our db 
                    //so if Luis recognises it but its not in our database we cant do anything. We can only do somethin if we know what the item is.
                }
            }

            if (usersFoodList == null || usersFoodList.Count == 0)
            {
                await context.PostAsync("we're struggling to find the item please could you rephrase it?");
                return;
            }

            //create the report and set simple values
            var report = new FoodReport()
            {
                FoodItems = usersFoodList,
               // TotalCalories = usersFoodList.Sum(i => i.Calories),
                TotalGramsOfFat = Convert.ToInt32(usersFoodList.Sum(i => i.GramsOfFat)),
                TotalGramsOfProtein = Convert.ToInt32(usersFoodList.Sum(i => i.GramsOfProtein))
            };

            //what do i want to do? What do they want to see? They want to know how much protein is in the item and they'd probs like us to tell them whether thats alot or not.
            //what if theres a few items if there is then its probably classed a a meal as bread slice x2 plus ham adds 20g (high) but 1 breadslice is only 5 (low)
            //with this i should sum the group of the items and see if its high or not, aka 18+ grams is high 12-18 medium 0-12 is low after we provide the report we could
            //then say do you want abit more e.g. a mini meal plan? want to know the best sources of protein? 

            //work out if high in bad fat
            foreach (var item in report.FoodItems)
            {
                //if (item.GramsOfSaturatedFat >= (item.TotalMixGrams * 0.05))
                //{
                //    report.FatLevel = MacroLevel.High;
                //}
            }

            //work out if high in protein
            //if (report.TotalGramsOfProtein >= 18)
            //    report.ProteinLevel = MacroLevel.High;
            //if (report.TotalGramsOfProtein <= 17 && report.TotalGramsOfProtein >= 12)
            //    report.ProteinLevel = MacroLevel.Average;
            //if (report.TotalGramsOfProtein < 12)
            //    report.ProteinLevel = MacroLevel.Low;

            ////work out if high in calories
            //if (report.TotalCalories >= 950)
            //    report.CalorieLevel = MacroLevel.High;
            //if (report.TotalCalories >= 500 && report.TotalCalories <= 949)
            //    report.CalorieLevel = MacroLevel.Average;
            //if (report.TotalCalories < 500)
            //    report.CalorieLevel = MacroLevel.Low;


            //Set the opening message the user sees
            var openingTopic = "";

            if (result.Query.Contains("protein"))
                openingTopic = "protein";
            if (result.Query.Contains("carbohydrates") || result.Query.Contains("carbohydrate")
                || result.Query.Contains("carbs") || result.Query.Contains("carb"))
                openingTopic = "carbs";
            if (result.Query.Contains("fat"))
                openingTopic = "fat";
            if (result.Query.Contains("calories") || result.Query.Contains("cals"))
                openingTopic = "calories";

            //this is based on what the user asked so if there message contained protein aka there asking how much protein then we start by tellin them the protein visa versa
            switch (openingTopic)
            {
                case "protein":
                    {
                        report.OpeningMessage = (report.FoodItems.Count() > 1) ? "this meal contains " + report.TotalGramsOfProtein + " grams of protein " +
                            "and is " + report.TotalCalories + " calories" : "this item contains " + report.TotalGramsOfProtein + " grams of protein " +
                            "and is " + report.TotalCalories + " calories";

                        //if its low in protein why not offer some higher suggestions? 

                    }
                    break;

                case "fat":
                    {
                        report.OpeningMessage = (report.FoodItems.Count() > 1) ? "this meal contains " + report.TotalGramsOfFat + " grams of fat " +
                            "and is " + report.TotalCalories + " calories" : "this item contains " + report.TotalGramsOfProtein + " grams of fat " +
                            "and is " + report.TotalCalories + " calories";

                        //if high in fat then find and offer products of the same type which aren't high in fat

                    }
                    break;

                case "calories":
                    {
                        report.OpeningMessage = (report.FoodItems.Count() > 1) ? "this meal is " + report.TotalCalories + " calories" :
                        "this item is " + report.TotalCalories + " calories";
                    }
                    break;
                default:
                    {
                        report.OpeningMessage = (report.FoodItems.Count() > 1) ? "this meal is " + report.TotalCalories + " calories " +
                            "and has " + report.TotalGramsOfProtein + " grams of protein" :
                        "this item is " + report.TotalCalories + " calories " +
                            "and has " + report.TotalGramsOfProtein + " grams of protein";
                    }
                    break;
            }


            //if (report.FatLevel == MacroLevel.High)
            //{
            //    report.ExpertAdvice += "Fat Warning X\r\n\r\n";

            //    report.ExpertAdvice += (usersFoodList.Count > 1) ? "This meal is high in saturated fat. " +
            //        " Consuming too much saturated fat on a regular basis will not only limit your progress but is bad for your health!" :

            //        "This is high in saturated fat. Consuming too much saturated fat on a regular basis will not only limit your progress but is bad for your health!";
            //}

            //Check the protein level
            //if (report.ProteinLevel == MacroLevel.Low)
            //{
            //    if (report.CalorieLevel != MacroLevel.Low)
            //    {
            //        report.ExpertAdvice += "Protein Alert!\r\n\r\n";
            //        report.ExpertAdvice += (report.FoodItems.Count > 1) ? "This meal isn't giving you much protein so make sure your other daily meals " +
            //            "have enough, if in doubt just ask us! " : "this item isn't giving you much protein so make sure your other daily meals " +
            //            "have enough, if in doubt just ask us! ";

            //        //Here i should say why not consider ..... this item 
            //    }
            //}

            //Add similar products

            var returnString = report.OpeningMessage + "\r\n\r\n";

            if (!string.IsNullOrWhiteSpace(report.ExpertAdvice))
                returnString += report.ExpertAdvice;

            await context.PostAsync(returnString);
        }

        [LuisIntent("WhatsMyProteinOrCals")]
        private async Task WhatsMyProteinOrCals(IDialogContext context, LuisResult result)
        {
            //variables
            var typingMsg = context.MakeMessage();
            typingMsg.Type = ActivityTypes.Typing;

            //wait for 2 seconds
            Thread.Sleep(1000);

            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(3000);



            //send the progress update
            var msg = context.MakeMessage();
            msg.Attachments = new List<Attachment>() { new HeroCard(images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\stage_tailor.jpg") }).ToAttachment() };
            await context.PostAsync(msg);

            //wait for 2 seconds
            Thread.Sleep(2000);
            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(1000);

            var entities = new List<EntityRecommendation>(result.Entities);
            var goalEntity = new EntityRecommendation();

            if (entities.Where((entity) => entity.Type == "WeightGoal").Count() > 0)
            {
                goalEntity = entities.Where((entity) => entity.Type == "WeightGoal").First();
                context.Call<object>(new GetUserWeightDialog(goalEntity.Entity ?? ""), WhatsMyProteinOrCalsComplete);
            }
            context.Call<object>(new GetUserWeightDialog(""), WhatsMyProteinOrCalsComplete);
        }

        [LuisIntent("DoingTooLittleTooMuch")]
        private async Task DoingTooLittleTooMuch(IDialogContext context, LuisResult result)
        {
            //variables
            var typingMsg = context.MakeMessage();
            typingMsg.Type = ActivityTypes.Typing;

            //wait for 2 seconds
            Thread.Sleep(1000);

            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(3000);



            //send the progress update
            var msg = context.MakeMessage();
            msg.Attachments = new List<Attachment>() { new HeroCard(images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\stage_tailor.jpg") }).ToAttachment() };
            await context.PostAsync(msg);

            //wait for 2 seconds
            Thread.Sleep(2000);
            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(1000);


            await context.PostAsync(
        "Before making any changes consider if your getting enough sleep and eating a balanced diet? If not improve these before anything else."
        );
            await context.PostAsync(
                "Too much?\r\n\r\n" +
        "Are you becoming irritable easier than before or is your motivation is whaning?. \r\n\r\nIf so maybe you need to cut back a day or two. Or" +
        " try to cut your sessions down by keeping only the bigger exercises" +
        "\r\n\r\nRemember sometimes its not just your body that needs the break, its your mind as well"
        );
            //    await context.PostAsync(
            //"How to cut back? ...\r\n\r\n Is their a session you dread or an unimportant session? Remove it! (you can always add it back in later on)" +
            //"Half your session times(just focus on the big exercises! \r\n\r\n Hold On? your telling me to stop working out!?!?!? r\n\r\n We're saying make the workout as least intrusive as possible, or keep it how it is" +
            //"and run the risk of giving up all together."
            //);

            await context.PostAsync(
                "Not enough? \r\n\r\nAren't your muscles sore anymore or do you feel like you could do more once your finished?" +
            "\r\n\r\nChances are your not pushing hard enough. Why not set yourself a mini goal for every session? \r\n\r\nE.g. 'i want to do 50 press ups', or a '50KG squat' and grade yourself every session." +
            " Our plans have this built in so every session your working towards hitting a goal");

        }

        [LuisIntent("IveGotAnInjury")]
        private async Task IveGotAnInjury(IDialogContext context, IAwaitable<object> activity, LuisResult result)
        {
            //variables
            var typingMsg = context.MakeMessage();
            typingMsg.Type = ActivityTypes.Typing;

            //wait for 2 seconds
            Thread.Sleep(1000);

            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(3000);



            //send the progress update
            var msg = context.MakeMessage();
            msg.Attachments = new List<Attachment>() { new HeroCard(images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\stage_tailor.jpg") }).ToAttachment() };
            await context.PostAsync(msg);

            //wait for 2 seconds
            Thread.Sleep(2000);
            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(1000);

            var muscleGroupEntity = new EntityRecommendation();

            if (result != null)
            {
                var entities = new List<EntityRecommendation>(result.Entities);

                if (entities.Where((entity) => entity.Type == "MuscleGroup").Count() > 0)
                {
                    muscleGroupEntity = entities.Where((entity) => entity.Type == "MuscleGroup").First();
                    ViewModel.InjuredMuscleGroup = ViewModel.GetMuscleGroup(muscleGroupEntity.Entity);
                }
                if (entities.Where((entity) => entity.Type == "Joint").Count() > 0)
                {
                    muscleGroupEntity = entities.Where((entity) => entity.Type == "MuscleGroup").First();
                   // ViewModel.InjuredJoint = ViewModel.GetJoint(muscleGroupEntity.Entity);
                }


                //check if they provided a muscle group, joint or nothing?
                if (ViewModel.InjuredMuscleGroup != null && !IveGotAnInjuryDialogHasRan)
                {                       //im going to rename WantBiggerDialog to GetUserDataDialog as it can be shared with other methods
                    context.Call<object>(new InjuredMuscleGroupDialog(ViewModel.InjuredMuscleGroup.Name), IveGotAnInjuryDialogComplete);
                    return;
                }
                //if (ViewModel.InjuredJoint != null && !IveGotAnInjuryDialogHasRan)
                //{                       //im going to rename WantBiggerDialog to GetUserDataDialog as it can be shared with other methods
                //    context.Call<object>(new InjuredMuscleGroupDialog(null, ViewModel.InjuredJoint.Name), IveGotAnInjuryDialogComplete);
                //    return;
                //}
            }

            if (!IveGotAnInjuryDialogHasRan)
            {                           //rename i want bigger dialog to get user data
                context.Call<object>(new InjuredMuscleGroupDialog(), IveGotAnInjuryDialogComplete);
                return;
            }

            var activity1 = await activity;
            var iveGotAnInjuryDialog = activity1 as InjuredMuscleGroupDialog;

            var Gender = ViewModel.SetGender(iveGotAnInjuryDialog.Gender);
            var Fitness = ViewModel.SetFitnessLevel(iveGotAnInjuryDialog.Fitness);
            var GymHome = ViewModel.SetGymOrHome(iveGotAnInjuryDialog.GymHome);
            var Diet = iveGotAnInjuryDialog.Diet;
            var MuscleGroup = iveGotAnInjuryDialog.MuscleGroup;
            var Goal = ViewModel.SetWeightGoal(iveGotAnInjuryDialog.WeightGoal);

            if (!string.IsNullOrEmpty(MuscleGroup))
            {
                //if its null aka LUIS never picked up a MG on first run, the dialog would have asked for the MG so now we save the MG if we dont have one.
                ViewModel.InjuredMuscleGroup = ViewModel.InjuredMuscleGroup ?? ViewModel.GetMuscleGroup(MuscleGroup);
                //If the MG wasnt null, aka we had one, we ensure its the same that went into the above dialog and not one from a previous method/intent.
                //if (!ViewModel.InjuredMuscleGroup.Synonyms.Any(i => i == MuscleGroup))
                //{
                //    ViewModel.InjuredMuscleGroup = ViewModel.GetMuscleGroup(MuscleGroup);
                //}
            }

            //if SelectedMuscleGroup is still null but muscleGroupEntity is not null this means its a Joint injury.
            //What do we do in this circumstance? We could have a list of Joints?? 
            //we could see if the muscleGroupEntity is == to any of our Joints if it is then we know theyve injured a joint. 
            //and we say SelectedJoint??? maybe we shouldnt use selectedMG for this method and just use our own vars e.g. 

            var reccomendation = new Reccomendation();
            var expertNote = "";
            var exercisesSection = "";


            //if (ViewModel.InjuredJoint != null)
            //{
            //    switch (ViewModel.InjuredJoint.Name.ToLower())
            //    {
            //        case "wrist":
            //            {
            //                //just body weight exercises until its better
            //                //may aswell focus more so on legs
            //                 reccomendation.OpeningMessage = "\U0001F614\U0001F614\U0001F614\U0001F614\U0001F614";

            //                var returnString = (GymHome == GymOrHome.Gym) ? "Lets not pretend, it will limit what you can do in the gym. For now we advise you don't" +
            //                    "lift weights unless the pains minor, you won't be able to lift as heavy anyway so why risk injury for a lighter weight? \r\n\r\n" +
            //                    "\r\n\r\nDo you think you can hold your bodyweight? \r\n\r\nIf not ensure to stick to leg and core workouts for a few days, we'll send you some exercises dont worry these" +
            //                    "won't include anything that will put too much pressure on your wrist but you'll have to be the judge, if in doubt play it safe. " :
            //                    "\r\n\r\nDo you think you can hold your bodyweight? \r\n\r\nIf not ensure to stick to leg and core workouts for a few days, we'll send you some exercises dont worry these" +
            //                    "won't include anything that will put too much pressure on your wrist but you'll have to be the judge, if in doubt play it safe. ";

            //                reccomendation.ExpertNote = "Expert Note \r\n\r\n" + returnString;

            //                var exercises = new List<Exercise>();
            //                var mgs = new Route("").MuscleGroups;
            //                exercises.Add(ViewModel.GetSingleExericise(null,mgs.FirstOrDefault(i => i.Name == "Qauds"), GymOrHome.Home, Goal));
            //                exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Hamms"), GymOrHome.Home, Goal));
            //                exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Calves"), GymOrHome.Home, Goal));
            //                exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Chest"), GymOrHome.Home, Goal));
            //                exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Core"), GymOrHome.Home, Goal));

            //                 exercisesSection = "Exercises \r\n\r\n";

            //                foreach (var i in exercises)
            //                    exercisesSection += " " + i.Name + " - Rating: " + i.Popularity.ToString().Substring(0, 3) + "\r\n\r\n";

            //                reccomendation.ReccomendedExercises = exercisesSection;
            //            }
            //            break;
            //        case "ankle":
            //            {
            //                reccomendation.OpeningMessage = "\U0001F614\U0001F614\U0001F614\U0001F614\U0001F614";

            //                var returnString = (Gender == AppPrototype.Models.Enums.Gender.M) ? "You'll be happy to know leg days cancelled, don't \U0001F95B it though! \U0001F602, it will limit what you can do in the gym. For now we advise you don't" +
            //                    "train legs and aim to do a seated variation of any standing exercise heres a few to keep you going\r\n\r\n" :
            //                    "Commiserations as a lady we understand you actually train your legs, unlike some men! However stay away from legs until its recovered, why not focus on core and glutes? Heres some exercise ideas (P.s do any standing exercises seated!)";

            //                reccomendation.ExpertNote = "Expert Note \r\n\r\n" + returnString;

            //                var exercises = new List<Exercise>();
            //                var mgs = new Route("").MuscleGroups;

            //                if (Gender == AppPrototype.Models.Enums.Gender.M)
            //                {
            //                    exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Chest"), GymOrHome.Home, Goal));
            //                    exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Back"), GymOrHome.Home, Goal));
            //                    exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Shoulders"), GymOrHome.Home, Goal));
            //                    exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Biceps"), GymOrHome.Home, Goal));
            //                    exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Triceps"), GymOrHome.Home, Goal));
            //                }
            //                else
            //                {
            //                    exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Core"), GymOrHome.Home, Goal));
            //                    exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Core"), GymOrHome.Home, Goal));
            //                    exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Glutes"), GymOrHome.Home, Goal));
            //                    exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Glutes"), GymOrHome.Home, Goal));
            //                    exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Shoulders"), GymOrHome.Home, Goal));
            //                }


            //                exercisesSection = "Exercises \r\n\r\n";

            //                foreach (var i in exercises)
            //                    exercisesSection += " " + i.Name + " - Rating: " + i.Popularity.ToString().Substring(0, 3) + "\r\n\r\n";

            //                reccomendation.ReccomendedExercises = exercisesSection;
            //            }
            //            break;
            //        case "hip":
            //            {
            //                reccomendation.OpeningMessage = "\U0001F614\U0001F614\U0001F614\U0001F614\U0001F614";

            //                var returnString = (Gender == AppPrototype.Models.Enums.Gender.M) ? "You'll be happy to know leg days cancelled, don't \U0001F95B it though! \U0001F602, it will limit what you can do in the gym. For now we advise you don't" +
            //                    "train legs and aim to do a seated variation of any standing exercise heres a few to keep you going\r\n\r\n" :
            //                    "Commiserations as a lady we understand you actually train your legs, unlike some men! However stay away from legs until its recovered, why not focus on core and glutes? Heres some exercise ideas (P.s do any standing exercises seated!)";

            //                reccomendation.ExpertNote = "Expert Note \r\n\r\n" + returnString;

            //                var exercises = new List<Exercise>();
            //                var mgs = new Route("").MuscleGroups;

            //                if (Gender == AppPrototype.Models.Enums.Gender.M)
            //                {
            //                    exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Chest"), GymOrHome.Home, Goal));
            //                    exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Back"), GymOrHome.Home, Goal));
            //                    exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Shoulders"), GymOrHome.Home, Goal));
            //                    exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Biceps"), GymOrHome.Home, Goal));
            //                    exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Triceps"), GymOrHome.Home, Goal));
            //                }
            //                else
            //                {
            //                    exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Core"), GymOrHome.Home, Goal));
            //                    exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Core"), GymOrHome.Home, Goal));
            //                    exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Glutes"), GymOrHome.Home, Goal));
            //                    exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Glutes"), GymOrHome.Home, Goal));
            //                    exercises.Add(ViewModel.GetSingleExericise(null, mgs.FirstOrDefault(i => i.Name == "Shoulders"), GymOrHome.Home, Goal));
            //                }


            //                exercisesSection = "Exercises \r\n\r\n";

            //                foreach (var i in exercises)
            //                    exercisesSection += " " + i.Name + " - Rating: " + i.Popularity.ToString().Substring(0, 3) + "\r\n\r\n";

            //                reccomendation.ReccomendedExercises = exercisesSection;
            //            }
            //            break;
            //        case "knee":
            //            {

            //            }
            //            break;
            //        case "elbow":
            //            {

            //            }
            //            break;
            //        case "foot":
            //            {

            //            }
            //            break;
            //        case "neck":
            //            {

            //            }
            //            break;

        }

        [LuisIntent("MuscleTurnToFat")]
        private async Task MuscleTurnToFat(IDialogContext context, IAwaitable<object> activity, LuisResult result)
        {
            //variables
            var typingMsg = context.MakeMessage();
            typingMsg.Type = ActivityTypes.Typing;

            //wait for 2 seconds
            Thread.Sleep(1000);

            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(3000);



            //send the progress update
            var msg = context.MakeMessage();
            msg.Attachments = new List<Attachment>() { new HeroCard(images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\stage_tailor.jpg") }).ToAttachment() };
            await context.PostAsync(msg);

            //wait for 2 seconds
            Thread.Sleep(2000);
            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(1000);


            await context.PostAsync("\U0001F645 \r\n\r\nIt doesnt turn to fat, thats a myth. \r\n\r\n But theres a reason the myth came about");

            await context.PostAsync("Lets say your training" +
                " 3 days per week. If your pushing yourself you'll be hungry no doubt, and as your burning alot of calories regularly you can afford to have \U0001F645 every now and then." +
                "\r\n\r\nBut if you suddenly stopped training would your eating habits change to reflect the less calories burnt? ... Its unlikely. You'd still be hungry and your body wouldnt" +
                "burn it off as quick as it used to hense the gradual increase of fat." + "\r\n\r\nSo what happens to the muscle? ... where it decreases in size as its not longer needed as " +
                "your body no longer pushes heavy weights or conducts the same physical exercise as what built it. Sad i know. ");
        }

        [LuisIntent("NeedMotivation")]
        private async Task NeedMotivation(IDialogContext context, IAwaitable<object> activity, LuisResult result)
        {
            //variables
            var typingMsg = context.MakeMessage();
            typingMsg.Type = ActivityTypes.Typing;

            //wait for 2 seconds
            Thread.Sleep(1000);

            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(3000);



            //send the progress update
            var msg = context.MakeMessage();
            msg.Attachments = new List<Attachment>() { new HeroCard(images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\stage_tailor.jpg") }).ToAttachment() };
            await context.PostAsync(msg);

            //wait for 2 seconds
            Thread.Sleep(2000);
            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(1000);


            await context.PostAsync("You've made the effort to ask the question. Mentally your there already. \r\n\r\n" +
                "There will always be distractions so theres probably not much use listing what they are.");

            await context.PostAsync("The word 'Plan' probably sounds joyless to you, because it is, lets face it a large percentage of people don't stick to their 'plan' all the " +
                "time (even if they say they do!) \r\n\r\nBut then on the flipside, theres that over used saying that you'll probably cringe at so we'll just say you will need some" +
                "kind of routine. Wether its a 15 minute session 3 times a week, or even a sporting event with a friend.");

            PromptDialog.Choice(
 context: context,
 resume: BestCalorieBurnerComplete,
 options: new List<string>() { "Yes", "Maybe", "Unsure" },
 prompt: "Are you confident about it?",
 retry: "Selected plan not avilable . Please try again.",
 promptStyle: PromptStyle.Auto);

        }

        [LuisIntent("HowLongRest")]
        private async Task HowLongRest(IDialogContext context, IAwaitable<object> activity, LuisResult result)
        {
            //variables
            var typingMsg = context.MakeMessage();
            typingMsg.Type = ActivityTypes.Typing;

            //wait for 2 seconds
            Thread.Sleep(1000);

            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(3000);



            //send the progress update
            var msg = context.MakeMessage();
            msg.Attachments = new List<Attachment>() { new HeroCard(images: new List<CardImage>() { new CardImage(@"D:\DDriveDownloads\stage_tailor.jpg") }).ToAttachment() };
            await context.PostAsync(msg);

            //wait for 2 seconds
            Thread.Sleep(2000);
            //send typing msg
            await context.PostAsync(typingMsg);
            Thread.Sleep(1000);

            context.Call<object>(new HowMuchRestDialog(), HowLongRestDialogComplete);
        }


    }
}