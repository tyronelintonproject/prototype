﻿

using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using Microsoft.Bot.Builder.FormFlow;
using System.Collections.Generic;
using AppPrototype.Models;
using AppPrototype.Models.Enums;

namespace PrototypeBot.Dialogs
{
    [Serializable]
    public class PlanGeneratorDialog : IDialog<object>
    {

        public string IWantGoal { get; set; }
        public string WeightGoal { get; set; }
        public string Sex { get; set; }
        public string FitnessLevel { get; set; }
        public string DaysPerWeek { get; set; }
        public string SessionLength { get; set; }
        public string Activity { get; set; }
        public string GymOrHome { get; set; }
        public string ChoosenPhoto { get; set; }
        public Celebrity SelectedCelebrity { get; set; }
        public string GymOrHomeReccomendation { get; set; }
        public string IWantToReccomendation { get; set; }
        public string TimeExpectancy { get; set; }
        public string UserWeight { get; set; }
        public string UserAge { get; set; }
        public string UserHeight { get; set; }
        public string MealsPlannedFor { get; set; }


        public async Task StartAsync(IDialogContext context)
        {
            await GetGoalChoiceAsync(context);
         //   msg.Attachments.Add(GetIntroCard().ToAttachment());
         //   await context.PostAsync(msg);

         //   PromptDialog.Choice(
         //context: context,
         //   resume: FirstChoiceReceivedAsync,
         //   options: new List<String>() { "lets go", "show me more" },
         //   retry: "Were having a few issues",
         //   prompt: "Ready to start asking?",
         //   promptStyle: PromptStyle.Auto
         //   );

            //context.Wait(FirstChoiceReceivedAsync);
        }

        private async Task FirstChoiceReceivedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            await GetGoalChoiceAsync(context);

            //  var userMessage = "You’ll be able to say things like “I’m going on holiday in 6 weeks” and we’ll take care of the whole prep for you, or I" +
            //  "want to get bigger legs / arms, please help me lose body fat asap” we’ll instantly update your plan using tried and" +
            //"tested techniques or “i had a kebab last night balance me out!” “i’ve only got mc donalds and subway as lunch options";

         //   var msg = await activity;

         //   if (msg == "show me more")
         //   {
         //       var newMsg = context.MakeMessage();
         //       newMsg.Attachments.Add(GetCard1().ToAttachment());//how to use the fitness plan gif
         //       newMsg.Attachments.Add(GetCard2().ToAttachment());//how to use the nutrition plan gif
         //       newMsg.Attachments.Add(GetCard3().ToAttachment());//how to use the fitness plan gif
         //       await context.PostAsync(newMsg);
         //       //add a "Are you happy msg at the end of here before just sending more or doing more"?
         //       PromptDialog.Choice(
         //  context: context,
         //     resume: GetGoalChoiceAsync,
         //     options: new List<String>() { "Lets go" },
         //     retry: "Were having a few issues",
         //     prompt: "Are to start asking?",
         //     promptStyle: PromptStyle.Auto
         //     );
         //       return;
         //   }

         //   PromptDialog.Choice(
         //context: context,
         //   resume: GoalChoiceReceivedAsync,
         //   options: new List<String>() { "Simply typing 'i want to gain/lose etc'", "i'll choose from the model list", "theres a celebrity i want to look like" },
         //   retry: "having issues",
         //   prompt: "Great, we're going to ask a few questions create a tailored plan. Tell us your goal by...",
         //   promptStyle: PromptStyle.Auto
         //   );
         //   //var messageToSend = context.MakeMessage();
            //messageToSend.Text = "either click on 
           // of who you'd like to look like or tell us what you want to look like e.g. 'i want to...'";
            //messageToSend.Attachments = GetBodyCards();

            //await context.PostAsync(messageToSend);
            //context.Wait(MessageReceivedAsync);
        }



        private async Task GetGoalChoiceAsync(IDialogContext context)
        {
            PromptDialog.Choice(
            context: context,
               resume: GoalChoiceReceivedAsync,
               options: new List<String>() { "Simply typing 'i want to gain/lose etc'", "i'll choose from the model list", "theres a celebrity i want to look like" },
               retry: "Were having a few issues",
               prompt: "Tell us your goal by",
               promptStyle: PromptStyle.Auto
               );
        }


        private async Task UserGenderRecievedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            Sex = await activity;

            var gymOrHomeRec = "Where will you be training?";

            if (WeightGoal == "GainMuscle")
                gymOrHomeRec = "Where will you be training? .. gaining muscle is usually quicker when working with weights in a gym, however it is " +
                    "still very effective to use your bodyweight to get bigger and stronger";
            if (WeightGoal == "ToneUp")
                gymOrHomeRec = "Where will you be training? .. toning up is generally achieved at a similar rate if training at gym or home";
            if (WeightGoal == "LooseFat")
                gymOrHomeRec = "Where will you be training? .. loosing fat is usually quicker when doing HIIT (high intense cardio workouts) which can be done in the comfort of your home" +
                    " however if your worried about loosing muscle mass working with weights in a gym alongside short bursts of HIIT is the right option for you";

            PromptDialog.Choice(
            context: context,
               resume: ThirdChoiceReceivedAsync,
               options: new List<String>() { "Gym", "Home" },
               retry: "Were having a few issues",
               prompt: gymOrHomeRec,
               promptStyle: PromptStyle.Auto
               );
        }



        private async Task GoalChoiceReceivedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            var choice = await activity;

            switch (choice.ToLower())
            {
                case "simply typing 'i want to gain/lose etc'":
                    {
                        //Do a test to check that we find an intent for what they asked. if we do then save it as their i want goal, set the weight goal based on the i want goal,
                        //we then either need to find out whether its an ASAP or soon goal, i think they will all want this ASAP tho, especially on creation. //need to get the users sex.

                        await context.PostAsync("Fire away just say 'i want to (followed by what your goal is , e.g. i want to loose stubborn belly fat)'");
                        context.Wait(UserIWantToRecievedAsync);

                        //i need to get the users sex? maybe i can get through channel data but for now il just get through a method
                    }
                    break;
                case "i'll choose from the model list":
                    {
                        var messageToSend = context.MakeMessage();
                        messageToSend.Text = "Tap on the image that you'd prefer to look like";
                        messageToSend.Attachments = GetBodyCards();

                        await context.PostAsync(messageToSend);
                        context.Wait(ModelChoiceReceivedAsync);
                        //If a type 1 male then IWantRecc is to IWantToGetBigger
                        //if type 2 female then IWantRecc is IWantBiggerQaudsGlutes,BitOfCardioToo. send the user the reccomendation, then ask if this is right or not. if right set the i want to 
                        //that. set the weight goal and time goal. //dont need to find out sex.
                    }
                    break;
                case "theres a celebrity i want to look like":
                    {
                        //look through our DB for a celebrity with that name, if 1 match set the match that celebritys type to a type e.g. if a type 1 male then IWantRecc is to IWantToGetBigger
                        //if type 2 female then IWantRecc is IWantBiggerQaudsGlutes,BitOfCardioToo. send the user the reccomendation, then ask if this is right or not. if right set the i want to 
                        //that. set the weight goal and time goal. (if more than 1 match send a list of possibles, if no match try to find closest possiblities //dont need to find out sex
                        await context.PostAsync("Whats the name of the celebrity?");
                        context.Wait(CelebrityReceivedAsync);
                    }
                    break;
            }

        }

        private async Task CelebrityReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            var message = await activity;

            SelectedCelebrity = new CelebrityFinder().FindSingle(message.Text);
            var celebrities = new List<Celebrity>();

            Sex = SelectedCelebrity.Gender.ToString();

            if (SelectedCelebrity != null)
            {
                if (SelectedCelebrity.Gender == Gender.M)
                {
                    switch (SelectedCelebrity.BodyTypeId)
                    {
                        case 1:
                            {
                                IWantToReccomendation = "We can see that you want to get a muscular physqiue, the fastest way to look bigger is to develop bigger" +
                               "shoulders as well as put dense muscle on in other areas, would you agree this sounds good?";
                            }
                            break;
                        case 2:
                            {
                                IWantToReccomendation = "We can see that you want to get a toned physqiue, the fastest way to look bigger is to hit heavy compound exercises" +
                                "as well as do some high intensey cardio along with a controlled diet, would you agree this sounds good?";
                            }
                            break;
                        case 3:
                            {
                                IWantToReccomendation = "We can see that you want to get a toned physqiue, the fastest way to look bigger is to hit heavy compound exercises" +
                               "as well as do some high intensey cardio along with a controlled diet, would you agree this sounds good?";
                            }
                            break;
                    }
                }
                else
                {
                    switch (SelectedCelebrity.BodyTypeId)
                    {
                        case 1:
                            {
                                IWantToReccomendation = "We can see that you want to sculpt a strong physique with attention to adding muscle mass" +
                            "the fastest way to achieve this would be to focus on compound movements that use bigger muscle groups such as heavy pushing and " +
                            "pulling movements, we reccomend training with weights yet this physique can still be achieved using just bodyweight" +
                            "would you agree this sounds good?";
                            }
                            break;
                        case 2:
                            {
                                IWantToReccomendation = "We can see that you want to get a toned physqiue with bigger thighs and glutes," +
                            "the fastest way to achieve this would be to add a focus to your glutes and thighs" +
        "as well as other areas, would you agree this sounds good?";
                            }
                            break;
                        case 3:
                            {
                                IWantToReccomendation = "We can see that you want to shed some body fat," +
                            "the fastest way to achieve this would be to use HIIT cardio which will give you the quickest results" +
        "would you agree this sounds good?";
                            }
                            break;
                    }
                }

                PromptDialog.Choice(
                    context: context,
                    resume: CelebOptionDecidedAsync,
                    options: new List<String>() { "Yes thats exactly what i want", "yes but i don't want to add to much focus on that", "nope not what ive" +
                "got in mind" },
                    prompt: IWantToReccomendation,
                    retry: "Selected plan not avilabel . Please try again.",
                    promptStyle: PromptStyle.Auto);

            }
        }

        private async Task UserIWantToRecievedAsync(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            var message = await activity;

            SetWeightGoal(message.Text);

            IWantGoal = message.Text;

            PromptDialog.Choice(
            context: context,
               resume: UserGenderRecievedAsync,
               options: new List<String>() { "male", "female" },
               retry: "Having issues",
               prompt: "Whats your gender",
               promptStyle: PromptStyle.Auto
               );

        }

        private async Task SendFirstChoiceAsync(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            PromptDialog.Choice(
            context: context,
               resume: FirstChoiceReceivedAsync,
               options: new List<String>() { "lets go", "show me more" },
               retry: "Were having a few issues",
               prompt: "",
               promptStyle: PromptStyle.Auto
               );
        }

        private async Task SecondReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            var messageToSend = context.MakeMessage();
            messageToSend.Text = "Tap on the image that you'd prefer to look like";
            messageToSend.Attachments = GetBodyCards();

            await context.PostAsync(messageToSend);
            //  context.Wait(MessageReceivedAsync);
        }

        public virtual async Task HearMoreDialogComplete(IDialogContext context, IAwaitable<object> response)
        {
            var messageToSend = context.MakeMessage();
            messageToSend.Text = "Tap on the image that you'd prefer to look like or if none say 'I want to get bigger X or I want to loose X' etc";
            //   messageToSend. = GetImages().ToAttachment();

            await context.PostAsync(messageToSend);
            //  context.Wait(MessageReceivedAsync);
        }



        private async Task ModelChoiceReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            var message = await activity;

            //if (!message.Text.Contains("Option"))
            //{
            //    IWantToReccomendation = "we see you know what you want how soon do you want it?";
            //    PromptDialog.Choice(
            //   context: context,
            //   resume: OptionDecidedAsync,
            //   options: new List<String>() { "ASAP", "Soon" },
            //   prompt: IWantToReccomendation,
            //   retry: "Selected plan not avilable . Please try again.",
            //   promptStyle: PromptStyle.Auto);

            //}


            switch (message.Text)
            {
                case "Option1Male":
                    ChoosenPhoto = "Option1Male";
                    Sex = "Male";
                    IWantToReccomendation = "We can see that you want to get a muscular physqiue, the fastest way to look bigger is to develop bigger" +
                        "shoulders as well as put dense muscle on in other areas, would you agree this sounds good?";
                    break;
                case "Option2Male":
                    ChoosenPhoto = "Option1Male";
                    Sex = "Male";
                    IWantToReccomendation = "We can see that you want to get a toned physqiue, the fastest way to look bigger is to hit heavy compound exercises" +
                        "as well as do some high intensey cardio along with a controlled diet, would you agree this sounds good?";
                    break;
                case "Option3Male":
                    ChoosenPhoto = "Option1Male";
                    Sex = "Male";
                    IWantToReccomendation = "We can see that you want to get a toned physqiue, the fastest way to look bigger is to hit heavy compound exercises" +
                         "as well as do some high intensey cardio along with a controlled diet, would you agree this sounds good?";
                    break;

                case "Option2Female":
                    ChoosenPhoto = "Option2Female";
                    IWantToReccomendation = "We can see that you want to get a toned physqiue with bigger thighs and glutes," +
                        "the fastest way to achieve this would be to add a focus to your glutes and thighs" +
    "as well as other areas, would you agree this sounds good?";
                    Sex = "Female";
                    break;
            }

            PromptDialog.Choice(
                context: context,
                resume: OptionDecidedAsync,
                options: new List<String>() { "Yes thats exactly what i want", "yes but i don't want to add to much focus on that", "nope not what ive" +
                "got in mind" },
                prompt: IWantToReccomendation,
                retry: "Selected plan not avilabel . Please try again.",
                promptStyle: PromptStyle.Auto);
        }


        private async Task OptionDecidedAsync(IDialogContext context, IAwaitable<string> activity)
        {

            var message = await activity;

            if (message == "nope not what ive got in mind")
            {


            }

            TimeExpectancy = SetTimeExpectancy(message);



            switch (ChoosenPhoto)
            {
                case "Option1Male":


                    if (TimeExpectancy.ToLower() == "incorrect")
                    {
                        //context.Wait(ChoiceReceivedAsync(context);
                        return;
                    }

                    IWantGoal = "I want to get big " + TimeExpectancy;
                    WeightGoal = "GainMuscle";
                    Sex = "Male";
                    GymOrHomeReccomendation = "Where will you be training? Based on your prior choices we reccomend you train at the gym to achieve the quickest results but training at home can also produce" +
                        "good results too";
                    break;

                case "Option2Male":

                    if (TimeExpectancy.ToLower() == "incorrect")
                    {
                        //context.Wait(ChoiceReceivedAsync(context);
                        return;
                    }

                    IWantGoal = "I want to get a strong core" + TimeExpectancy;
                    WeightGoal = "ToneUp";
                    Sex = "Male";
                    GymOrHomeReccomendation = "Where will you be training? Based on your prior choices we reccomend you train at the gym to achieve the quickest results but training at home can also produce" +
                        "good results too";
                    break;

                case "Option3Male":


                    if (TimeExpectancy.ToLower() == "incorrect")
                    {
                        //context.Wait(ChoiceReceivedAsync(context);
                        return;
                    }

                    IWantGoal = "I want to loose body fat" + TimeExpectancy;
                    WeightGoal = "LooseFat";
                    Sex = "Male";
                    GymOrHomeReccomendation = "Where will you be training? Based on your prior choices either option will be sufficent to help you reach your goals, diet will play a key role" +
                        " in your results";
                    break;

                case "Option2Female":
                    IWantGoal = "I want to get bigger qauds and glutes" + TimeExpectancy;
                    WeightGoal = "ToneUp";
                    Sex = "Female";
                    GymOrHomeReccomendation = "You can achieve this physique both at home or in the gym, the gym may give you a slight advantage in terms"
                    + "how quickly the changes occur";
                    break;
                case "Option1Female":
                    IWantGoal = "I want to get big" + TimeExpectancy;
                    WeightGoal = "GainMuscle";
                    Sex = "Female";
                    GymOrHomeReccomendation = "We reccomend you train at the gym as this will give you quicker results than training at home";
                    break;

                case "Option3Female":
                    IWantGoal = "I want to loose fat" + TimeExpectancy;
                    WeightGoal = "LooseFat";
                    Sex = "Female";
                    GymOrHomeReccomendation = "You can achieve this physique both at home or in the gym, the gym may give you a slight advantage in terms"
                    + "how quickly the changes occur";
                    break;
            }
            PromptDialog.Choice(
            context: context,
                        resume: ThirdChoiceReceivedAsync,
                        options: new List<String>() { "gym", "home" },
                        prompt: GymOrHomeReccomendation,
                        retry: "Selected plan not avilabel . Please try again.",
                        promptStyle: PromptStyle.Auto);
        }


        private async Task CelebOptionDecidedAsync(IDialogContext context, IAwaitable<string> activity)
        {

            var message = await activity;

            TimeExpectancy = SetTimeExpectancy(message);

            if (SelectedCelebrity.Gender == Gender.M)
            {
                switch (SelectedCelebrity.BodyTypeId)
                {
                    case 1:

                        if (TimeExpectancy.ToLower() == "incorrect")
                        {
                            //context.Wait(ChoiceReceivedAsync(context);
                            return;
                        }

                        IWantGoal = "I want to get big" + TimeExpectancy;
                        WeightGoal = "GainMuscle";
                        GymOrHomeReccomendation = "Where will you be training? Based on your prior choices we reccomend you train at the gym to achieve the quickest results but training at home can also produce" +
                       "good results too";
                        break;

                    case 2:
                        IWantGoal = "I want to get a strong core" + TimeExpectancy;
                        WeightGoal = "ToneUp";
                        GymOrHomeReccomendation = "You can achieve this physique both at home or in the gym, the gym may give you a slight advantage in terms"
                        + "how quickly the changes occur";
                        break;
                    case 3:
                        IWantGoal = "I want to loose body fat" + TimeExpectancy;
                        WeightGoal = "LooseFat";
                        GymOrHomeReccomendation = "Where will you be training? Based on your prior choices either option will be sufficent to help you reach your goals, diet will play a key role" +
                      " in your results";
                        break;
                }
            }
            else
            {
                switch (SelectedCelebrity.BodyTypeId)
                {
                    case 1:
                        {
                            IWantGoal = "I want to get big" + TimeExpectancy;
                            WeightGoal = "GainMuscle";
                            GymOrHomeReccomendation = "We reccomend you train at the gym as this will give you quicker results than training at home";
                        }
                        break;
                    case 2:
                        {
                            IWantGoal = "I want to get bigger qauds and glutes" + TimeExpectancy;
                            WeightGoal = "ToneUp";
                            GymOrHomeReccomendation = "You can achieve this physique both at home or in the gym, the gym may give you a slight advantage in terms"
                   + "how quickly the changes occur";
                        }
                        break;
                    case 3:
                        {
                            IWantGoal = "I want to loose fat" + TimeExpectancy;
                            WeightGoal = "LooseFat";
                            GymOrHomeReccomendation = "You can achieve this physique both at home or in the gym, the gym may give you a slight advantage in terms"
                   + "how quickly the changes occur";
                        }
                        break;
                }
            }
            PromptDialog.Choice(
            context: context,
                        resume: ThirdChoiceReceivedAsync,
                        options: new List<String>() { "gym", "home" },
                        prompt: GymOrHomeReccomendation,
                        retry: "Selected plan not avilabel . Please try again.",
                        promptStyle: PromptStyle.Auto);
        }


        public async Task GetGymOrHomeAsync(IDialogContext context, IAwaitable<string> activity)
        {
            PromptDialog.Choice(
           context: context,
                       resume: ThirdChoiceReceivedAsync,
                       options: new List<String>() { "gym", "home" },
                       prompt: GymOrHomeReccomendation,
                       retry: "Selected plan not avilabel . Please try again.",
                       promptStyle: PromptStyle.Auto);
        }


        private async Task ThirdChoiceReceivedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            GymOrHome = await activity;

            if (WeightGoal == "GainMuscle")
            {
                if (GymOrHome == "Gym")
                    Activity = "Weightlifting";
                Activity = "Bodyweighttraining";
            }
            if (WeightGoal == "ToneUp")
            {
                if (GymOrHome == "Gym")
                    Activity = "Weightlifting";
                Activity = "Bodyweighttraining";
            }
            if (WeightGoal == "LooseFat")
            {
                if (GymOrHome == "Gym")
                    Activity = "WeightliftingHIIT";
                Activity = "HIIT";
            }


            var userMessage = "tap the image that describes you current fitness level";

            var message = context.MakeMessage();
            message.Text = userMessage;
            message.Attachments = GetFitnessCards();
            await context.PostAsync(message);

            context.Wait(FourthChoiceReceivedAsync);

        }

        private async Task FourthChoiceReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            var message = await activity;
            FitnessLevel = message.Text;

            var userMessage = "how many days per week do you want to exercise?";

            if (WeightGoal == "GainMuscle")
            {
                switch (FitnessLevel)
                {
                    case "Advanced":
                        { userMessage += " based on what you've said we suggest either 4/5/6"; };
                        break;
                    case "Intermediate":
                        { userMessage += " based on what you've said we suggest either 3/4/5"; }
                        break;
                    case "Beginner":
                        { userMessage += " based on what you've said we suggest either 3/4"; }
                        break;
                }
            }
            if (WeightGoal == "ToneUp")
            {
                switch (FitnessLevel)
                {
                    case "Advanced":
                        { userMessage += " based on what you've said we suggest either 4/5/6"; };
                        break;
                    case "Intermediate":
                        { userMessage += " based on what you've said we suggest either 3/4/5"; }
                        break;
                    case "Beginner":
                        { userMessage += " based on what you've said we suggest either 3/4"; }
                        break;
                }
            }
            if (WeightGoal == "LooseFat")
            {
                switch (FitnessLevel)
                {
                    case "Advanced":
                        { userMessage += " based on what you've said we suggest either 4/5/6"; };
                        break;
                    case "Intermediate":
                        { userMessage += " based on what you've said we suggest either 3/4/5"; }
                        break;
                    case "Beginner":
                        { userMessage += " based on what you've said we suggest either 3/4"; }
                        break;
                }
            }

            PromptDialog.Choice(
            context: context,
            resume: FifthChoiceReceivedAsync,
            options: new List<String>() { "3", "4", "5", "6" },
            prompt: userMessage,
            retry: "Selected plan not avilabel . Please try again.",
            promptStyle: PromptStyle.Auto);
        }

        private async Task FifthChoiceReceivedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            DaysPerWeek = await activity;

            var userMessage = "how long do you want to train for?";

            if (WeightGoal == "GainMuscle" || WeightGoal == "ToneUp")
            {
                if (Activity == "Weightlifting")
                {
                    if (FitnessLevel == "Advanced")
                        userMessage += " We'd reccomend 45 or 60 minute sessions for yourself, as long as its no less than 30 your good to go!";
                    userMessage += " We'd reccomend 30 or 45 minute sessions for yourself, this will keep the intensity up";
                }
                else
                {
                    if (FitnessLevel == "Advanced")
                        userMessage += " We'd reccomend 30 or 45 minute sessions for yourself, as long as its no less than 30 your good to go!";
                    userMessage += " We'd reccomend 30 minute sessions for yourself, this will keep the intensity up";
                }
            }
            else
            {//they want to loose
                if (FitnessLevel == "Advanced")
                    userMessage += " We'd reccomend 20 or 30 minute sessions for yourself, as long as its no less than 20 your good to go!";
                userMessage += " We'd reccomend 10 or 20 minute sessions for yourself, this will keep the intensity up";
            }

            PromptDialog.Choice(
            context: context,
            resume: SixthChoiceReceivedAsync,
            options: SetSessionLength(),
            prompt: userMessage,
            retry: "Selected plan not avilabel . Please try again.",
            promptStyle: PromptStyle.Auto);
        }

        private async Task SixthChoiceReceivedAsync(IDialogContext context, IAwaitable<int> activity)
        {
            var message = await activity;
            SessionLength = message.ToString();

            await context.PostAsync("Your fitness plans done!!! (EMOJI)");

            var mealGifMsg = context.MakeMessage();
            mealGifMsg.Attachments.Add(GetMealOptionCard1().ToAttachment());
            await context.PostAsync(mealGifMsg);

            PromptDialog.Choice(
            context: context,
            resume: SeventhChoiceReceivedAsync,
            options: new List<string>() { "I’ll finish it later i want to start working out", "I'll finish it now" },
            prompt: "Want to finish your diet plan too?",
            retry: "Selected plan not avilable . Please try again.",
            promptStyle: PromptStyle.Auto);
        }


        private async Task SeventhChoiceReceivedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            var message = await activity;

            if (message.ToLower().Contains("i'll finish it now"))
            {
                await context.PostAsync("Tell us your weight and the measurement you want us to use? (E.g. 16 stone, 180 pounds, 65 kg) if you don't know take a guess or say 'unsure', you can update this later");
                context.Wait(EigthChoiceReceivedAsync);
            }
            else
            {
                await context.PostAsync("Heres your plan!");
                context.Done(this);
            }
        }

        private async Task EigthChoiceReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {

            var message = await activity;
            if (message.Text.ToLower().Contains("unsure"))
                UserWeight = "";
            UserWeight = message.Text;

            await context.PostAsync("Tell us your Height in feet? (E.g. 5 foot 6, 5'6, 5 ft 3) if you don't know take a guess or say 'unsure', you can update this later");
            context.Wait(NinthChoiceReceivedAsync);
        }

        private async Task NinthChoiceReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            var message = await activity;

            if (message.Text.ToLower().Contains("unsure"))
                UserHeight = "";
            UserHeight = message.Text;

            await context.PostAsync("Finally do you mind revealing your age, we won't use this for anything except calculating your calories based on " +
                "your height, weight and age, if not just say 'No'");
            context.Wait(TenthChoiceReceivedAsync);
        }

        private async Task TenthChoiceReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            //here we would either use a new rich card that allowed user to tick no reply or we would check if theres numberes in the reply

            var message = await activity;

            if (message.Text.ToLower().Contains("no"))
                UserAge = "0";
            UserAge = message.Text;

            //await context.PostAsync("which meals would you like daily healthy options for? {e.g. breakfast and dinner, breakfast lunch and dinner, dinner and snack," +
            //    "breakfast and snack, all}");
            //context.Wait(EleventhChoiceReceivedAsync);

            PromptDialog.Choice(
            context: context,
            resume: EleventhChoiceReceivedAsync,
            options: new List<string>() { "Just Breakfast", "Just Lunch", "Just Dinner", "Just a Healthy Snack", "Both Breakfast & Dinner", "All of Above" },
            prompt: "which meals would you like to include in your plan?",
            retry: "Selected plan not avilabel . Please try again.",
            promptStyle: PromptStyle.Auto);
        }

        private async Task EleventhChoiceReceivedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            var message = await activity;

            MealsPlannedFor = message;

            await context.PostAsync("Sit back and bare with us while we generate your new plan, remember you can ask us any question, anytime");
            context.Done(this);
        }



        private async Task After(IDialogContext context)
        {
            //  var activity = await result;
            //  var number = Convert.ToInt32(activity) * Convert.ToInt32(activity);


            //   await context.PostAsync("hi the result is " +  number.ToString());
        }

        //public virtual async Task ShowAnnuvalConferenceTicket(IDialogContext context, IAwaitable<IMessageActivity> activity)
        //{
        //    var message = await activity;

        //    PromptDialog.Choice(
        //        context: context,
        //        resume: ChoiceReceivedAsync,
        //        options: (IEnumerable<AnnuvalConferencePass>)Enum.GetValues(typeof(AnnuvalConferencePass)),
        //        prompt: "Hi. Please Select Annuval Conference 2018 Pass :",
        //        retry: "Selected plan not avilabel . Please try again.",
        //        promptStyle: PromptStyle.Auto
        //        );
        //}

        //public virtual async Task ChoiceReceivedAsync(IDialogContext context, IAwaitable<AnnuvalConferencePass> activity)
        //{
        //    AnnuvalConferencePass response = await activity;
        //    context.Call<object>(new AccountDialog(), ChildDialogComplete);

        //}

        //public virtual async Task ChildDialogComplete(IDialogContext context, IAwaitable<object> response)
        //{
        //    await context.PostAsync("Thanks for select C# Corner bot for Annual Conference 2018 Registrion .");
        //    context.Done(this);
        //}



        //private async Task SendTheImagesAsync(IDialogContext context, IAwaitable<bool> result)
        //{
        //    var result1 = await result;

        //    if (!result1)

        //        return;

        //    var msg = context.MakeMessage();
        //    await context.Wait(this.StartAsync);
        //    msg.Attachments = new List<Attachment>();
        //    msg.Attachments.Add(GetHeroCard());
        //    await context.PostAsync(msg);
        //    PromptDialog.Text(
        //context: context,
        //resume: StartAsync,
        //prompt: "test",
        //attempts: 4);


        //}


        private static List<Attachment> GetBodyCards()
        {
            var muscularCardMale = new HeroCard
            {   //Tap cannot be used on Facebook messenger
                //Tap = new CardAction(ActionTypes.ImBack, text: "Option1", displayText: "Option 1 Please", value: "Option1Male"),
                Images = new List<CardImage> { new CardImage("https://lh3.googleusercontent.com/-fnwLMmJTmdk/WaVt5LR2OZI/AAAAAAAAG90/qlltsHiSdZwVdOENv1yB25kuIvDWCMvWACLcBGAs/h120/annuvalevent.PNG"),
                },
                Buttons = new List<CardAction>() { new CardAction(ActionTypes.ImBack, title: "This", text: "Option1", displayText:
                "Option 1 Please", value: "Option1Male")},
            };
            var muscularCardFemale = new HeroCard
            {
                //Tap = new CardAction(ActionTypes.ImBack, text: "Option1", displayText: "Option 1 Please", value: "Option1Female"),
                Images = new List<CardImage> { new CardImage("https://lh3.googleusercontent.com/-fnwLMmJTmdk/WaVt5LR2OZI/AAAAAAAAG90/qlltsHiSdZwVdOENv1yB25kuIvDWCMvWACLcBGAs/h120/annuvalevent.PNG") },
                Buttons = new List<CardAction>() { new CardAction(ActionTypes.ImBack, text: "Option1", displayText: "Option 1 Please",value: "Option1Female",
                title: "This") },
            };

            var musclarTonedCardMale = new HeroCard
            {
                //Tap = new CardAction(ActionTypes.ImBack, text: "Option2", displayText: "Option 2 Please", value: "Option2Male"),
                Images = new List<CardImage> { new CardImage("https://lh3.googleusercontent.com/-fnwLMmJTmdk/WaVt5LR2OZI/AAAAAAAAG90/qlltsHiSdZwVdOENv1yB25kuIvDWCMvWACLcBGAs/h120/annuvalevent.PNG") },
                Buttons = new List<CardAction>() { new CardAction(ActionTypes.ImBack, text: "Option2", displayText: "Option 2 Please", value: "Option2Male",
                title: "This") },
            };
            var TonedWithHourGlassCardFemale = new HeroCard
            {
                //Tap = new CardAction(ActionTypes.ImBack, text: "Option2", displayText: "Option 2 Please", value: "Option2Female"),
                Images = new List<CardImage> { new CardImage("https://lh3.googleusercontent.com/-fnwLMmJTmdk/WaVt5LR2OZI/AAAAAAAAG90/qlltsHiSdZwVdOENv1yB25kuIvDWCMvWACLcBGAs/h120/annuvalevent.PNG") },
                Buttons = new List<CardAction>() { new CardAction(ActionTypes.ImBack, text: "Option2", displayText: "Option 2 Please",
                title: "This", value: "Option2Female") },
            };

            var slimandTonedTonedCardMale = new HeroCard
            {
                //Tap = new CardAction(ActionTypes.ImBack, text: "Option3", displayText: "Option 3 Please", value: "Option3Male"),
                Images = new List<CardImage> { new CardImage("https://lh3.googleusercontent.com/-fnwLMmJTmdk/WaVt5LR2OZI/AAAAAAAAG90/qlltsHiSdZwVdOENv1yB25kuIvDWCMvWACLcBGAs/h120/annuvalevent.PNG") },
                Buttons = new List<CardAction>() { new CardAction(ActionTypes.ImBack, text: "Option3", displayText: "Option 3 Please",
                title: "This", value: "Option3Male") },
            };
            var slimCardFemale = new HeroCard
            {
                //Tap = new CardAction(ActionTypes.ImBack, text: "Option3", displayText: "Option 3 Please", value: "Option3Female"),
                Images = new List<CardImage> { new CardImage("https://lh3.googleusercontent.com/-fnwLMmJTmdk/WaVt5LR2OZI/AAAAAAAAG90/qlltsHiSdZwVdOENv1yB25kuIvDWCMvWACLcBGAs/h120/annuvalevent.PNG") },
                Buttons = new List<CardAction>() { new CardAction(ActionTypes.ImBack, text: "Option3", displayText: "Option 3 Please",
                title: "This", value: "Option3Female") },
            };

            return new List<Attachment>() { musclarTonedCardMale.ToAttachment(),muscularCardFemale.ToAttachment(), muscularCardMale.ToAttachment(),
            TonedWithHourGlassCardFemale.ToAttachment(), slimandTonedTonedCardMale.ToAttachment(), slimCardFemale.ToAttachment()};
        }


        private static List<CardImage> GetImages()
        {

            var image1 = (new CardImage("https://lh3.googleusercontent.com/-fnwLMmJTmdk/WaVt5LR2OZI/AAAAAAAAG90/qlltsHiSdZwVdOENv1yB25kuIvDWCMvWACLcBGAs/h120/annuvalevent.PNG")
            { Alt = "Muscular male", Tap = new CardAction() { Type = ActionTypes.ImBack, Value = "Option1Male" } });

            var image2 = new CardImage("https://lh3.googleusercontent.com/-fnwLMmJTmdk/WaVt5LR2OZI/AAAAAAAAG90/qlltsHiSdZwVdOENv1yB25kuIvDWCMvWACLcBGAs/h120/annuvalevent.PNG")
            { Alt = "Muscular male", Tap = new CardAction() { Type = ActionTypes.ImBack, Value = "Option2Male" } };

            var images = new List<CardImage>() { image1, image2 };

            return images;

        }


        private static List<Attachment> GetFitnessCards()
        {

            var advancedFitnessCard = new HeroCard
            {
                Images = new List<CardImage> { new CardImage("https://lh3.googleusercontent.com/-fnwLMmJTmdk/WaVt5LR2OZI/AAAAAAAAG90/qlltsHiSdZwVdOENv1yB25kuIvDWCMvWACLcBGAs/h120/annuvalevent.PNG"),
                },
                Buttons = new List<CardAction>() { new CardAction(ActionTypes.ImBack, text: "Advanced", displayText: "I'm advanced", value: "Advanced", title: "I'm advanced", image: "https://media.giphy.com/media/jAYUbVXgESSti/giphy.gif") }
            };
            var intermediateFitnessCard = new HeroCard
            {
                Images = new List<CardImage> { new CardImage("https://lh3.googleusercontent.com/-fnwLMmJTmdk/WaVt5LR2OZI/AAAAAAAAG90/qlltsHiSdZwVdOENv1yB25kuIvDWCMvWACLcBGAs/h120/annuvalevent.PNG") },
                Buttons = new List<CardAction>() { new CardAction(ActionTypes.ImBack, text: "Intermediate", displayText: "I'm intermediate", value: "Intermediate", title: "I'm intermediate") }
            };

            var beginnerFitnessCard = new HeroCard
            {
                Images = new List<CardImage> { new CardImage("https://lh3.googleusercontent.com/-fnwLMmJTmdk/WaVt5LR2OZI/AAAAAAAAG90/qlltsHiSdZwVdOENv1yB25kuIvDWCMvWACLcBGAs/h120/annuvalevent.PNG") },
                Buttons = new List<CardAction>() { new CardAction(ActionTypes.ImBack, text: "Beginner", displayText: "I'm a beginner", value: "Beginner", title: "I'm a beginner") }
            };

            return new List<Attachment>() { advancedFitnessCard.ToAttachment(), intermediateFitnessCard.ToAttachment(), beginnerFitnessCard.ToAttachment() };
        }

        public static HeroCard GetIntroCard()
        {
            return new HeroCard()
            {
                Tap = new CardAction(ActionTypes.PlayVideo),//<What does play video mean?
                Text = "Welcome to FitChat!, Any question, Anytime",
                Images = new List<CardImage> { new CardImage("https://media.giphy.com/media/jAYUbVXgESSti/giphy.gif") },
            };
        }

        public static HeroCard GetCard1()
        {
            return new HeroCard()
            {
                Tap = new CardAction(ActionTypes.PlayVideo),//<What does play video mean?
                Title = "How to use for fitness",
                Images = new List<CardImage> { new CardImage("https://media.giphy.com/media/jAYUbVXgESSti/giphy.gif") },
            };
        }
        public static HeroCard GetCard2()
        {
            return new HeroCard()
            {
                Tap = new CardAction(ActionTypes.PlayVideo),//<What does play video mean?
                Title = "How to use for food",
                Images = new List<CardImage> { new CardImage("https://media.giphy.com/media/jAYUbVXgESSti/giphy.gif") },
            };
        }
        public static HeroCard GetCard3()
        {
            return new HeroCard()
            {
                Tap = new CardAction(ActionTypes.PlayVideo),//<What does play video mean?
                Title = "How to ask questions",
                Images = new List<CardImage> { new CardImage("https://media.giphy.com/media/jAYUbVXgESSti/giphy.gif") },
            };
        }


        public static HeroCard GetMealOptionCard1()
        {
            return new HeroCard()
            {
                Tap = new CardAction(ActionTypes.PlayVideo),//<What does play video mean?
                Title = "How meal options work, Swipe to see what you can ask",
                Images = new List<CardImage> { new CardImage("https://media.giphy.com/media/jAYUbVXgESSti/giphy.gif") },
            };
        }

        //    public virtual async Task ShowCards(IDialogContext context, IAwaitable<IMessageActivity> activity)
        //    {
        //        if (activity.t)

        //        await context.PostAsync("Tap your preferred look");
        //    var message = context.MakeMessage();
        //    var attachments = GetCards();
        //    message.Attachments = attachments;
        //        await context.PostAsync(message);

        //    context.Wait(this.MessageReceivedAsync);
        //    }


        //    public enum AnnuvalConferencePass
        //    {
        //        EarlyBird,
        //        Regular,
        //        DelegatePass,
        //        CareerandJobAdvice,
        //    }

        public string SetTimeExpectancy(string message)
        {
            if (message == "Yes thats exactly what i want")
                return "ASAP";
            if (message == "yes but i don't want to add to much focus on that")
                return "Soon";
            if (message == "nope not what ive got in mind")
                return "Incorrect";
            return null;
        }

        public List<int> SetSessionLength()
        {
            switch (Activity)
            {
                case "Weightlifting":
                    {
                        return new List<int>() { 15, 30, 45, 60 };
                    }

                case "Bodyweighttraining":
                    {
                        return new List<int>() { 10, 20, 30, 45 };
                    }

                case "HIIT":
                    {
                        return new List<int>() { 10, 15, 20, 30 };
                    }


            }
            return null;
        }

        public void SetWeightGoal(string iWantTo)
        {


            if (iWantTo.ToLower().Contains("bigger") || iWantTo.ToLower().Contains("muscular") || iWantTo.ToLower().Contains("larger") ||
                iWantTo.ToLower().Contains("big") || iWantTo.ToLower().Contains("gain") || iWantTo.ToLower().Contains("grow") || iWantTo.ToLower().Contains("massive") ||
                iWantTo.ToLower().Contains("huge") || iWantTo.ToLower().Contains("full") || iWantTo.ToLower().Contains("strong") || iWantTo.ToLower().Contains("stronger"))
            { WeightGoal = "GainMuscle"; return; }

            if (iWantTo.ToLower().Contains("smaller") || iWantTo.ToLower().Contains("toned") || iWantTo.ToLower().Contains("tiny") ||
    iWantTo.ToLower().Contains("small") || iWantTo.ToLower().Contains("loose") || iWantTo.ToLower().Contains("strink") || iWantTo.ToLower().Contains("decrease") ||
    iWantTo.ToLower().Contains("get rid off"))
            { WeightGoal = "LoseFat"; return; }

            else { WeightGoal = "ToneUp"; }
        }

    }
}







