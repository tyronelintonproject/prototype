﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;

namespace PrototypeBot.Dialogs
{
    [Serializable]
    public class RootDialog : IDialog<object>
    {
        public async Task StartAsync(IDialogContext context)
        {

            await context.PostAsync("Welcome to Answerfit!, \r\n\r\nAsk any fitness question and receive an experts response tailored to you!");

            PromptDialog.Confirm(
        context: context,
        resume: MessageReceivedAsync,
        options: new string[] { "Yes" },
        prompt: "Got something to ask??",
        retry: "Selected plan not avilable . Please try again.",
        promptStyle: PromptStyle.Auto);
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<bool> result)
        {
            MakeRoot();
        }


        internal static IDialog<object> MakeRoot()
        {
            return Chain.From(() => new Dialogs.LuisDialog());
        }

    }
}