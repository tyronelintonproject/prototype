﻿using Microsoft.Bot.Builder.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PrototypeBot.Dialogs
{
    [Serializable]
    public class WantToLoseDialog : IDialog<object>
    {
        public string Gender { get; set; }
        public string Fitness { get; set; }
        public string MuscleGroup { get; set; }
        public string Diet { get; set; }
        public string GymHome { get; set; }

        public WantToLoseDialog(string muscleGroup = null)
        {
            MuscleGroup = (!string.IsNullOrEmpty(muscleGroup)) ? muscleGroup : "";
        }


        async Task IDialog<object>.StartAsync(IDialogContext context)
        {
            if (!string.IsNullOrEmpty(MuscleGroup))
            {
                PromptDialog.Choice(
            context: context,
            resume: GenderAndLevelRecievedSkipToDietAsync,
             options: new List<string>() { "Fit man", "Fit woman", "Average man", "Average woman", "Unfit man",
            "Unfit woman" },
            prompt: "Which best describes you?",
            retry: "Please try again.",
            attempts: 3,
            promptStyle: PromptStyle.Auto);
                return;
            }

            PromptDialog.Choice(
            context: context,
            resume: GenderAndLevelRecievedAsync,
            options: new List<string>() { "Fit man", "Average man", "Unfit man", "Fit woman",
            "Average woman", "Unfit woman" },
            prompt: "Lets get your answer we just need 3 bits of info. Are you a..?",
            retry: "Please try again.",
            attempts: 3,
            promptStyle: PromptStyle.Auto);
        }


        private async Task GenderAndLevelRecievedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            var resultString = await activity;

            Fitness = resultString.Substring(0, resultString.IndexOf(" "));
            Gender = resultString.Substring(resultString.IndexOf(" "), (resultString.Length - resultString.IndexOf(" ")));

            PromptDialog.Choice(
          context: context,
          resume: MuscleGroupRecievedAsync,
          options: new List<string>() { "Shoulders", "Chest", "Back", "Arms", "Core", "Qauds", "Hamms", "Calves" },
          prompt: "Where do you want to loose weight?",
          retry: "Please try again.",
          attempts: 3,
          promptStyle: PromptStyle.Auto);
        }

        private async Task GenderAndLevelRecievedSkipToDietAsync(IDialogContext context, IAwaitable<string> activity)
        {
            var resultString = await activity;

            Fitness = resultString.Substring(0, resultString.IndexOf(" "));
            Gender = resultString.Substring(resultString.IndexOf(" "), (resultString.Length - resultString.IndexOf(" ")));

            PromptDialog.Choice(
         context: context,
         resume: DietRecievedAsync,
         options: new List<string>() { "Very good", "Average", "Not that great" },
         prompt: "How heathy do you eat?",
         retry: "Please try again.",
         attempts: 3,
         promptStyle: PromptStyle.Auto);
        }

        private async Task MuscleGroupRecievedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            MuscleGroup = await activity;

            PromptDialog.Choice(
          context: context,
          resume: DietRecievedAsync,
          options: new List<string>() { "Very good", "Average", "Not that great" },
          prompt: "How heathy do you eat?",
          retry: "Please try again.",
          attempts: 3,
          promptStyle: PromptStyle.Auto);
        }

        private async Task DietRecievedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            Diet = await activity;

            PromptDialog.Choice(
          context: context,
          resume: GymHomeRecievedAsync,
          options: new List<string>() { "Gym", "Home" },
          prompt: "Where will you be working out?",
          retry: "Please try again.",
          attempts: 3,
          promptStyle: PromptStyle.Auto);
        }

        private async Task GymHomeRecievedAsync(IDialogContext context, IAwaitable<string> activity)
        {
            GymHome = await activity;

            context.Done(this);
        }
    }
}