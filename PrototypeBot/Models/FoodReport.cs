﻿using AppPrototype.Models.CustomTypes.Food;
using AppPrototype.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrototypeBot.Models
{

        public class FoodReport
        {
            //update the food item class to include additonal props (High in fat true / false ) high in protein high/ false)
            public string OpeningMessage { get; set; }
            public List<FoodItem> FoodItems { get; set; }
            public int TotalCalories { get; set; }
            public int TotalGramsOfProtein { get; set; }
            public int TotalGramsOfFat { get; set; }
            public MacroLevel FatLevel { get; set; }
            public MacroLevel ProteinLevel { get; set; }
            public MacroLevel CalorieLevel { get; set; }
            public double PercentageOfDailyCals { get; set; }
            public string ExpertAdvice { get; set; }
        }
    //the below macrolevel class is false need to get the old app prototype with the propa class
    public class MacroLevel
    { }
    
}